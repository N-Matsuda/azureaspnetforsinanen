﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.IO;
using WebApplication.Models;

namespace WebApplication1
{
    public partial class SkkAP8 : System.Web.UI.Page
    {
        const string Nonregistered = "未登録";
        const int redrawtimer1 = 30000; //全集信用タイマー
        const int redrawtimer2 = 30000; //個別集信用タイマー　
        const int redrawtimer3 = 120000; //カードロック用タイマー
        string sitename, skkcode, screentype;
        int sitenum;
        string allap8checked = "false";


        protected void Page_Load(object sender, EventArgs e)
        {
            Session[GlobalVar.DispType] = GlobalVar.TabsAP8;
            TitleLB.Text = "Jimboy";
            int dspno;
            try
            {
                //現在のユーザーの会社名取り出し
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaUserSite suser = new SynaUserSite();
                suser.OpenTable(User.Identity.Name, compname, true);

                //管理者以外は設定メニューはずす。
                if (suser.CheckAdminOrSubAdmin(User.Identity.Name) == false)
                {
                    AP8SettingBtn.Visible = false;
                    AP8SettingBtn.Enabled = false;
                }
                if (suser.CheckAdmin(User.Identity.Name) == false)
                {   //地域管理者以外は3番目以降のタブ(カードロック処理)は削除
                    C1.Web.Wijmo.Controls.C1Tabs.C1TabPageCollection c1tabs = C1Tabs3.Pages;
                    c1tabs.RemoveAt(4);
                    c1tabs.RemoveAt(3);
                }
                try
                {
                    allap8checked = (string)Session[GlobalVar.SAllAP8Chk];
                } catch(Exception ex)
                {
                    Session[GlobalVar.SAllAP8Chk] = allap8checked = "false";
                }
                dspno = (int)Session[GlobalVar.ADisplayNo];
                GetSiteNumNameCode();
            }
            catch (Exception ex)
            {
                dspno = GlobalVar.TabAP8StatusNo;
                //SetSiteNumNameCode(1, GlobalVar.ADefSiteName1, GlobalVar.ADefSkkcode1);
            }

            if (dspno == GlobalVar.TabAP8StatusNo) //集信状況
            {
                if (Page.IsPostBack == false)
                {
                    ShowAP8Status(User.Identity.Name, allap8checked);
                }
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8StatusNo;
            }
            else if (dspno == GlobalVar.TabAP8IndvStatusNo) //個別集信状況
            {
                if (Page.IsPostBack == false)
                {
                    ShowIndvAP8Status(sitenum, User.Identity.Name);
                }
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8IndvStatus;
            }
            else if (dspno == GlobalVar.TabAP8ConfigNo) //集信設定
            {
                if (Page.IsPostBack == false)
                {
                    ShowAP8Setting(User.Identity.Name);
                }
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8ConfigNo;
            }
            else if (dspno == GlobalVar.TabAP8CardLockNo)                                     //カードロックマスター管理
            {
                if (Page.IsPostBack == false)
                {
                    ShowAP8CardLock();
                }
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8CardLockNo;
            } else
            {
                if (Page.IsPostBack == false)
                {
                    ShowAP8CardLockMaster();
                }
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8CardLockMasterNo;
            }
            AddMetaHead();
        }
        //自動ログアウト設定ヘッダー追加
        protected void AddMetaHead()
        {
            System.Web.UI.HtmlControls.HtmlHead head = Page.Header;
            System.Web.UI.HtmlControls.HtmlMeta meta = new System.Web.UI.HtmlControls.HtmlMeta();

            meta.HttpEquiv = "Refresh";
            meta.Content = GlobalVar.logouttime + ";URL= Account/Login.aspx";
            head.Controls.Add(meta);
        }
        //------------- タブ変更 --------------------------
        protected void C1Tab3_SelectedChanged(object sender, EventArgs e)
        {
            string pagestr = ((C1.Web.Wijmo.Controls.C1Tabs.C1Tabs)sender).SelectedPage.Text;
            SetSiteNumNameCode(1, GlobalVar.ADefSiteName1, GlobalVar.ADefSkkcode1);
            RedrawTimerDisable();
            if (pagestr == GlobalVar.TabAP8Status) //集信状況
            {
                ShowAP8Status(User.Identity.Name, allap8checked);
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8StatusNo;
            }
            else if (pagestr == GlobalVar.TabAP8IndvStatus) //個別集信状況
            {
                ShowIndvAP8Status(sitenum, User.Identity.Name);
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8IndvStatusNo;
            }
            else if (pagestr == GlobalVar.TabAP8Config)    //集信設定
            {
                ShowAP8Setting(User.Identity.Name);
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8ConfigNo;
            }
            else if (pagestr == GlobalVar.TabAP8CardLock) //カードロック配信
            {
                ShowAP8CardLock();
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8CardLockNo;
            } else
            {
                ShowAP8CardLockMaster();
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8CardLockNo;
            }
            AddMetaHead();
        }
        //セッション情報から施設番号、施設名、施設SKKコード取り出し
        protected void GetSiteNumNameCode()
        {
            sitenum = (int)Session[GlobalVar.ASiteNo];
            sitename = (string)Session[GlobalVar.ASiteName];
            skkcode = (string)Session[GlobalVar.ASkkcode];
        }
        //施設番号、施設名、施設SKKコード取り出しをセッション情報に格納する。
        protected void SetSiteNumNameCode(int num, string name, string code)
        {
            Session[GlobalVar.ASiteName] = sitename = name;
            Session[GlobalVar.ASkkcode] = skkcode = code;
            Session[GlobalVar.ASiteNo] = sitenum = num;
        }
        //在庫画面再描画用タイマー設定
        protected void RedrawTimerSetForAll()
        {
            AP8Timer1.Enabled = true;
            AP8Timer1.Interval = redrawtimer1;
        }
        //個別集信用タイマー設定
        protected void RedrawTimerSetForIndv()
        {
            AP8Timer2.Enabled = true;
            AP8Timer2.Interval = redrawtimer2;
        }
        //カードロック用タイマー設定
        protected void RedrawTimerSetForCard()
        {
            AP8Timer3.Enabled = true;
            AP8Timer3.Interval = redrawtimer3;
        }

        protected void RedrawTimerDisable()
        {
            AP8Timer1.Enabled = false;
            AP8Timer2.Enabled = false;
            AP8Timer3.Enabled = false;
        }

        //----------------- 集信状況表作成 -------------------------------------------
        protected void ShowAP8Status(string username, string check)
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                AP8DataTable ap8dat = new AP8DataTable();
                ap8dat.CreateAP8StatusTable(username,compname);  //現在のアカウントの集信状況表を作成
                C1GridViewAP8Stat.DataSource = ap8dat.GetAP8StatusTable();
                Page.DataBind();
                if (check == "false")
                    CheckBox1.Checked = false;
                else
                    CheckBox1.Checked = true;
                int numtbl = C1GridViewAP8Stat.Rows.Count;
                for (int i = 0; i < numtbl; i++)    //集信施設を選択するチェックボックスを有効化
                {
                    C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow grow = C1GridViewAP8Stat.Rows[i];
                    CheckBox ctl = (CheckBox)grow.Cells[0].Controls[0];
                    ctl.Enabled = true;
                    if (check == "true")
                        ctl.Checked = true;
                    else
                        ctl.Checked = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //集信開始ボタン
        protected void StartButton_Click(object sender, EventArgs e)
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                AP8DataTable ap8tbl = new AP8DataTable();
                ap8tbl.CreateAP8StatusTable(User.Identity.Name, compname);
                DataTable chktbl = ap8tbl.GetAP8StatusTable();

                //チェックマークが入った施設のSKKコード、デバイスID取り出す
                int numtbl = C1GridViewAP8Stat.Rows.Count;
                List<string> chksites = new List<string>();
                for (int i = 0; i < numtbl; i++)
                {
                    C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow grow = C1GridViewAP8Stat.Rows[i];
                    CheckBox ctl = (CheckBox)grow.Cells[0].Controls[0];
                    if (ctl.Checked == true)
                    {
                        //chksites.Add(chktbl.Rows[i]["施設名"].ToString());
                        chksites.Add(ap8tbl.GetSkkcodeFromStatusTable(i));
                    }
                }
                int numsite = chksites.Count;
                string[] sitear= chksites.ToArray();
                bool[] resar = new bool[numsite];

                //各施設に集信コマンド送る
                for (int i = 0; i < numsite; i++)
                {
                    TcpCom tcom = new TcpCom();
                    if ((tcom.SendAP8Com(sitear[i], GlobalVar.GetAP8Data, compname) == true) )
                    {
                        resar[i] = true;
                    } else
                    { //通信エラーログ作成
                        resar[i] = false;
                        string val = sitear[i] + GlobalVar.GetAP8Data + GlobalVar.AccessNG;
                        SynaSiteAP8Log sap8log = new SynaSiteAP8Log();
                        sap8log.InsertRecord(val);
                    }
                }
                RedrawTimerSetForAll();
                Response.Write("<script language=JavaScript>alert('集信中')</script>");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //集信再描画タイマー
        protected void AP8Timer_Tick(object sender, EventArgs e)
        {
            RedrawTimerSetForAll();
            GetSiteNumNameCode();

            try
            {
                allap8checked = (string)Session[GlobalVar.SAllAP8Chk];
            }
            catch (Exception ex)
            {
                Session[GlobalVar.SAllAP8Chk] = allap8checked = "false";
            }
            ShowAP8Status(User.Identity.Name, allap8checked);
            Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8StatusNo;
        }

        //全ての施設にチェックを入れるボタン
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                allap8checked = (string)Session[GlobalVar.SAllAP8Chk];
                if (allap8checked == "false")
                    allap8checked = "true";
                else
                    allap8checked = "false";
                Session[GlobalVar.SAllAP8Chk] = allap8checked;
            }
            catch (Exception ex)
            {
                Session[GlobalVar.SAllAP8Chk] = allap8checked = "false";
            }
            ShowAP8Status(User.Identity.Name, allap8checked);
            AddMetaHead();
        }

        //----------------- 個別集信状況表表示 -------------------------------------------
        protected void ShowIndvAP8Status (int ssno, string username)
        {
            try
            {
                string skcode, sitenm;
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaSiteDat sydat = new SynaSiteDat();
                sydat.OpenTable("*", username, compname);
                skcode = sydat.GetSKKCode(ssno - 1);

                //施設名ドロップダウンリスト作成
                SelectSSDDList.Items.Clear();
                SelectSSDDList.DataSource = sydat.GetSiteList();
                sitenm = sydat.GetSiteName(ssno - 1);
                SelectSSDDList.Text = sitenm;

                //日付リスト作成
                AP8DatDatesLB.Items.Clear();
                FilePath fp = new FilePath();
                List<string> filenmlst = fp.ListAP8FilesDate(skcode);
                filenmlst.Reverse();
                AP8DatDatesLB.DataSource = filenmlst;
                string[] filenmar = filenmlst.ToArray();
                if (filenmar.Count() > 0)
                    AP8DatDatesLB.Text = filenmar[0];

                //直近の指定SKKコードのステータス表作成、表示
                AP8DataTable ap8dat = new AP8DataTable();
                ap8dat.CreateAP8IndvStatusTable(skcode, username, compname);
                C1GridViewAP8IndvStatus.DataSource = ap8dat.GetAP8IndStatusTable();

                SetSiteNumNameCode(ssno, sitenm, skcode);
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8IndvStatusNo;
                Page.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //施設名選択ドロップダウンリストで施設が選択された
        protected void SelectSSDDList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int siteno = SelectSSDDList.SelectedIndex + 1;
            Session[GlobalVar.ASiteNo] = siteno;
            ShowIndvAP8Status(siteno, User.Identity.Name);  //指定された施設を取り出し個別集信状況表示
        }
        //前の施設ボタンが押された
        protected void PrevSSButton_Click(object sender, EventArgs e)
        {
            int siteno = (int)Session[GlobalVar.ASiteNo];
            if (siteno <= 1)
            {
                WebResponseCtrl.WriteResponse(Response, "先頭の施設です");
                return;
            }
            siteno = siteno - 1;
            Session[GlobalVar.ASiteNo] = siteno;
            ShowIndvAP8Status(siteno, User.Identity.Name);  //前の施設の個別集信状況表示
        }

        //次の施設ボタンが押された
        protected void NextSSButton_Click(object sender, EventArgs e)
        {
            int siteno = (int)Session[GlobalVar.ASiteNo];
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            SynaSiteDat sydat = new SynaSiteDat();
            sydat.OpenTable("*", User.Identity.Name, compname);
            siteno = siteno + 1;
            if ( siteno > sydat.GetNumOfRecord())
            {
                WebResponseCtrl.WriteResponse(Response, "最後の施設です");
                return;
            }
            Session[GlobalVar.ASiteNo] = siteno;
            ShowIndvAP8Status(siteno, User.Identity.Name);   //次の施設の個別集信状況表示
        }
        //集信ボタンが押された
        protected void CollectButton_Click(object sender, EventArgs e)
        {
            GetSiteNumNameCode();
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            TcpCom tcom = new TcpCom();
            if ((tcom.SendAP8Com(skkcode, GlobalVar.GetAP8Data, compname) == true))   //集信コマンドを施設に送信
            {
                RedrawTimerSetForIndv();
                Response.Write("<script language=JavaScript>alert('集信中')</script>");
            }
            else
            {
                //集信失敗ログを追加
                string val = skkcode + GlobalVar.GetAP8Data + GlobalVar.AccessNG;
                SynaSiteAP8Log sap8log = new SynaSiteAP8Log();
                sap8log.InsertRecord(val);
                //RedrawTimerSetForIndv();
                sitenum = (int)Session[GlobalVar.ASiteNo];
                ShowIndvAP8Status(sitenum, User.Identity.Name);
                Page.DataBind();
            }
        }
        //最集信ボタンが押された
        protected void ReCollectButton_Click(object sender, EventArgs e)
        {
            GetSiteNumNameCode();
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            TcpCom tcom = new TcpCom();
            if ((tcom.SendAP8Com(skkcode, GlobalVar.RegetAP8Data, compname) == true)) //再集信コマンドを施設に送信
            {
                RedrawTimerSetForIndv();
                Response.Write("<script language=JavaScript>alert('集信中')</script>");
            }
            else
            {
                string val = skkcode + GlobalVar.RegetAP8Data + GlobalVar.AccessNG;
                SynaSiteAP8Log sap8log = new SynaSiteAP8Log();
                sap8log.InsertRecord(val);
                //RedrawTimerSetForIndv();
                sitenum = (int)Session[GlobalVar.ASiteNo];
                ShowIndvAP8Status(sitenum, User.Identity.Name);
                Page.DataBind();
            }
        }
        //ダウンロードボタンが押された
        protected void DownloadButton_Click(object sender, EventArgs e)
        {
            GetSiteNumNameCode();
            FilePath fp = new FilePath();
            //string data = fp.ReadSpecifiedDayAP8Data(skkcode, DateTime.Now.ToLocalTime());
            string data = fp.ReadTodayAP8Data(skkcode);
            data = RunLengthComp.RLDecompress(data);
            Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8IndvStatusNo;
            WebResponseCtrl.CreateResponse(Response, data, "ap8data.txt");      //本日のデータをダウンロード
        }

        //指定した日付のダウンロードボタンクリック
        protected void SelectedDateDLBtn_Click(object sender, EventArgs e)
        {
            GetSiteNumNameCode();
            string datestr = AP8DatDatesLB.Text;                //指定日を取り出す
            FilePath fp = new FilePath();
            string data = fp.ReadSpecifiedDayAP8Data(skkcode, DateTime.Parse(datestr));
            data = RunLengthComp.RLDecompress(data);
            Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8IndvStatusNo;
            WebResponseCtrl.CreateResponse(Response, data, "ap8data.txt");  //指定日のデータをダウンロード
        }
        //個別集信用タイマー
        protected void AP8Timer2_Tick(object sender, EventArgs e)
        {
            int siteno = (int)Session[GlobalVar.ASiteNo];
            ShowIndvAP8Status(siteno, User.Identity.Name);
            Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8IndvStatusNo;
            RedrawTimerDisable();
        }

        //-------------------- 集信設定 ------------------------------------------
        protected void ShowAP8Setting(string username)
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaSiteDat sydat = new SynaSiteDat();
                sydat.OpenTable("*", username, compname);

                CfgTabSSNameLB.Items.Clear();       //施設名リスト作成
                CfgTabSSNameLB.DataSource = sydat.GetSiteList();
                CfgTabSSNameLB.Text = sydat.GetSiteName(0);

                List<string> timelst = new List<string>();  //自動集信設定時刻リスト作成
                TimeSlctLB.Items.Clear();
                for (int i = 0; i < 24; i++)
                {
                    for (int j = 0; j < 60; j += 10)
                    {
                        timelst.Add(i.ToString("D2") + ":" + j.ToString("D2"));
                    }
                }
                TimeSlctLB.DataSource = timelst;
                SynaUserSite syu = new SynaUserSite();
                List<string> ulst = syu.OpenTable(username, compname, true);

                SynaSiteDatAP8 ap8site = new SynaSiteDatAP8(); //各施設ごとの自動集信状況の表作成
                ap8site.OpenTable();
                ap8site.CreateCollectDataCfg(ulst);
                APCfgGridView.DataSource = ap8site.GetCollectDataCfgTbl();
                AP8AutoCol atcol = ap8site.GetAutoColBySiteName(ap8site.GetSSNamebyLine(0));
                TimeSlctLB.Text = atcol.coltime.Insert(2, ":");

                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8ConfigNo;
                Page.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //施設名変更
        protected void CfgTabSSNameLB_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sitename = CfgTabSSNameLB.Text;      //指定施設名
            SynaSiteDatAP8 ap8site = new SynaSiteDatAP8();
            ap8site.OpenTable();
            AP8AutoCol atcol = ap8site.GetAutoColBySiteName(sitename);
            if (atcol.autocol == 0)
                TimeSlctLB.Text = "00:00";
            else
                TimeSlctLB.Text = atcol.coltime.Insert(2, ":");
            //ShowAP8Setting(User.Identity.Name);
            return;
        }

        //自動集信解除ボタン
        protected void AutoColRelBtn_Click(object sender, EventArgs e)
        {
            string sitename = CfgTabSSNameLB.Text;      //指定施設名
            string sttime = TimeSlctLB.Text;            

            SynaSiteDatAP8 ap8site = new SynaSiteDatAP8();
            ap8site.OpenTable();
            string skkcode = ap8site.GetSkkcodeBySiteName(sitename);
            bool res = ap8site.SetAutoCollectOff(skkcode);      //指定された施設の自動集信を解除
            if (res == true)
            {
                WebResponseCtrl.WriteResponse(Response, sitename + "の自動集信を解除しました");
            }
            ShowAP8Setting(User.Identity.Name);
            return;
        }

        //自動集信設定ボタン
        protected void AutoColSetBtn_Click(object sender, EventArgs e)
        {
            string sitename = CfgTabSSNameLB.Text;
            string sttime = TimeSlctLB.Text;            //指定された施設と時間を取り出す
            sttime = sttime.Remove(2, 1);

            SynaSiteDatAP8 ap8site = new SynaSiteDatAP8();
            ap8site.OpenTable();
            string skkcode = ap8site.GetSkkcodeBySiteName(sitename);
            bool res = ap8site.SetAutoCollectTime(skkcode, sttime); //指定された施設の自動集信を設定
            if (res == true)
            {
                WebResponseCtrl.WriteResponse(Response, sitename + "の自動集信を設定しました");
            }
            ShowAP8Setting(User.Identity.Name);
            return;
        }

        //-------------------- カードロック配信 ------------------------------------------
        protected void ShowAP8CardLock()
        {
            try
            {
                //AP8のある施設一覧を取り出す。
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaSiteDat ap8site = new SynaSiteDat();
                ap8site.OpenTableWithSkkcode("*", compname);

                CardLockData cldt = new CardLockData();
                cldt.CreateCLStatusTbl(ap8site);
                DeliveryStatusGV1.DataSource = cldt.GetCLStatusTbl();   //各施設のカードロック配信状況表作成
                cldt.CreateCLChkTbl(ap8site);
                DeliveryStatusGV2.DataSource = cldt.GetCLChkTbl();      //各施設のカードロック配信チェックリスト作成

                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8CardLockNo;
                Page.DataBind();

                int numtbl = DeliveryStatusGV2.Rows.Count;
                for (int i = 0; i < numtbl; i++)
                {
                    C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow grow = DeliveryStatusGV2.Rows[i];
                    CheckBox ctl = (CheckBox)grow.Cells[0].Controls[0];
                    ctl.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //bool bsenall ---> 全ての施設に配信開始 false --> checkmarkの入った施設のみ配信
        private void StartDelivery(bool ball)
        {
            try
            {
                //AP8のある施設一覧を取り出す。
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaSiteDat ap8site = new SynaSiteDat();
                ap8site.OpenTableWithSkkcode("*", compname);

                CardLockData cldt = new CardLockData();
                cldt.CreateCLChkTbl(ap8site);
                DataTable chktbl = cldt.GetCLChkTbl();
                //チェックマークが入った施設のSKKコード、デバイスID取り出す
                int numtbl = DeliveryStatusGV2.Rows.Count;
                List<string> chksites = new List<string>();
                for (int i = 0; i < numtbl; i++)
                {
                    C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow grow = DeliveryStatusGV2.Rows[i];
                    CheckBox ctl = (CheckBox)grow.Cells[0].Controls[0];
                    if (ball == false)
                    {
                        if (ctl.Checked == true)
                        {
                            //chksites.Add(chktbl.Rows[i]["施設名"].ToString());
                            chksites.Add(cldt.GetSkkcodeFromStatusTable(i));
                        }
                    } else
                    {
                        chksites.Add(cldt.GetSkkcodeFromStatusTable(i));
                    }
                }
                int numsite = chksites.Count;
                string[] skkcodear = chksites.ToArray();
                bool[] resar = new bool[numsite];

                //各施設にカードロック配信コマンド送る
                for (int i = 0; i < numsite; i++)
                {
                    TcpCom tcom = new TcpCom();
                    if ((tcom.SendAP8Com(skkcodear[i], GlobalVar.PostCardLockData, compname) == true))
                    {
                        resar[i] = true;
                    }
                    else
                    {
                        resar[i] = false;
                        //集信失敗ログを追加
                        string val = skkcodear[i] + GlobalVar.PostCardLockData + GlobalVar.AccessNG;
                        SynaSiteAP8Log sap8log = new SynaSiteAP8Log();
                        sap8log.InsertRecord(val);
                    }
                }
                RedrawTimerSetForCard();
                Response.Write("<script language=JavaScript>alert('配信中')</script>");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //直ちに配信ボタン
        protected void StartDeliveryBtn_Click(object sender, EventArgs e)
        {
            StartDelivery(true);
        }

        //選択した施設に再配信ボタン
        protected void StartReDeliveryBtn_Click(object sender, EventArgs e)
        {
            StartDelivery(false);
        }

        //-------------------- カードロックマスター管理 ------------------------------------------
        protected void ShowAP8CardLockMaster()
        {
            try
            {
                List<string> cmpcodelst = CardLockData.GetCompanyCodeList();
                List<string> sscodelst = CardLockData.GetSSCodeList();
                CompanyCodeLB.DataSource = cmpcodelst;
                SSCodeLB.DataSource = sscodelst;
                string[] cmpcodear = cmpcodelst.ToArray();
                string[] sscodear = sscodelst.ToArray();
                CompanyCodeLB.Text = cmpcodear[0];
                SSCodeLB.Text = sscodelst[0];
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8CardLockMasterNo;
                Page.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //検索ボタン
        //カード区分、企業社コード, SSコード, 得意先コード, 車両番号を指定して読み出す
        protected void SearchBtn0_Click(object sender, EventArgs e)
        {
            string cdtype = CardTypeTB.Text; //カード区分
            string cpcode = CompanyCodeLB.Text;　//企業社コード
            string sscode = SSCodeLB.Text;  //SSコード
            string clcode = ClientCodeTB0.Text;　//得意先コード
            string carnum = CarNumberTB0.Text;  //車両番号
            if (clcode.Length < 6)
            {
                int num = 6 - clcode.Length;
                for (int i = 0; i < num; i++)
                    clcode = "0" + clcode;
            }
            if (carnum.Length < 4)
            {
                int num = 4 - carnum.Length;
                for (int i = 0; i < num; i++)
                    carnum = "0" + carnum;
            }
            CardLockData cld = new CardLockData();
            cld.OpenCardLockData(cdtype, cpcode, sscode, clcode, carnum);
            if( cld.GetNumCardLockData() < 1)
            {
                CurrentStatusTB0.Text = Nonregistered;
                return;
            }
            bool blocked = cld.GetLockstatus(0);
            if (blocked == true) //カードロックステータス ロック中
            {
                CurrentStatusTB0.Text = "ロック状態";
                LockRB1.Checked = true;
                LockRB2.Checked = false;
            }
            else
            {
                CurrentStatusTB0.Text = "ロック解除状態";
                LockRB1.Checked = false;
                LockRB2.Checked = true;
            }
            Page.DataBind();
        }

        //登録ボタン
        protected void LockValidChgBtn0_Click(object sender, EventArgs e)
        {
            string cdtype = CardTypeTB.Text; //カード区分
            string cpcode = CompanyCodeLB.Text;　//企業社コード
            string sscode = SSCodeLB.Text;  //SSコード
            string clcode = ClientCodeTB0.Text;　//得意先コード
            string carnum = CarNumberTB0.Text;  //車両番号
            if (clcode.Length < 6)
            {
                int num = 6 - clcode.Length;
                for (int i = 0; i < num; i++)
                    clcode = "0" + clcode;
            }
            if (carnum.Length < 4)
            {
                int num = 4 - carnum.Length;
                for (int i = 0; i < num; i++)
                    carnum = "0" + carnum;
            }
            bool blocked = LockRB1.Checked;
            CardLockData cld = new CardLockData();　//ロック有効ならtrue
            string status = CurrentStatusTB0.Text;
            bool rslt = false;
            if( status != Nonregistered ) //カードが既に登録されている場合
            {
                rslt = cld.ChgCardLockStatus(cdtype, cpcode, sscode, clcode, carnum, blocked);
                if (rslt == true) //変更成功
                {
                    if (blocked == true)
                    {
                        WebResponseCtrl.WriteResponse(Response, "ロック有効に変更しました。");
                        CurrentStatusTB0.Text = "ロック状態";
                        LockRB1.Checked = true;
                        LockRB2.Checked = false;
                    }
                    else
                    {
                        WebResponseCtrl.WriteResponse(Response, "ロックを解除しました");
                        CurrentStatusTB0.Text = "ロック解除状態";
                        LockRB1.Checked = false;
                        LockRB2.Checked = true;
                    }
                }
                else
                {
                    if (blocked == true)
                    {
                        WebResponseCtrl.WriteResponse(Response, "ロック有効に変更できませんでした。");
                        CurrentStatusTB0.Text = "ロック解除状態";
                        LockRB1.Checked = false;
                        LockRB2.Checked = true;
                    }
                    else
                    {
                        WebResponseCtrl.WriteResponse(Response, "ロックを解除できませんでした");
                        CurrentStatusTB0.Text = "ロック状態";
                        LockRB1.Checked = true;
                        LockRB2.Checked = false;
                    }
                }
            } else  //カードが未登録の場合
            {
                rslt = cld.AddCardLockStatus(cdtype, cpcode, sscode, clcode, carnum, blocked);
                if (rslt == true) //変更成功
                {
                    WebResponseCtrl.WriteResponse(Response, "カード情報を追加しました。");
                }
                else
                {
                    WebResponseCtrl.WriteResponse(Response, "カード情報を追加できませんでした。");
                }
                if (blocked == true)
                {
                    CurrentStatusTB0.Text = "ロック状態";
                    LockRB1.Checked = true;
                    LockRB2.Checked = false;
                }
                else
                {
                    CurrentStatusTB0.Text = "ロック解除状態";
                    LockRB1.Checked = false;
                    LockRB2.Checked = true;
                }
            }
            if ( rslt == true ) //変更時はマスターのカードファイルも更新する。
            {
                try
                {
                    string sendtext = "";    //ダウンロード用文字列作成
                    CardLockData cldall = new CardLockData(); //ロック有効ならtrue
                    cldall.OpenCardLockDataAll(); //全てのカードロックデータを開く
                    sendtext += cldall.GetCardDataString();
                    FilePath fp = new FilePath();
                    fp.UploadCardLockFile(sendtext);


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            Page.DataBind();
        }

        //マスターダウンロードボタン
        protected void MasterDLBtn_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            string sitename = (string)Session[GlobalVar.SSiteName];
            sitename = sitename.TrimEnd();

            string sDownloadFileName = "カードロック" + dt.ToString("yyyyMMdd") + ".csv"; //ダウンロードファイル名設定
            try
            {
                string sendtext = "";    //ダウンロード用文字列作成
                CardLockData cld = new CardLockData(); //ロック有効ならtrue
                cld.OpenCardLockDataAll(); //全てのカードロックデータを開く
                sendtext += cld.GetCardDataString();
                WebResponseCtrl.CreateResponse(Response, sendtext, sDownloadFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //カードロック配信用タイマー
        protected void AP8Timer3_Tick(object sender, EventArgs e)
        {
            RedrawTimerSetForCard();
            ShowAP8CardLock();
            Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8CardLockNo;
        }

        //---------------- ヘッダーボタン処理 ------------
        //ログアウトボタン
        protected void AP8LogoutBtn_Click(object sender, EventArgs e)
        {
            RedrawTimerDisable();
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            Response.Redirect("Account\\Login.aspx");
        }

        protected void AP8LeveBtn_Click(object sender, EventArgs e)
        {
            RedrawTimerDisable();
            Session[GlobalVar.DispType] = GlobalVar.TabsLevevision;
            Session[GlobalVar.SDisplayNo] = 0;
            Session[GlobalVar.SAllAP8Chk] = "false";
            Response.Redirect("SkkDefault.aspx");
        }

        protected void AP8SettingBtn_Click(object sender, EventArgs e)
        {
            RedrawTimerDisable();
            Session[GlobalVar.DispType] = GlobalVar.TabsSettings;
            Session[GlobalVar.SDisplayNo] = 0;
            Session[GlobalVar.SAllAP8Chk] = "false";
            Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgDispSiteNo;
            Response.Redirect("SkkSettings.aspx");
        }
    }




}

