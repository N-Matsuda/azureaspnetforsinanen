﻿//------------------------------------------------------------------------------
// <自動生成>
//     このコードはツールによって生成されました。
//
//     このファイルへの変更は、以下の状況下で不正な動作の原因になったり、
//     コードが再生成されるときに損失したりします。 
// </自動生成>
//------------------------------------------------------------------------------

namespace WebApplication {
    
    
    public partial class SkkDefault {
        
        /// <summary>
        /// Panel1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel1;
        
        /// <summary>
        /// TitleLB コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label TitleLB;
        
        /// <summary>
        /// AP8Button コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button AP8Button;
        
        /// <summary>
        /// SettingButton コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button SettingButton;
        
        /// <summary>
        /// LogOutBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button LogOutBtn;
        
        /// <summary>
        /// C1Tabs1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1Tabs C1Tabs1;
        
        /// <summary>
        /// C1Tabs1_Tab1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1TabPage C1Tabs1_Tab1;
        
        /// <summary>
        /// Label32 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label32;
        
        /// <summary>
        /// PrefListBox1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox PrefListBox1;
        
        /// <summary>
        /// Label33 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label33;
        
        /// <summary>
        /// SSListBox コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox SSListBox;
        
        /// <summary>
        /// ShowLowInvSitesBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button ShowLowInvSitesBtn;
        
        /// <summary>
        /// NxtPageBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button NxtPageBtn;
        
        /// <summary>
        /// PrvPageBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PrvPageBtn;
        
        /// <summary>
        /// DownloadBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button DownloadBtn;
        
        /// <summary>
        /// SSNameLabelZ1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ1;
        
        /// <summary>
        /// C1SplitterZ1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ1;
        
        /// <summary>
        /// C1GridViewZ1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ1;
        
        /// <summary>
        /// C1WebChartZ1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ1;
        
        /// <summary>
        /// SSNameLabelZ2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ2;
        
        /// <summary>
        /// C1SplitterZ2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ2;
        
        /// <summary>
        /// C1GridViewZ2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ2;
        
        /// <summary>
        /// C1WebChartZ2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ2;
        
        /// <summary>
        /// SSNameLabelZ3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ3;
        
        /// <summary>
        /// C1SplitterZ3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ3;
        
        /// <summary>
        /// C1GridViewZ3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ3;
        
        /// <summary>
        /// C1WebChartZ3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ3;
        
        /// <summary>
        /// SSNameLabelZ4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ4;
        
        /// <summary>
        /// C1SplitterZ4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ4;
        
        /// <summary>
        /// C1GridViewZ4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ4;
        
        /// <summary>
        /// C1WebChartZ4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ4;
        
        /// <summary>
        /// SSNameLabelZ5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ5;
        
        /// <summary>
        /// C1SplitterZ5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ5;
        
        /// <summary>
        /// C1GridViewZ5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ5;
        
        /// <summary>
        /// C1WebChartZ5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ5;
        
        /// <summary>
        /// SSNameLabelZ6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ6;
        
        /// <summary>
        /// C1SplitterZ6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ6;
        
        /// <summary>
        /// C1GridViewZ6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ6;
        
        /// <summary>
        /// C1WebChartZ6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ6;
        
        /// <summary>
        /// SSNameLabelZ7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ7;
        
        /// <summary>
        /// C1SplitterZ7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ7;
        
        /// <summary>
        /// C1GridViewZ7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ7;
        
        /// <summary>
        /// C1WebChartZ7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ7;
        
        /// <summary>
        /// SSNameLabelZ8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ8;
        
        /// <summary>
        /// C1SplitterZ8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ8;
        
        /// <summary>
        /// C1GridViewZ8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ8;
        
        /// <summary>
        /// C1WebChartZ8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ8;
        
        /// <summary>
        /// SSNameLabelZ9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ9;
        
        /// <summary>
        /// C1SplitterZ9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ9;
        
        /// <summary>
        /// C1GridViewZ9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ9;
        
        /// <summary>
        /// C1WebChartZ9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ9;
        
        /// <summary>
        /// SSNameLabelZ10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ10;
        
        /// <summary>
        /// C1SplitterZ10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ10;
        
        /// <summary>
        /// C1GridViewZ10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ10;
        
        /// <summary>
        /// C1WebChartZ10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ10;
        
        /// <summary>
        /// SSNameLabelZ11 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ11;
        
        /// <summary>
        /// C1SplitterZ11 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ11;
        
        /// <summary>
        /// C1GridViewZ11 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ11;
        
        /// <summary>
        /// C1WebChartZ11 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ11;
        
        /// <summary>
        /// SSNameLabelZ12 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ12;
        
        /// <summary>
        /// C1SplitterZ12 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ12;
        
        /// <summary>
        /// C1GridViewZ12 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ12;
        
        /// <summary>
        /// C1WebChartZ12 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ12;
        
        /// <summary>
        /// SSNameLabelZ13 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ13;
        
        /// <summary>
        /// C1SplitterZ13 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ13;
        
        /// <summary>
        /// C1GridViewZ13 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ13;
        
        /// <summary>
        /// C1WebChartZ13 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ13;
        
        /// <summary>
        /// SSNameLabelZ14 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ14;
        
        /// <summary>
        /// C1SplitterZ14 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ14;
        
        /// <summary>
        /// C1GridViewZ14 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ14;
        
        /// <summary>
        /// C1WebChartZ14 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ14;
        
        /// <summary>
        /// SSNameLabelZ15 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ15;
        
        /// <summary>
        /// C1SplitterZ15 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ15;
        
        /// <summary>
        /// C1GridViewZ15 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ15;
        
        /// <summary>
        /// C1WebChartZ15 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ15;
        
        /// <summary>
        /// SSNameLabelZ16 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ16;
        
        /// <summary>
        /// C1SplitterZ16 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ16;
        
        /// <summary>
        /// C1GridViewZ16 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ16;
        
        /// <summary>
        /// C1WebChartZ16 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ16;
        
        /// <summary>
        /// SSNameLabelZ17 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ17;
        
        /// <summary>
        /// C1SplitterZ17 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ17;
        
        /// <summary>
        /// C1GridViewZ17 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ17;
        
        /// <summary>
        /// C1WebChartZ17 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ17;
        
        /// <summary>
        /// SSNameLabelZ18 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ18;
        
        /// <summary>
        /// C1SplitterZ18 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ18;
        
        /// <summary>
        /// C1GridViewZ18 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ18;
        
        /// <summary>
        /// C1WebChartZ18 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ18;
        
        /// <summary>
        /// SSNameLabelZ19 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ19;
        
        /// <summary>
        /// C1SplitterZ19 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ19;
        
        /// <summary>
        /// C1GridViewZ19 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ19;
        
        /// <summary>
        /// C1WebChartZ19 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ19;
        
        /// <summary>
        /// SSNameLabelZ20 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ20;
        
        /// <summary>
        /// C1SplitterZ20 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ20;
        
        /// <summary>
        /// C1GridViewZ20 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ20;
        
        /// <summary>
        /// C1WebChartZ20 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ20;
        
        /// <summary>
        /// SSNameLabelZ21 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ21;
        
        /// <summary>
        /// C1SplitterZ21 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ21;
        
        /// <summary>
        /// C1GridViewZ21 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ21;
        
        /// <summary>
        /// C1WebChartZ21 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ21;
        
        /// <summary>
        /// SSNameLabelZ22 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ22;
        
        /// <summary>
        /// C1SplitterZ22 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ22;
        
        /// <summary>
        /// C1GridViewZ22 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ22;
        
        /// <summary>
        /// C1WebChartZ22 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ22;
        
        /// <summary>
        /// SSNameLabelZ23 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ23;
        
        /// <summary>
        /// C1SplitterZ23 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ23;
        
        /// <summary>
        /// C1GridViewZ23 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ23;
        
        /// <summary>
        /// C1WebChartZ23 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ23;
        
        /// <summary>
        /// SSNameLabelZ24 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ24;
        
        /// <summary>
        /// C1SplitterZ24 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ24;
        
        /// <summary>
        /// C1GridViewZ24 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ24;
        
        /// <summary>
        /// C1WebChartZ24 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ24;
        
        /// <summary>
        /// SSNameLabelZ25 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ25;
        
        /// <summary>
        /// C1SplitterZ25 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ25;
        
        /// <summary>
        /// C1GridViewZ25 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ25;
        
        /// <summary>
        /// C1WebChartZ25 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ25;
        
        /// <summary>
        /// SSNameLabelZ26 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ26;
        
        /// <summary>
        /// C1SplitterZ26 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ26;
        
        /// <summary>
        /// C1GridViewZ26 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ26;
        
        /// <summary>
        /// C1WebChartZ26 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ26;

        /// <summary>
        /// SSNameLabelZ27 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelZ27;
        
        /// <summary>
        /// C1SplitterZ27 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Splitter.C1Splitter C1SplitterZ27;
        
        /// <summary>
        /// C1GridViewZ27 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewZ27;
        
        /// <summary>
        /// C1WebChartZ27 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartZ27;

        /// <summary>
        /// C1Tabs1_Tab2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1TabPage C1Tabs1_Tab2;
        
        /// <summary>
        /// SSNameLabelTH コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSNameLabelTH;
        
        /// <summary>
        /// Label34 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label34;
        
        /// <summary>
        /// PrefListBox2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox PrefListBox2;
        
        /// <summary>
        /// Label2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label2;
        
        /// <summary>
        /// THSSListBox コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox THSSListBox;
        
        /// <summary>
        /// Label1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;
        
        /// <summary>
        /// SelectTankDDL コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList SelectTankDDL;
        
        /// <summary>
        /// SelectTankLabel0 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SelectTankLabel0;
        
        /// <summary>
        /// LVDataDownloadBtn コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button LVDataDownloadBtn;
        
        /// <summary>
        /// C1WebChartTH1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.C1WebChart.C1WebChart C1WebChartTH1;
        
        /// <summary>
        /// C1Tabs1_Tab4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1TabPage C1Tabs1_Tab4;
        
        /// <summary>
        /// Label15 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label15;
        
        /// <summary>
        /// SSManageDateLabel コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label SSManageDateLabel;
        
        /// <summary>
        /// C1GridViewWrgSS1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView C1GridViewWrgSS1;
        
        /// <summary>
        /// C1Tabs1_Tab5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1Tabs.C1TabPage C1Tabs1_Tab5;
        
        /// <summary>
        /// Label16 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label16;
        
        /// <summary>
        /// PrevButton コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button PrevButton;
        
        /// <summary>
        /// NextButton コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button NextButton;
        
        /// <summary>
        /// Label35 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label35;
        
        /// <summary>
        /// PrefListBox3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox PrefListBox3;
        
        /// <summary>
        /// Label17 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label17;
        
        /// <summary>
        /// SSNameLB コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.ListBox SSNameLB;
        
        /// <summary>
        /// Label18 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label18;
        
        /// <summary>
        /// SSCodeText コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox SSCodeText;
        
        /// <summary>
        /// Label19 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label19;
        
        /// <summary>
        /// TankVolGrid コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView TankVolGrid;
        
        /// <summary>
        /// Label20 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label20;
        
        /// <summary>
        /// OilTypeGrid コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::C1.Web.Wijmo.Controls.C1GridView.C1GridView OilTypeGrid;
        
        /// <summary>
        /// Label21 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label21;
        
        /// <summary>
        /// LWVRegButton コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button LWVRegButton;
        
        /// <summary>
        /// Label22 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label22;
        
        /// <summary>
        /// LWVTB1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB1;
        
        /// <summary>
        /// Label23 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label23;
        
        /// <summary>
        /// LWVTB2 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB2;
        
        /// <summary>
        /// Label24 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label24;
        
        /// <summary>
        /// LWVTB3 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB3;
        
        /// <summary>
        /// Label25 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label25;
        
        /// <summary>
        /// LWVTB4 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB4;
        
        /// <summary>
        /// Label26 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label26;
        
        /// <summary>
        /// LWVTB5 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB5;
        
        /// <summary>
        /// Label27 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label27;
        
        /// <summary>
        /// LWVTB6 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB6;
        
        /// <summary>
        /// Label28 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label28;
        
        /// <summary>
        /// LWVTB7 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB7;
        
        /// <summary>
        /// Label29 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label29;
        
        /// <summary>
        /// LWVTB8 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB8;
        
        /// <summary>
        /// Label30 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label30;
        
        /// <summary>
        /// LWVTB9 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB9;
        
        /// <summary>
        /// Label31 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label31;
        
        /// <summary>
        /// LWVTB10 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox LWVTB10;
        
        /// <summary>
        /// Timer1 コントロール。
        /// </summary>
        /// <remarks>
        /// 自動生成されたフィールド。
        /// 変更するには、フィールドの宣言をデザイナー ファイルから分離コード ファイルに移動します。
        /// </remarks>
        protected global::System.Web.UI.Timer Timer1;
    }
}
