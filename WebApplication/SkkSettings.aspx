﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkkMaster.Master" AutoEventWireup="true" CodeBehind="SkkSettings.aspx.cs" Inherits="WebApplication1.SkkSettings" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Tabs" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="Panel2" runat="server" BackColor="#102C44" ForeColor="#99CCFF" Height="30px">
          &nbsp;&nbsp;&nbsp;<asp:Label ID="TitleLB" runat="server" Text="Label" ForeColor="White" Width="600px"></asp:Label>
          &nbsp;&nbsp;&nbsp;
          <asp:Button ID="SetLevevisionBtn" runat="server" OnClick="SetLevevisionBtn_Click" Text="レベビジョン" Height="28px" Width="120px" />
          &nbsp;
          <asp:Button ID="SetAP8Btn" runat="server" Height="28px" OnClick="SetAP8Btn_Click" Text="ジムボーイ   " Width="84px" />
          &nbsp;
          <asp:Button ID="SettingLogoutBtn" runat="server" OnClick="SettingLogoutBtn_Click" Text="ログアウト" Height="28px" Width="84px" />
        <br />
        <br />
    </asp:Panel>
    <br />
    <wijmo:C1Tabs ID="C1Tabs2" runat="server" AutoPostBack="True" OnSelectedChanged="C1Tabs2_SelectedChanged">
        <Pages>
            <wijmo:C1TabPage runat="server" Text="表示施設" StaticKey="" ID="C1Tabs2_Tab1">
                <br />
                <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" Text="表示施設"></asp:Label>
                　　　　　　　　<asp:Label ID="Label2" runat="server" Font-Size="Large" Text="アカウント名"></asp:Label>
                　　<asp:DropDownList ID="AccountNameDDList" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="AccountNameDDList_SelectedIndexChanged">
                </asp:DropDownList>
                <br />
                <br />
                アカウントの表示施設をチェックし、右の登録ボタンをクリック&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="RegButton" runat="server" OnClick="RegButton_Click" Text="　　登録　　" />
                <br />
                <br />
                <br />
                <wijmo:C1GridView ID="UserSSGrid" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: 1px; left: 0px">
                    <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                    <Columns>
                        <wijmo:C1CheckBoxField DataField="表示" HeaderText="表示" Width="50px">
                        </wijmo:C1CheckBoxField>
                        <wijmo:C1BoundField DataField="施設名" HeaderText="施設名" Width="200px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="SKKコード" HeaderText="施設コード" Width="200px">
                        </wijmo:C1BoundField>
                    </Columns>
                </wijmo:C1GridView>
                <br />
                アカウント毎の登録施設を記述したCSVファイルを下記参照ボタンより指定し、<br />アップロードボタンを押すことにより、アカウント毎の表示施設を一括登録します。<br />
                <br />
                <asp:FileUpload ID="FileUpload1" runat="server" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="アップロード" />
                <br />
                <br />
                全てのアカウント毎の表示施設を下記ボタンよりダウンロードできます。<br />
                <br />
                <asp:Button ID="btnDownload" runat="server" OnClick="btnDownload_Click" Text="ダウンロード" />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="C1Tabs2_Tab3" runat="server" StaticKey="" Text="アカウント管理" TabIndex="1">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="X-Large" Text="削除するアカウント"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label4" runat="server" Text="削除するアカウントを下のリストより、選択し、右の削除ボタンをクリック"></asp:Label>
                　　<asp:Button ID="AccDelButton" runat="server" OnClick="AccDelButton_Click" Text=" アカウント削除" />
                <br />
                <br />
                <asp:DropDownList ID="AccDelDDL" runat="server" Width="400px">
                </asp:DropDownList>
                <br />
                <br />
                <br />
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="X-Large" Text="拠点管理者として設定するアカウント"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label6" runat="server" Text="拠点管理者として設定するアカウントを下のリストより、選択し、右の設定ボタンをクリック"></asp:Label>
                &nbsp;&nbsp;
                <asp:Button ID="AccRegMngBtn" runat="server" OnClick="AccRegMngBtn_Click" Text="設定" />
                <br />
                <br />
                <asp:DropDownList ID="AccRegMngDDL" runat="server" Width="400px">
                </asp:DropDownList>
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="C1Tabs2_Tab2" runat="server" StaticKey="" Text="アカウント作成" TabIndex="2">
                <br />
                <br />
                <br />
                <asp:Label ID="Label7" runat="server" Text="既存のアカウント"></asp:Label>
                　　<asp:ListBox ID="ExiAccLB" runat="server" Rows="1" Width="300px"></asp:ListBox>
                <br />
                <br />
                <br />
                <asp:Button ID="AccRegButton" runat="server" CausesValidation="False" OnClick="AccRegButton_Click" Text=" アカウント登録画面へ " />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
</asp:Content>
