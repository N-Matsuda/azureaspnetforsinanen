﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication.Models;
using System.IO;
using System.Drawing;
using System.Web.Security;
using System.Data;
using System.Net;
using System.Diagnostics;

namespace WebApplication
{
    public partial class SkkDefault : System.Web.UI.Page
    {
        const int NUMSSPERPAGE = 27;
        const int redrawtimer = 600000; //再描画タイマー ms
        //const int redrawtimer = 60000; //再描画タイマー ms
        ZHistData ZHistDat;
        SiteInfData SiteDat;
        C1.Web.Wijmo.Controls.C1GridView.C1GridView[] C1GVArr;
        C1.Web.C1WebChart.C1WebChart[] C1WCArr;
        C1.Web.Wijmo.Controls.C1Splitter.C1Splitter[] C1SPArr;
        Label[] SSNLArr;

        string skkcode;
        string sitename;
        int companyno;
        string disptype;
        string prefname;

        int sitenum;
        int invpageno;
        int tankno;
        bool showwrgonly;
        LeveCompanyProfile lvcprof;

        protected void Page_Load(object sender, EventArgs e)
        {
            //showwrgonly = false;
            if ((GlobalVar.loginreq == true) && (User.Identity.IsAuthenticated == false))
            {
                Session[GlobalVar.SDisplayNo] = GlobalVar.TabTableNo;
                Session[GlobalVar.SDispWrgOnly] = false;
                Response.Redirect("Account\\Login.aspx");
            }
            //現在のユーザーの会社名取り出し
            SynaUserSite suser = new SynaUserSite();
            suser.OpenTable(User.Identity.Name, "*", false);
            string compname = suser.GetCompanyName();

            //ユーザー毎の対象施設取り出し
            lvcprof = new LeveCompanyProfile();                 //会社名に対応する会社情報を開き会社番号を取り出す
            lvcprof.OpenTable(compname);
            Session[GlobalVar.SCompanyNo] = lvcprof.compno;
            Session[GlobalVar.SCompanyName] = lvcprof.compname1;
            //施設登録内容の液種をfalseにする。
            Label20.Visible = true;
            OilTypeGrid.DisplayVisible = true;

            //管理者以外は設定メニューはずす。
            if (suser.CheckAdminOrSubAdmin(User.Identity.Name) == false)
            {
                SettingButton.Visible = false;
                SettingButton.Enabled = false;
            }
            //if( (GlobalVar.loginreq == true) && ( compname != GlobalVar.CompanyNameAlpha) )       //シナネン以外の会社名ならログアウトする。
            int dspno;
            try
            {
                disptype = (string)Session[GlobalVar.DispType];
            }
            catch (Exception ex)
            {
                disptype = GlobalVar.TabsLevevision;
            }
            if (disptype == GlobalVar.TabsAP8)                  //表示ページが在庫以外なら、AP8、設定表示へ切り替え
            {
                Session[GlobalVar.ADisplayNo] = GlobalVar.TabAP8StatusNo;
                Response.Redirect("SkkAP8.aspx");
            } 
            else if (disptype == GlobalVar.TabsSettings)
            {
                Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgDispSiteNo;
                Response.Redirect("SkkSettings.aspx");
            }
            disptype = GlobalVar.TabsLevevision;
            Session[GlobalVar.DispType] = disptype;
            try
            {                                                   //セッション情報の取り出し or 初期設定
                dspno = (int)Session[GlobalVar.SDisplayNo];
                invpageno = (int)Session[GlobalVar.SIPageNo];
            }
            catch (Exception ex)
            {
                Session[GlobalVar.SDispWrgOnly] = showwrgonly = false;
                dspno = GlobalVar.TabTableNo;
            }
            DataUnitTable dtu = new DataUnitTable();
            dtu.OpenTable("*", User.Identity.Name);           ////会社名に対応するSSデータ一覧を取り出す
            TitleLB.Text = "レべビジョン";

            if (dspno == GlobalVar.TabTableNo) //表＆グラフ
            {
                if (Page.IsPostBack == false)
                {
                    SetTableComponents();       //表示コントロール設定
                    GetSiteSkkcode(lvcprof);           //SKKコード取得
                    SetPrefListB(GlobalVar.defprefname, PrefListBox1, true);           //県名一覧表示
                    SetZaikoSelectLB(GlobalVar.defprefname, SSListBox, true);         //施設リストボックス表示
                    ShowTableZ(sitenum, GlobalVar.defprefname, false);        //在庫表グラフ表示
                    Session[GlobalVar.SDispWrgOnly] = false;
                }
                Page.DataBind();
                AdjustTableLayoutZ();
                RedrawTimerSet();           //再描画タイマーセット
                SetDispSession(GlobalVar.TabTableNo);//表&グラフタブセット
            }
            else if (dspno == GlobalVar.TabSSTHisNo) //タンク別履歴
            {
                if (Page.IsPostBack == false)
                {
                    GetSiteSkkcode(lvcprof);
                    SetPrefListB(prefname, PrefListBox2,false);           //県名一覧表示
                    SetZaikoSelectLB(sitename, THSSListBox, false);
                    SelectTankDDL.Items.Clear();
                    SiteDat = new SiteInfData();
                    int tno = SiteDat.OpenTableSkkcode(skkcode);
                    for (int i = 1; i <= tno; i++)               //タンクドロップダウンリスト作成
                    {
                        SelectTankDDL.Items.Add(new ListItem("タンク" + i.ToString(), i.ToString()));
                    }
                }
                else
                {
                    GetSiteSkkcode(lvcprof);
                }
                ShowSSTankHistory(sitename, skkcode, tankno);   //タンク別履歴をグラフ表示

                DateTime dt = DateTime.Now.ToLocalTime();
                RedrawTimerDisable();
                Page.DataBind();
                SetDispSession(GlobalVar.TabSSTHisNo);
            } else if(dspno == GlobalVar.TabSSWrgNo)//警報発生施設表示
            {
                if (Page.IsPostBack == false)
                {
                    ShowWarningSite();      //警報発生中の施設を表表示
                }
                RedrawTimerDisable();
                Page.DataBind();
                AdjustTableLayoutSSMng();
                SetDispSession(GlobalVar.TabSSWrgNo);
            } else //登録施設表示
            {
                if (Page.IsPostBack == false)
                {
                    SetPrefListB(prefname, PrefListBox3,false);           //県名一覧表示
                    ShowSSInfo(1, User.Identity.Name);      //施設情報の登録内容表示
                    Session[GlobalVar.SSiteNo] = sitenum = 0;
                    Session[GlobalVar.SValSet] = GlobalVar.ValSet; //下限値設定済
                }
                RedrawTimerDisable();
                Page.DataBind();
                SetDispSession(GlobalVar.TabSiteInfNo);
            }
            AddMetaHead();
        }
        //自動ログアウト設定ヘッダー追加
        protected void AddMetaHead()
        {
            System.Web.UI.HtmlControls.HtmlHead head = Page.Header;
            System.Web.UI.HtmlControls.HtmlMeta meta = new System.Web.UI.HtmlControls.HtmlMeta();

            meta.HttpEquiv = "Refresh";
            meta.Content = GlobalVar.logouttime + ";URL= Account/Login.aspx"; 
            head.Controls.Add(meta);
        }

        //----------------------- タブ変更 ------------------------------------------------
        protected void C1Tab1_SelectedChanged(object sender, EventArgs e)
        {
            string accname = User.Identity.Name;
            string pagestr = ((C1.Web.Wijmo.Controls.C1Tabs.C1Tabs)sender).SelectedPage.Text;
            GetCompanyProfile();
            if (pagestr == GlobalVar.TabTable) //表＆グラフ
            {
                PrefListBox1.Enabled = SSListBox.Enabled = NxtPageBtn.Enabled = PrvPageBtn.Enabled = true;
                Session[GlobalVar.SDispWrgOnly] = showwrgonly = false;
                Session[GlobalVar.SSelectSite] = GlobalVar.AllSites;
                ShowLowInvSitesBtn.Text = GlobalVar.ShowWrgSites;
                try
                {
                    SetTableComponents();    //表示コンポーネントセット
                    SetDefSiteSkkcode(lvcprof,true);    //SKKコード取り出し
                    SetPrefListB(GlobalVar.defprefname, PrefListBox1, true);           //県名一覧表示
                    SetZaikoSelectLB(GlobalVar.defprefname, SSListBox, true);         //施設リストボックス表示

                    ShowTableZ(sitenum, GlobalVar.defprefname, false);    //在庫画面表示
                    Page.DataBind();
                    AdjustTableLayoutZ();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                RedrawTimerSet();   //再描画タイマーセット
                Session[GlobalVar.SDisplayNo] = GlobalVar.TabTableNo;
            }
            else if (pagestr == GlobalVar.TabSSTHistory) //タンク別履歴
            {
                SetDefSiteSkkcode(lvcprof, true);
                try
                {
                    SetPrefListB(prefname, PrefListBox2,false);           //県名一覧表示
                    SetZaikoSelectLB(prefname, THSSListBox, false);
                    SiteDat = new SiteInfData();
                    int totaltno = SiteDat.OpenTableSkkcode(skkcode);
                    SelectTankDDL.Items.Clear();            //タンク番号ドロップダウンリスト作成
                    for (int i = 1; i <= totaltno; i++)
                    {
                        SelectTankDDL.Items.Add(new ListItem("タンク" + i.ToString(), i.ToString()));
                    }
                    SSNameLabelTH.Text = sitename + " " + skkcode;  //施設名表示
                    ShowSSTankHistory(sitename, skkcode, tankno);   //タンク別履歴グラフ表示
                    Page.DataBind();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                RedrawTimerDisable();
                SetDispSession(GlobalVar.TabSSTHisNo);
            }
            else if (pagestr == GlobalVar.TabSSWrg)//警報発生施設表示
            {
                try
                {
                    SetDefSiteSkkcode(lvcprof, true);
                    ShowWarningSite();          //警報発生施設表表示
                    RedrawTimerDisable();
                    Page.DataBind();
                    AdjustTableLayoutSSMng();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                SetDispSession(GlobalVar.TabSSWrgNo);
            }
            else                                  //登録施設表示
            {
                try
                {
                    SetDefSiteSkkcode(lvcprof, true);
                    RedrawTimerDisable();
                    SetPrefListB(prefname, PrefListBox3, false);           //県名一覧表示
                    ShowSSInfo(0, User.Identity.Name);  //施設情報の登録内容表示
                    Page.DataBind();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                SetDispSession(GlobalVar.TabSiteInfNo);
            }
            AddMetaHead();
        }

        //----------------------- 表表示画面処理 ------------------------------------------------
        protected void ShowTableZ(int sitenum, string prefname, bool showsingle)　//表表示処理
        {
            try
            {
                showwrgonly = (bool)Session[GlobalVar.SDispWrgOnly];
                companyno = (int)Session[GlobalVar.SCompanyNo];
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                ZaikoDataTable dtbl;
                foreach (string file in Directory.GetFiles(HttpContext.Current.Server.MapPath("./") + "C1WebChartTemp\\", "*.png"))
                {       //ComponentOne テンポラリファイル削除
                    File.Delete(file);
                }
                DataUnitTable dttable = new DataUnitTable();    //現在のアカウントで表示可能なSS一覧取得
                dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);

                int numsite = dttable.GetNumOfRecord();     //施設数
                string slsite = Session[GlobalVar.SSelectSite].ToString().TrimEnd();
                int i,j=0;
                string tmstr = "";
                for (i = sitenum, j=0; i < numsite; i++)         //各SSに対して処理実行
                {
                    try
                    {
                        string skkcode = dttable.GetSSCode(i);  //SKKコード取り出し
                        dtbl = new ZaikoDataTable(skkcode);             
                        List<DateTime> dtlst = dtbl.CreateZaikoTable(showwrgonly);
                        if (dtlst.Count>0)
                        {
                            dtbl.CreateTankOilZaikoTablePerSS();      //各施設に対する在庫表作成
                            ShowTableComponent(j);
                            DataTable dtb = dtbl.GetTankTable();
                            C1GVArr[j].DataSource = dtb;              //在庫表表示
                            DrawGraph.ShowSSGraph(C1WCArr[j], dtbl, skkcode);   //指定SKKコード施設のグラフ作成
                            DateTime[] dtar = dtlst.ToArray();
                            tmstr = dtar[0].ToString("yyyy年M月d日 H時m分");
                            SSNLArr[j].Text = dttable.GetSSName(i) + " 　　" + tmstr; //SS名、時間表示
                            SSNLArr[j].Visible = true;
                            j++;
                        }
                        if( showsingle == true )
                            break;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                    if (j >= NUMSSPERPAGE)
                        break;
                }
                if (j == 0)
                {
                    if (showwrgonly == true)
                        WebResponseCtrl.WriteResponse(Response, "警報発生中の施設はありません。");
                }
                if (j < NUMSSPERPAGE)
                {
                    for (; j < NUMSSPERPAGE; j++)
                        HideTableComponent(j);
                }
                Session[GlobalVar.SSortby] = "タンク";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected void AdjustTableLayoutZ()
        {
            string str;
            try
            {       //Arrayの実態がないときに終了するため
                bool bchk = C1GVArr[0].DisplayVisible;
            }
            catch (Exception ex)
            {
                return;
            }
            for (int j = 0; j < NUMSSPERPAGE; j++)
            {
                if (C1GVArr[j].DisplayVisible == true)
                {
                    for (int i = 0; i < C1GVArr[j].Rows.Count; i++)
                    {
                        C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow drow = C1GVArr[j].Rows[i];

                        str = drow.Cells[5].Text;
                        if (str != GlobalVar.TankNoError)
                        {
                            C1GVArr[j].Rows[i].Cells[GlobalVar.TankStatColNo].ForeColor = Color.Red;
                        }
                    }
                }
            }

        }

        //施設選択用リストボックス設定
        protected void SetZaikoSelectLB(string prefname, ListBox sslistbox, bool incall)
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                companyno = (int)Session[GlobalVar.SCompanyNo];
                DataUnitTable dttable = new DataUnitTable();
                //現在の会社のログインアカウントに対するデータテーブルをオープン
                dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);

                sslistbox.Items.Clear();
                List<string> ssnames = dttable.GetSiteList();   //SS名一覧を取り出す
                if( incall == true )
                    ssnames.Insert(0, GlobalVar.AllSites);
                sslistbox.DataSource = ssnames;
                string[] ssar = ssnames.ToArray();
                sslistbox.Text = ssar[0];
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        //在庫画面再描画用タイマー設定
        protected void RedrawTimerSet()
        {
            Timer1.Enabled = true;
            Timer1.Interval = redrawtimer;
        }
        protected void RedrawTimerDisable()
        {
            Timer1.Enabled = false;
        }

        //画面更新タイマー
        protected void Timer1_Tick(object sender, EventArgs e)
        {
            string sessionstr = (string)Session[GlobalVar.SComStatus];
            string prefname = PrefListBox1.Text.TrimEnd();
            string ssname = SSListBox.Text.TrimEnd();
            int dspno = (int)Session[GlobalVar.SDisplayNo];
            Timer1.Enabled = false;
            showwrgonly = (bool)Session[GlobalVar.SDispWrgOnly];
            if (showwrgonly == true) //警報施設のみの時は再描画しない
            {
                return;
            } 
            else if (dspno == GlobalVar.TabTableNo)
            {                       //在庫画面更新処理
                int siteno = (int)Session[GlobalVar.SSiteNo];
                SetTableComponents();
                SetPrefListB(prefname, PrefListBox1, true);           //県名一覧表示
                SetZaikoSelectLB(prefname, SSListBox, true);         //施設リストボックス表示
                if (ssname == GlobalVar.AllSites)
                    ShowTableZ(siteno, prefname, false); //在庫画面表示
                else
                    ShowTableZ(siteno, prefname, true); //在庫画面表示
                Page.DataBind();
                AdjustTableLayoutZ();
                RedrawTimerSet();　//再描画タイマーセット
                Session[GlobalVar.SDisplayNo] = GlobalVar.TabTableNo;
                return;
            }
        }

        //ダウンロードボタンクリック時
        protected void DownloadBtn_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            try
            {
                string sitename = SSListBox.Text.TrimEnd();
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                string prefname = PrefListBox1.Text.TrimEnd();
                string skkcode = Session[GlobalVar.SSkkcode].ToString().TrimEnd();
                string sDownloadFileName = "在庫" + dt.ToString("yyyyMMdd") + ".csv"; //ダウンロードファイル名指定
                DataUnitTable dttable = new DataUnitTable();

                //現在の会社の指定アカウントに対するすべての施設のデータテーブルをオープン
                if (sitename == GlobalVar.AllSites) //全ての施設
                {
                    dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);
                }
                else
                {
                    dttable.OpenTableSkkcodesMbl(skkcode, User.Identity.Name, prefname, compname);
                }
                int numsite = dttable.GetNumOfRecord(); //施設数
                if( (prefname != GlobalVar.alljapan) && (sitename != GlobalVar.AllSites) )
                    numsite = 1;
                DateTime dtnow = DateTime.Now.ToLocalTime();
                if (numsite > 0)
                {
                    string sendtext = "";
                    sendtext += ZaikoDataTable.GetInventoryStringHeader();  //ヘッダー取り出し
                    for (int i = 0; i < numsite; i++)   //各施設ごとにSKKコードを取り出し、在庫表を作成
                    {
                        skkcode = dttable.GetSSCode(i);
                        ZaikoDataTable dtbl = new ZaikoDataTable(skkcode);
                        sendtext += dtbl.GetInventoryString();  //在庫表より在庫文字列を取り出す
                    }
                    WebResponseCtrl.CreateResponse(Response, sendtext, sDownloadFileName);  //取り出した文字列をダウンロード
                }
                else
                {
                    WebResponseCtrl.WriteResponse(Response, "データがありません。");
                    return;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //施設切り替え
        protected void SSListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            sitename = SSListBox.Text;      //選択された施設名取り出し
            GetCompanyProfile();

            //減警報のみ、データ無しのみ表示モードは解除する。
            showwrgonly = (bool)Session[GlobalVar.SDispWrgOnly];
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            Session[GlobalVar.SDispWrgOnly] = showwrgonly = false;
            ShowLowInvSitesBtn.Text = GlobalVar.ShowWrgSites;
            string prefname = PrefListBox1.Text.TrimEnd(); //県名

            //選択された施設に対するデータテーブルを開く
            DataUnitTable dttable = new DataUnitTable();
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);  //指定ユーザーの指定県名に該当するSS一覧取り出し
            if (sitename == GlobalVar.AllSites)
            {
                Session[GlobalVar.SSiteNo] = sitenum = 0;
                Session[GlobalVar.SSelectSite] = Session[GlobalVar.SSkkcode] = skkcode = dttable.GetSSCode(0);
            }
            else
            {
                Session[GlobalVar.SSiteNo] = sitenum = dttable.GetSSCodeBySitenum(sitename);
                Session[GlobalVar.SSelectSite] = Session[GlobalVar.SSkkcode] = skkcode = dttable.GetSSCodeBySitename(sitename);
            }
            int numpage = (sitenum - 1) / NUMSSPERPAGE;
            Session[GlobalVar.SIPageNo] = numpage;
            SetTableComponents();

            SetPrefListB(prefname, PrefListBox1,true);           //県名一覧表示
            SetZaikoSelectLB(prefname, SSListBox, true);         //施設リストボックス表示
            SSListBox.Text = sitename;
            if (sitename == GlobalVar.AllSites)         　//全ての施設
            {
                if(prefname == GlobalVar.alljapan) //全国
                    NxtPageBtn.Enabled = PrvPageBtn.Enabled = true;
                else
                    NxtPageBtn.Enabled = PrvPageBtn.Enabled = false;
                ShowTableZ(sitenum, prefname, false);         //在庫画面表示
                RedrawTimerSet();           //再描画タイマーセット
            }
            else　　　　　　　　　　　　　　　　　　　　　//特定の施設
            {
                NxtPageBtn.Enabled = PrvPageBtn.Enabled = false;
                ShowTableZ(sitenum, prefname, true);         //在庫画面表示
                RedrawTimerDisable();
            }
            Page.DataBind();
            AdjustTableLayoutZ();
            SetDispSession(GlobalVar.TabTableNo);
        }

        //次ページボタンクリック時
        protected void NxtPageBtn_Click(object sender, EventArgs e)
        {
            GetCompanyProfile();
            invpageno = (int)Session[GlobalVar.SIPageNo];
            string prefname = PrefListBox1.Text.TrimEnd();　//県名
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            DataUnitTable dttable = new DataUnitTable();    //現在のアカウントで表示可能なSS一覧取得
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname); //指定ユーザー、指定県名でのSS一覧取り出し
            int numsite = dttable.GetNumOfRecord();
            int numpage = (numsite - 1) / NUMSSPERPAGE; //SSの数より合計ページ計算
            bool blast = false;
            invpageno = invpageno + 1;              //次ページ設定
            if (invpageno > numpage)                //合計ページを超えたときはエラー処理
            {
                blast = true;
                invpageno -= 1;
            }
            Session[GlobalVar.SIPageNo] = invpageno;
            Session[GlobalVar.SSiteNo] = sitenum = NUMSSPERPAGE * invpageno;
            Session[GlobalVar.SSiteName] = sitename = dttable.GetSSName(sitenum);
            Session[GlobalVar.SSkkcode] = skkcode = dttable.GetSSCode(sitenum);

            SetTableComponents();
            SetPrefListB(prefname, PrefListBox1,true);           //県名一覧表示
            SetZaikoSelectLB(prefname, SSListBox, true);         //施設リストボックス表示
            ShowTableZ(sitenum, prefname, false);        //表&グラフ表示
            Page.DataBind();
            AdjustTableLayoutZ();
            RedrawTimerSet();           //再描画タイマーセット
            SetDispSession(GlobalVar.TabTableNo);
            if( blast == true )
                WebResponseCtrl.WriteResponse(Response, "最後のページです。");
        }

        //前ページボタンクリック時
        protected void PrvPageBtn_Click(object sender, EventArgs e)
        {
            GetCompanyProfile();
            invpageno = (int)Session[GlobalVar.SIPageNo];
            string prefname = PrefListBox1.Text.TrimEnd();　//県名
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            DataUnitTable dttable = new DataUnitTable();    //現在のアカウントで表示可能なSS一覧取得
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname); //指定ユーザー、指定県名でのSS一覧取り出し
            bool bfirst = false;
            if (invpageno == 0)     //先頭のページの場合はエラー処理
                bfirst = true;
            else
                invpageno = invpageno - 1;  //ページ数を一つ減らす
            Session[GlobalVar.SIPageNo] = invpageno;
            Session[GlobalVar.SSiteNo] = sitenum = NUMSSPERPAGE * invpageno;    //ページの先頭SSの番号を計算
            Session[GlobalVar.SSiteName] = sitename = dttable.GetSSName(sitenum);
            Session[GlobalVar.SSkkcode] = skkcode = dttable.GetSSCode(sitenum);

            SetTableComponents();
            SetPrefListB(prefname, PrefListBox1,true);           //県名一覧表示
            SetZaikoSelectLB(prefname, SSListBox, true);         //施設リストボックス表示
            ShowTableZ(sitenum, prefname, false);        //表&グラフ表示
            Page.DataBind();
            AdjustTableLayoutZ();
            RedrawTimerSet();           //再描画タイマーセット
            SetDispSession(GlobalVar.TabTableNo);
            if( bfirst == true )
                WebResponseCtrl.WriteResponse(Response, "先頭のページです。");
        }

        //減警報施設のみ表示ボタンが押されたとき
        protected void ShowLowInvSitesBtn_Click(object sender, EventArgs e)
        {
            GetCompanyProfile();
            showwrgonly = (bool)Session[GlobalVar.SDispWrgOnly];
            if ((showwrgonly == null) || (showwrgonly == false))    //全ての施設表示中ならば警報発生施設のみ表示へ
            {
                showwrgonly = true;
                ShowLowInvSitesBtn.Text = GlobalVar.ShowAllSites;
                PrefListBox1.Enabled = SSListBox.Enabled = NxtPageBtn.Enabled = PrvPageBtn.Enabled = DownloadBtn.Enabled = false;
                PrefListBox1.Items.Clear();
                SSListBox.Items.Clear();
                RedrawTimerDisable(); //再描画無効に
            }
            else
            {                                       //警報発生施設のみ表示中ならばすべての施設表示へ
                showwrgonly = false;
                ShowLowInvSitesBtn.Text = GlobalVar.ShowWrgSites;
                SetDefSiteSkkcode(lvcprof, false);
                PrefListBox1.Enabled = SSListBox.Enabled = NxtPageBtn.Enabled = PrvPageBtn.Enabled = DownloadBtn.Enabled = true;
                SetPrefListB(prefname, PrefListBox1, true);           //県名一覧表示
                SetZaikoSelectLB(prefname, SSListBox, true);         //施設リストボックス表示
                RedrawTimerSet();　//再描画タイマーセット
            }
            Session[GlobalVar.SDispWrgOnly] = showwrgonly; 
            Session[GlobalVar.SPrefName] = prefname = GlobalVar.defprefname;
            Session[GlobalVar.SSiteNo] = sitenum = 0;
            SetTableComponents();
               
            ShowTableZ(sitenum, prefname, false);           //在庫履歴表、グラフ表示
            Page.DataBind();
            AdjustTableLayoutZ();
            AddMetaHead();
            Session[GlobalVar.SDisplayNo] = GlobalVar.TabTableNo;
        }

        //県名一覧画面のリストボックス設定
        protected void SetPrefListB(string prefname, ListBox PrefListBox1, bool incalljapan)
        {
            //県別リストボックス
            List<string> prefs = new List<string>(GlobalVar.prefnames);
            if (incalljapan == true)
                prefs.Insert(0, GlobalVar.alljapan);
            PrefListBox1.DataSource = prefs;
            PrefListBox1.Text = prefname;
        }

        //県名選択が変更になったとき
        protected void PrefListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCompanyProfile();
            ListBox list = (ListBox)sender;
            string prefname = list.SelectedValue; //選択されたタンク番号取り出し
            Session[GlobalVar.SPrefName] = prefname;
            Session[GlobalVar.SSiteNo] = sitenum = 0;
            Session[GlobalVar.SIPageNo] = invpageno = 0;
            showwrgonly = (bool)Session[GlobalVar.SDispWrgOnly];　            //減警報のみ、データ無しのみ表示モードは解除する。
            Session[GlobalVar.SDispWrgOnly] = showwrgonly = false;
            ShowLowInvSitesBtn.Text = GlobalVar.ShowWrgSites;
            DownloadBtn.Enabled = true;
            if (prefname == GlobalVar.alljapan)
                NxtPageBtn.Enabled = PrvPageBtn.Enabled = true;
            else
                NxtPageBtn.Enabled = PrvPageBtn.Enabled = false;
            SetTableComponents();
            SetPrefListB(prefname, PrefListBox1,true);           //県名一覧表示
            SetZaikoSelectLB(prefname, SSListBox, true);         //施設リストボックス表示
            ShowTableZ(sitenum, prefname, false);         //在庫画面表示
            Page.DataBind();
            AdjustTableLayoutZ();
            RedrawTimerSet();           //再描画タイマーセット
            SetDispSession(GlobalVar.TabTableNo);
        }

        //----------------------- タンク毎施設履歴表示画面処理 ------------------------------------------------
        protected void ShowSSTankHistory(string sitename, string skkcode, int Tankno)
        {
            try
            {
                foreach (string file in Directory.GetFiles(HttpContext.Current.Server.MapPath("./") + "C1WebChartTemp\\", "*.png"))
                {
                    File.Delete(file);
                }
                //タンク毎グラフ作成表示
                DrawGraph.ShowChartTankHistory(C1WebChartTH1, SelectTankLabel0, sitename, skkcode, Tankno, false);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //タンク番号変更時の再描画
        protected void SelectTankDDL_SelectIndexChanged(object sender, EventArgs e)
        {
            GetCompanyProfile();
            DropDownList list = (DropDownList)sender;
            skkcode = (string)Session[GlobalVar.SSkkcode];
            string tankno = list.SelectedValue; //選択されたタンク番号取り出し

            int tno = int.Parse(tankno);
            Session[GlobalVar.STankNo] = tno;
            skkcode = (string)Session[GlobalVar.SSkkcode];
            sitename = (string)Session[GlobalVar.SSiteName];//現在のSKKコード、施設名をSessionより取り出す
            SSNameLabelTH.Text = sitename + " " + skkcode;
            ShowSSTankHistory(sitename, skkcode, tno);  //選択されたタンク番号のグラフ表示
            Page.DataBind();
        }

        //施設一覧用リストボックス設定
        protected void SetDropDownList(ListBox ddlist, string ssname)
        {
            int idx = 0;
            string ssnmstr = "";
            ddlist.Items.Clear();
            DataUnitTable dttable = new DataUnitTable();
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            //ログインユーザーの参照可能なSSリストを取り出す
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);
            List<string> ssnames = dttable.GetSiteList();
            int i = 0;
            foreach (string ssnm in ssnames)    //SS名をドロップダウンリストに追加
            {
                ssnmstr = ssnm.TrimEnd();
                if (ssnmstr == ssname)
                    idx = i;
                ddlist.Items.Add(new ListItem(ssnmstr, i.ToString()));
                i++;
            }
            ddlist.SelectedIndex = idx;
        }

        //県名変更時の再表示
        protected void PrefListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = (ListBox)sender;
            string prefname = list.SelectedValue; //選択されたタンク番号取り出し
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            DataUnitTable dttable = new DataUnitTable();
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);
            sitename = dttable.GetSSName(0);
            skkcode = dttable.GetSSCode(0);    //選択された施設名、SKKコード取り出し
            if (skkcode == "") //施設見つからず
            {
                THSSListBox.Items.Clear();
                SSNameLabelTH.Text = "施設がありません。";
                SelectTankLabel0.Text = "";
                //THHisDateLabel.Text = "";
                C1WebChartTH1.Visible = LVDataDownloadBtn.Visible = false;
            }
            else
            {
                LVDataDownloadBtn.Visible = true;
                SetZaikoSelectLB(prefname, THSSListBox, false);         //施設リストボックス表示
                SiteDat = new SiteInfData();
                int tno = SiteDat.OpenTableSkkcode(skkcode);    //選択された施設のタンク数を取り出し
                SelectTankDDL.Items.Clear();
                for (int i = 1; i <= tno; i++)      //タンク番号リスト作成
                {
                    SelectTankDDL.Items.Add(new ListItem("タンク" + i.ToString(), i.ToString()));
                }
                SSNameLabelTH.Text = sitename + " " + skkcode;
                Session[GlobalVar.SSiteNo] = Session[GlobalVar.STankNo] = 1;
                Session[GlobalVar.SSiteName] = sitename;
                Session[GlobalVar.SSkkcode] = skkcode;
                ShowSSTankHistory(sitename, skkcode, 1);    //指定施設、SKKコードのグラフ作成、表示
            }
            Page.DataBind();
        }

        //施設名変更時の再表示
        protected void THSSListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = (ListBox)sender;
            string sitename = list.SelectedValue;
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            string prefname = PrefListBox2.SelectedValue; //選択されたタンク番号取り出し
            GetCompanyProfile();

            DataUnitTable dttable = new DataUnitTable();
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);
            skkcode = dttable.GetSSCodeBySitename(sitename);

            SiteDat = new SiteInfData();
            int tno = SiteDat.OpenTableSkkcode(skkcode);    //選択された施設のタンク数を取り出し
            SelectTankDDL.Items.Clear();
            for (int i = 1; i <= tno; i++)      //タンク番号リスト作成
            {
                SelectTankDDL.Items.Add(new ListItem("タンク" + i.ToString(), i.ToString()));
            }

            SSNameLabelTH.Text = sitename + " " + skkcode;
            Session[GlobalVar.STankNo] = 1;
            Session[GlobalVar.SSiteName] = sitename;
            Session[GlobalVar.SSkkcode] = skkcode;
            ShowSSTankHistory(sitename, skkcode, 1);    //指定施設、SKKコードのグラフ作成、表示
            Page.DataBind();
        }
        //在庫データダウンロードボタン

        protected void LVDataDownloadBtn_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            string sitename = (string)Session[GlobalVar.SSiteName];
            sitename = sitename.TrimEnd();

            string sDownloadFileName = sitename + dt.ToString("yyyyMMdd") + ".csv"; //ダウンロードファイル名設定
            try
            {
                skkcode = (string)Session[GlobalVar.SSkkcode];
                ZHistDat = new ZHistData();
                ZHistDat.OpenTableSkkcodeRecord(skkcode);   //指定Skkコードに対する在庫履歴オープン
                string sendtext = ZHistDat.GetInventroyString();    //ダウンロード用文字列作成

                WebResponseCtrl.CreateResponse(Response, sendtext, sDownloadFileName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //----------------------- 警報施設表示画面処理 ------------------------------------------------
        protected void ShowWarningSite()
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                ZaikoDataTable dtbl = new ZaikoDataTable(lvcprof.mrdefcode, User.Identity.Name, compname);
                dtbl.CreateWarningSSTable();        //現在のアカウントで警報発生中の施設テーブル作成
                int cnt = dtbl.GetNumOfRecordOfWarningTable();  //警報発生中の施設数
                if (cnt == 0)
                {
                    SSManageDateLabel.Text = "警報発生中の施設はありません。";
                    C1GridViewWrgSS1.DisplayVisible = false;
                }
                else
                {
                    SSManageDateLabel.Text = "警報発生中の施設";
                    C1GridViewWrgSS1.DataSource = dtbl.GetWrningTable();    //警報施設表表示
                    C1GridViewWrgSS1.DisplayVisible = true;
                    for (int i = 0; i < C1GridViewWrgSS1.Rows.Count; i++)   //警報情報を赤字で表示
                    {
                        C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow drow = C1GridViewWrgSS1.Rows[i];
                        C1GridViewWrgSS1.Rows[i].Cells[4].ForeColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected void AdjustTableLayoutSSMng()
        {
            for (int i = 0; i < C1GridViewWrgSS1.Rows.Count; i++)
            {
                C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow drow = C1GridViewWrgSS1.Rows[i];
                C1GridViewWrgSS1.Rows[i].Cells[2].ForeColor = Color.Red;
            }
        }

        //----------------------- 施設設定表示画面処理 ------------------------------------------------
        void ShowSSInfo(int ssno, string username)
        {
            int valset;
            companyno = (int)Session[GlobalVar.SCompanyNo];

            TankVolGrid.DisplayVisible = true;
            OilTypeGrid.DisplayVisible = true;
            LWVTB1.Visible = LWVTB2.Visible = LWVTB3.Visible = LWVTB4.Visible = LWVTB5.Visible = LWVTB6.Visible = LWVTB7.Visible = LWVTB8.Visible = LWVTB9.Visible = LWVTB10.Visible = true;
            Label19.Visible = Label20.Visible = Label22.Visible = Label23.Visible = Label24.Visible = Label25.Visible = Label26.Visible = Label27.Visible = Label28.Visible = Label29.Visible = Label30.Visible = Label31.Visible = true;
            PrevButton.Enabled = NextButton.Enabled = true;
            Label21.Visible = true;
            LWVRegButton.Visible = true;
            try
            {
                valset = (int)Session[GlobalVar.SValSet];
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                valset = GlobalVar.ValNoSet;
            }

            try
            {
                string prefname = Session[GlobalVar.SPrefName].ToString().TrimEnd();
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                DataUnitTable dttable = new DataUnitTable();　//指定会社番号に対応するSSデータテーブルを取り出し
                dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, username, prefname, compname);
                int sitenum = dttable.GetNumOfRecord();
                string sitename = dttable.GetSSName(ssno);
                Session[GlobalVar.SSiteName] = sitename;

                string skkcode = dttable.GetSSCode(ssno);
                Session[GlobalVar.SSkkcode] = skkcode;

                SSNameLB.Items.Clear();                         //SS名リストの作成
                for (int i = 0; i < dttable.GetNumOfRecord(); i++)
                {
                    SSNameLB.Items.Add(dttable.GetSSName(i));
                }
                SSNameLB.Text = sitename;
                SSCodeText.Text = skkcode;

                SiteDat = new SiteInfData();
                SiteDat.OpenTableSkkcode(skkcode);                      //指定SKKコードに対するSS情報の取り出し
                TankVolGrid.DataSource = SiteDat.GetCapaTable();   //タンク1-10の容量テーブル
                OilTypeGrid.DataSource = SiteDat.GetOilTypeTable();
                
                //減警報値初期表示
                LWVTB1.Text = SiteDat.GetLowWarningByTank(1).ToString();
                LWVTB2.Text = SiteDat.GetLowWarningByTank(2).ToString();
                LWVTB3.Text = SiteDat.GetLowWarningByTank(3).ToString();
                LWVTB4.Text = SiteDat.GetLowWarningByTank(4).ToString();
                LWVTB5.Text = SiteDat.GetLowWarningByTank(5).ToString();
                LWVTB6.Text = SiteDat.GetLowWarningByTank(6).ToString();
                LWVTB7.Text = SiteDat.GetLowWarningByTank(7).ToString();
                LWVTB8.Text = SiteDat.GetLowWarningByTank(8).ToString();
                LWVTB9.Text = SiteDat.GetLowWarningByTank(9).ToString();
                LWVTB10.Text = SiteDat.GetLowWarningByTank(10).ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                valset = GlobalVar.ValNoSet;
            }
        }

        protected string CheckVolStr(string volstr, int tanknum, SiteInfData SiteDat, int ssno)
        {
            string retstr = "";
            int vol;
            try
            {
                volstr = volstr.Replace(",", "");
                vol = int.Parse(volstr);
                retstr = vol.ToString("#,0");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                retstr = "0";
            }
            return retstr;
        }

        //前の施設ボタン
        protected void PrevButton_Click(object sender, EventArgs e)
        {
            GetCompanyProfile();
            int siteno = (int)Session[GlobalVar.SSiteNo];
            bool bfirst = false;
            if (siteno == 0)
                bfirst = true;
            Session[GlobalVar.SValSet] = GlobalVar.ValNoSet;
            if( siteno > 0 )
                siteno = siteno - 1;
            ShowSSInfo(siteno, User.Identity.Name);     //前の施設の施設情報表示
            Page.DataBind();
            Session[GlobalVar.SSiteNo] = siteno;
            Session[GlobalVar.SDisplayNo] = GlobalVar.TabSiteInfNo;
            Session[GlobalVar.SValSet] = GlobalVar.ValNoSet;
            AddMetaHead();
            if( bfirst == true )
                WebResponseCtrl.WriteResponse(Response, "先頭の施設です");
        }
        //後ろの施設ボタン
        protected void NextButton_Click(object sender, EventArgs e)
        {
            GetCompanyProfile();
            int siteno = (int)Session[GlobalVar.SSiteNo];
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            string prefname = PrefListBox3.SelectedValue; //選択されたタンク番号取り出し

            DataUnitTable dttable = new DataUnitTable();
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);
            int sitenum = dttable.GetNumOfRecord();
            bool blast = false;
            siteno = siteno + 1;
            if (siteno >= sitenum)
            {
                blast = true;
                siteno -= 1;
            }
            ShowSSInfo(siteno, User.Identity.Name); //次の施設の施設情報表示
            Page.DataBind();
            Session[GlobalVar.SSiteNo] = siteno;
            Session[GlobalVar.SValSet] = GlobalVar.ValNoSet;
            Session[GlobalVar.SDisplayNo] = GlobalVar.TabSiteInfNo;
            AddMetaHead();
            if( blast == true )
                WebResponseCtrl.WriteResponse(Response, "最後の施設です");
        }

        //県名表示リストボックス
        protected void PrefListBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox list = (ListBox)sender;
            string prefname = list.SelectedValue; //選択されたタンク番号取り出し
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            DataUnitTable dttable = new DataUnitTable();
            dttable.OpenTableSkkcodesMbl(lvcprof.mrdefcode, User.Identity.Name, prefname, compname);
            sitename = dttable.GetSSName(0);
            skkcode = dttable.GetSSCode(0);    //選択された施設名、SKKコード取り出し
            if (skkcode == "") //施設見つからず
            {
                SSNameLB.Items.Clear();
                SSCodeText.Text = "施設がありません。";
                OilTypeGrid.DisplayVisible = TankVolGrid.DisplayVisible = false;
                LWVTB1.Visible = LWVTB2.Visible = LWVTB3.Visible = LWVTB4.Visible = LWVTB5.Visible = LWVTB6.Visible = LWVTB7.Visible = LWVTB8.Visible = LWVTB9.Visible = LWVTB10.Visible = false;
                Label19.Visible = Label20.Visible = Label22.Visible = Label23.Visible = Label24.Visible = Label25.Visible = Label26.Visible = Label27.Visible = Label28.Visible = Label29.Visible = Label30.Visible = Label31.Visible = false;
                Label21.Visible = false;
                LWVRegButton.Visible = false;
                PrevButton.Enabled = NextButton.Enabled = false;
            }
            else
            {
                Session[GlobalVar.SPrefName] = prefname;
                Session[GlobalVar.SValSet] = GlobalVar.ValNoSet;
                ShowSSInfo(0, User.Identity.Name);         //選択された施設の施設情報表示
                Session[GlobalVar.SValSet] = GlobalVar.ValSet;
                Session[GlobalVar.SSiteNo] = 0;
            }
            Page.DataBind();
            Session[GlobalVar.SDisplayNo] = GlobalVar.TabSiteInfNo;
            AddMetaHead();
        }

        //施設変更リストボックス
        protected void SSNameLB_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetCompanyProfile();
            int siteno = SSNameLB.SelectedIndex;        //選択された施設の番号取り出し

            Session[GlobalVar.SValSet] = GlobalVar.ValNoSet;
            ShowSSInfo(siteno, User.Identity.Name);         //選択された施設の施設情報表示
            Session[GlobalVar.SValSet] = GlobalVar.ValSet;
            Session[GlobalVar.SSiteNo] = siteno;
            Page.DataBind();
            Session[GlobalVar.SDisplayNo] = GlobalVar.TabSiteInfNo;
            AddMetaHead();
        }

        //減警報更新ボタン
        protected void LWVRegButton_Click(object sender, EventArgs e)
        {
            string[] lwvalstr = new string[10];
            string skkcode;
            bool res;
            try
            {
                GetCompanyProfile();
                skkcode = (string)Session[GlobalVar.SSkkcode];
                SiteDat = new SiteInfData();           //現在のSKKコードに対するSS情報取り出し
                SiteDat.OpenTableSkkcode(skkcode);

                lwvalstr[0] = LWVTB1.Text;          //更新された減警報値読み出す
                lwvalstr[1] = LWVTB2.Text;
                lwvalstr[2] = LWVTB3.Text;
                lwvalstr[3] = LWVTB4.Text;
                lwvalstr[4] = LWVTB5.Text;
                lwvalstr[5] = LWVTB6.Text;
                lwvalstr[6] = LWVTB7.Text;
                lwvalstr[7] = LWVTB8.Text;
                lwvalstr[8] = LWVTB9.Text;
                lwvalstr[9] = LWVTB10.Text;
                for (int i = 1; i <= 10; i++)
                {
                    res = SiteDat.SetLowWarningbyTank(i, lwvalstr[i - 1]);    //施設テーブルに減警報値を設定
                    if (res == false)
                    {
                        WebResponseCtrl.WriteResponse(Response, "タンク" + i.ToString() + "の入力を確認してください");
                        return;
                    }
                }
                res = SiteDat.UpdateLowWarningValue(skkcode); //データベースに反映
                string message = (res == true) ? "下限値を更新しました" : "下限値の更新に失敗しました";
                WebResponseCtrl.WriteResponse(Response, "下限値を更新しました");
                Session[GlobalVar.SValSet] = GlobalVar.ValNoSet;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //ヘッダーボタン処理 --------------------------
        protected void AP8Button_Click(object sender, EventArgs e)
        {
            RedrawTimerDisable();
            Session[GlobalVar.DispType] = GlobalVar.TabsAP8;
            Session[GlobalVar.SDisplayNo] = 0;
            Session[GlobalVar.ADisplayNo] = 0;
            Session[GlobalVar.SAllAP8Chk] = "false";
            Response.Redirect("SkkAP8.aspx");
        }

        protected void SettingButton_Click(object sender, EventArgs e)
        {
            RedrawTimerDisable();
            Session[GlobalVar.DispType] = GlobalVar.TabsSettings;
            Session[GlobalVar.SDisplayNo] = 0;
            Session[GlobalVar.SAllAP8Chk] = "false";
            Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgDispSiteNo;
            Response.Redirect("SkkSettings.aspx");
        }

        protected void LogOutBtn_Click(object sender, EventArgs e)
        {
            Timer1.Enabled = false;
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            Response.Redirect("Account\\Login.aspx");
        }
        //会社番号、情報取り出し
        protected void GetCompanyProfile()
        {
            companyno = (int)Session[GlobalVar.SCompanyNo];
            lvcprof = new LeveCompanyProfile();
            lvcprof.OpenTableById(companyno);
        }

        //-------- セッション処理 -------------
        protected void GetSiteSkkcode(LeveCompanyProfile lcp)
        {
            try
            {
                sitename = (string)Session[GlobalVar.SSiteName];
                skkcode = (string)Session[GlobalVar.SSkkcode];
                sitenum = (int)Session[GlobalVar.SSiteNo];
                tankno = (int)Session[GlobalVar.STankNo];
                prefname = (string)Session[GlobalVar.SPrefName];
                invpageno = (int)Session[GlobalVar.SIPageNo];
            }
            catch (Exception ex)
            {
                SetDefSiteSkkcode(lcp, true);
            }
        }
        protected void SetDefSiteSkkcode(LeveCompanyProfile lcp, bool bSetdefpref)
        {
            Session[GlobalVar.SSiteNo] = sitenum = 0;
            Session[GlobalVar.STankNo] = tankno = 1;
            Session[GlobalVar.SPrefName] = prefname = GlobalVar.defprefname;
            Session[GlobalVar.SIPageNo] = invpageno = 0;
            Session[GlobalVar.SSelectSite] = GlobalVar.AllSites;
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();

            DataUnitTable dttable = new DataUnitTable();
            //現在の会社のログインアカウントに対するデータテーブルをオープン
            dttable.OpenTableSkkcodesMbl(lcp.mrdefcode, User.Identity.Name, prefname, compname);
            if (dttable.GetNumOfRecord() > 0)
            {
                Session[GlobalVar.SSiteName] = sitename = dttable.GetSSName(0);
                Session[GlobalVar.SSkkcode] = skkcode = dttable.GetSSCode(0);
                if( bSetdefpref == true ) //デフォルトの県名を設定する場合
                    Session[GlobalVar.SPrefName] = prefname = dttable.GetPrefname(0);
            }
            else
            {
                Session[GlobalVar.SSiteName] = sitename = GlobalVar.DefSiteName1;
                Session[GlobalVar.SSkkcode] = skkcode = GlobalVar.DefSkkcode1;
            }
        }
        protected void SetDispSession(int dispno)
        {
            Session[GlobalVar.SDisplayNo] = dispno;
            //Session[GlobalVar.SDispWrgOnly] = false;
            //Session[GlobalVar.SIPageNo] = invpageno = 0;
        }

        //----------------------- 液種毎施設履歴表示画面処理 ------------------------------------------------
        //コンポーネントの表示
        protected void ShowTableComponent(int num)
        {
            if ((num >= 0) && (num < NUMSSPERPAGE))
            {
                C1GVArr[num].DisplayVisible = true;
                C1WCArr[num].Visible = true;
                C1SPArr[num].DisplayVisible = true;
                SSNLArr[num].Visible = true;
            }
        }
        //コンポーネントの非表示
        protected void HideTableComponent(int num)
        {
            if ((num >= 0) && (num < NUMSSPERPAGE))
            {
                C1GVArr[num].DisplayVisible = false;
                C1WCArr[num].Visible = false;
                C1SPArr[num].DisplayVisible = false;
                SSNLArr[num].Visible = false;
            }
        }

        //表表示に必要なコンポーネントをセット
        protected void SetTableComponents()
        {
            C1GVArr = new C1.Web.Wijmo.Controls.C1GridView.C1GridView[NUMSSPERPAGE];
            C1WCArr = new C1.Web.C1WebChart.C1WebChart[NUMSSPERPAGE];
            C1SPArr = new C1.Web.Wijmo.Controls.C1Splitter.C1Splitter[NUMSSPERPAGE];
            SSNLArr = new Label[NUMSSPERPAGE];
            //site1
            C1GVArr[0] = C1GridViewZ1;
            C1WCArr[0] = C1WebChartZ1;
            C1SPArr[0] = C1SplitterZ1;
            SSNLArr[0] = SSNameLabelZ1;
            C1GVArr[0].DisplayVisible = false;
            C1WCArr[0].Visible = false;
            C1SPArr[0].DisplayVisible = false;
            SSNLArr[0].Visible = false;
            //site2
            C1GVArr[1] = C1GridViewZ2;
            C1WCArr[1] = C1WebChartZ2;
            C1SPArr[1] = C1SplitterZ2;
            SSNLArr[1] = SSNameLabelZ2;
            C1GVArr[1].DisplayVisible = false;
            C1WCArr[1].Visible = false;
            C1SPArr[1].DisplayVisible = false;
            SSNLArr[1].Visible = false;
            //site3
            C1GVArr[2] = C1GridViewZ3;
            C1WCArr[2] = C1WebChartZ3;
            C1SPArr[2] = C1SplitterZ3;
            SSNLArr[2] = SSNameLabelZ3;
            C1GVArr[2].DisplayVisible = false;
            C1WCArr[2].Visible = false;
            C1SPArr[2].DisplayVisible = false;
            SSNLArr[2].Visible = false;
            //site4
            C1GVArr[3] = C1GridViewZ4;
            C1WCArr[3] = C1WebChartZ4;
            C1SPArr[3] = C1SplitterZ4;
            SSNLArr[3] = SSNameLabelZ4;
            C1GVArr[3].DisplayVisible = false;
            C1WCArr[3].Visible = false;
            C1SPArr[3].DisplayVisible = false;
            SSNLArr[3].Visible = false;
            //site5
            C1GVArr[4] = C1GridViewZ5;
            C1WCArr[4] = C1WebChartZ5;
            C1SPArr[4] = C1SplitterZ5;
            SSNLArr[4] = SSNameLabelZ5;
            C1GVArr[4].DisplayVisible = false;
            C1WCArr[4].Visible = false;
            C1SPArr[4].DisplayVisible = false;
            SSNLArr[4].Visible = false;
            //site6
            C1GVArr[5] = C1GridViewZ6;
            C1WCArr[5] = C1WebChartZ6;
            C1SPArr[5] = C1SplitterZ6;
            SSNLArr[5] = SSNameLabelZ6;
            C1GVArr[5].DisplayVisible = false;
            C1WCArr[5].Visible = false;
            C1SPArr[5].DisplayVisible = false;
            SSNLArr[5].Visible = false;
            //site7
            C1GVArr[6] = C1GridViewZ7;
            C1WCArr[6] = C1WebChartZ7;
            C1SPArr[6] = C1SplitterZ7;
            SSNLArr[6] = SSNameLabelZ7;
            C1GVArr[6].DisplayVisible = false;
            C1WCArr[6].Visible = false;
            C1SPArr[6].DisplayVisible = false;
            SSNLArr[6].Visible = false;
            //site8
            C1GVArr[7] = C1GridViewZ8;
            C1WCArr[7] = C1WebChartZ8;
            C1SPArr[7] = C1SplitterZ8;
            SSNLArr[7] = SSNameLabelZ8;
            C1GVArr[7].DisplayVisible = false;
            C1WCArr[7].Visible = false;
            C1SPArr[7].DisplayVisible = false;
            SSNLArr[7].Visible = false;
            //site9
            C1GVArr[8] = C1GridViewZ9;
            C1WCArr[8] = C1WebChartZ9;
            C1SPArr[8] = C1SplitterZ9;
            SSNLArr[8] = SSNameLabelZ9;
            C1GVArr[8].DisplayVisible = false;
            C1WCArr[8].Visible = false;
            C1SPArr[8].DisplayVisible = false;
            SSNLArr[8].Visible = false;
            //site10
            C1GVArr[9] = C1GridViewZ10;
            C1WCArr[9] = C1WebChartZ10;
            C1SPArr[9] = C1SplitterZ10;
            SSNLArr[9] = SSNameLabelZ10;
            C1GVArr[9].DisplayVisible = false;
            C1WCArr[9].Visible = false;
            C1SPArr[9].DisplayVisible = false;
            SSNLArr[9].Visible = false;
            //site11
            C1GVArr[10] = C1GridViewZ11;
            C1WCArr[10] = C1WebChartZ11;
            C1SPArr[10] = C1SplitterZ11;
            SSNLArr[10] = SSNameLabelZ11;
            C1GVArr[10].DisplayVisible = false;
            C1WCArr[10].Visible = false;
            C1SPArr[10].DisplayVisible = false;
            SSNLArr[10].Visible = false;

            //site12
            C1GVArr[11] = C1GridViewZ12;
            C1WCArr[11] = C1WebChartZ12;
            C1SPArr[11] = C1SplitterZ12;
            SSNLArr[11] = SSNameLabelZ12;
            C1GVArr[11].DisplayVisible = false;
            C1WCArr[11].Visible = false;
            C1SPArr[11].DisplayVisible = false;
            SSNLArr[11].Visible = false;
            //site13
            C1GVArr[12] = C1GridViewZ13;
            C1WCArr[12] = C1WebChartZ13;
            C1SPArr[12] = C1SplitterZ13;
            SSNLArr[12] = SSNameLabelZ13;
            C1GVArr[12].DisplayVisible = false;
            C1WCArr[12].Visible = false;
            C1SPArr[12].DisplayVisible = false;
            SSNLArr[12].Visible = false;
            //site14
            C1GVArr[13] = C1GridViewZ14;
            C1WCArr[13] = C1WebChartZ14;
            C1SPArr[13] = C1SplitterZ14;
            SSNLArr[13] = SSNameLabelZ14;
            C1GVArr[13].DisplayVisible = false;
            C1WCArr[13].Visible = false;
            C1SPArr[13].DisplayVisible = false;
            SSNLArr[13].Visible = false;
            //site15
            C1GVArr[14] = C1GridViewZ15;
            C1WCArr[14] = C1WebChartZ15;
            C1SPArr[14] = C1SplitterZ15;
            SSNLArr[14] = SSNameLabelZ15;
            C1GVArr[14].DisplayVisible = false;
            C1WCArr[14].Visible = false;
            C1SPArr[14].DisplayVisible = false;
            SSNLArr[14].Visible = false;
            //site16
            C1GVArr[15] = C1GridViewZ16;
            C1WCArr[15] = C1WebChartZ16;
            C1SPArr[15] = C1SplitterZ16;
            SSNLArr[15] = SSNameLabelZ16;
            C1GVArr[15].DisplayVisible = false;
            C1WCArr[15].Visible = false;
            C1SPArr[15].DisplayVisible = false;
            SSNLArr[15].Visible = false;
            //site17
            C1GVArr[16] = C1GridViewZ17;
            C1WCArr[16] = C1WebChartZ17;
            C1SPArr[16] = C1SplitterZ17;
            SSNLArr[16] = SSNameLabelZ17;
            C1GVArr[16].DisplayVisible = false;
            C1WCArr[16].Visible = false;
            C1SPArr[16].DisplayVisible = false;
            SSNLArr[16].Visible = false;
            //site18
            C1GVArr[17] = C1GridViewZ18;
            C1WCArr[17] = C1WebChartZ18;
            C1SPArr[17] = C1SplitterZ18;
            SSNLArr[17] = SSNameLabelZ18;
            C1GVArr[17].DisplayVisible = false;
            C1WCArr[17].Visible = false;
            C1SPArr[17].DisplayVisible = false;
            SSNLArr[17].Visible = false;
            //site19
            C1GVArr[18] = C1GridViewZ19;
            C1WCArr[18] = C1WebChartZ19;
            C1SPArr[18] = C1SplitterZ19;
            SSNLArr[18] = SSNameLabelZ19;
            C1GVArr[18].DisplayVisible = false;
            C1WCArr[18].Visible = false;
            C1SPArr[18].DisplayVisible = false;
            SSNLArr[18].Visible = false;
            //site20
            C1GVArr[19] = C1GridViewZ20;
            C1WCArr[19] = C1WebChartZ20;
            C1SPArr[19] = C1SplitterZ20;
            SSNLArr[19] = SSNameLabelZ20;
            C1GVArr[19].DisplayVisible = false;
            C1WCArr[19].Visible = false;
            C1SPArr[19].DisplayVisible = false;
            SSNLArr[9].Visible = false;
            //site21
            C1GVArr[20] = C1GridViewZ21;
            C1WCArr[20] = C1WebChartZ21;
            C1SPArr[20] = C1SplitterZ21;
            SSNLArr[20] = SSNameLabelZ21;
            C1GVArr[20].DisplayVisible = false;
            C1WCArr[20].Visible = false;
            C1SPArr[20].DisplayVisible = false;
            SSNLArr[20].Visible = false;
            //site22
            C1GVArr[21] = C1GridViewZ22;
            C1WCArr[21] = C1WebChartZ22;
            C1SPArr[21] = C1SplitterZ22;
            SSNLArr[21] = SSNameLabelZ22;
            C1GVArr[21].DisplayVisible = false;
            C1WCArr[21].Visible = false;
            C1SPArr[21].DisplayVisible = false;
            SSNLArr[21].Visible = false;
            //site23
            C1GVArr[22] = C1GridViewZ23;
            C1WCArr[22] = C1WebChartZ23;
            C1SPArr[22] = C1SplitterZ23;
            SSNLArr[22] = SSNameLabelZ23;
            C1GVArr[22].DisplayVisible = false;
            C1WCArr[22].Visible = false;
            C1SPArr[22].DisplayVisible = false;
            SSNLArr[22].Visible = false;
            //site24
            C1GVArr[23] = C1GridViewZ24;
            C1WCArr[23] = C1WebChartZ24;
            C1SPArr[23] = C1SplitterZ24;
            SSNLArr[23] = SSNameLabelZ24;
            C1GVArr[23].DisplayVisible = false;
            C1WCArr[23].Visible = false;
            C1SPArr[23].DisplayVisible = false;
            SSNLArr[23].Visible = false;
            //site25
            C1GVArr[24] = C1GridViewZ25;
            C1WCArr[24] = C1WebChartZ25;
            C1SPArr[24] = C1SplitterZ25;
            SSNLArr[24] = SSNameLabelZ25;
            C1GVArr[24].DisplayVisible = false;
            C1WCArr[24].Visible = false;
            C1SPArr[24].DisplayVisible = false;
            SSNLArr[24].Visible = false;
            //site26
            C1GVArr[25] = C1GridViewZ26;
            C1WCArr[25] = C1WebChartZ26;
            C1SPArr[25] = C1SplitterZ26;
            SSNLArr[25] = SSNameLabelZ26;
            C1GVArr[25].DisplayVisible = false;
            C1WCArr[25].Visible = false;
            C1SPArr[25].DisplayVisible = false;
            SSNLArr[25].Visible = false;
            //site27
            C1GVArr[26] = C1GridViewZ27;
            C1WCArr[26] = C1WebChartZ27;
            C1SPArr[26] = C1SplitterZ27;
            SSNLArr[26] = SSNameLabelZ27;
            C1GVArr[26].DisplayVisible = false;
            C1WCArr[26].Visible = false;
            C1SPArr[26].DisplayVisible = false;
            SSNLArr[26].Visible = false;
        }

    }
}