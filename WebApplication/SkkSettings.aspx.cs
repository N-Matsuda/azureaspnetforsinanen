﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication;
using WebApplication.Models;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;

namespace WebApplication1
{
    public partial class SkkSettings : System.Web.UI.Page
    {
        int companyno;
        LeveCompanyProfile lvcprof;

        protected void Page_Load(object sender, EventArgs e)
        {
            int dspno;
            Session[GlobalVar.DispType] = GlobalVar.TabsSettings;
            TitleLB.Text = "設定";
            //現在のユーザーの会社名取り出し
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            SynaUserSite suser = new SynaUserSite();
            suser.OpenTableCompany(compname);
            try
            {
                if (suser.CheckAdmin(User.Identity.Name) == false)
                {
                    C1.Web.Wijmo.Controls.C1Tabs.C1TabPageCollection c1tabs = C1Tabs2.Pages;
                    c1tabs.RemoveAt(2);
                    c1tabs.RemoveAt(1);
                }
                dspno = (int)Session[GlobalVar.CDisplayNo];
            }
            catch (Exception ex)
            {
                dspno = GlobalVar.TabCfgDispSiteNo;
            }
            if (dspno == GlobalVar.TabCfgDispSiteNo) //表示施設
            {
                if (Page.IsPostBack == false)
                {
                    ShowDispSites();
                }
                Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgDispSiteNo;
            }
            else if (dspno == GlobalVar.TabCfgRmAccountNo) //アカウント削除
            {
                if (Page.IsPostBack == false)
                    ShowAccountList();
                Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgRmAccountNo;
            }
            else 
            {
                if (Page.IsPostBack == false)
                {
                    ShowCreAcc();
                }
                Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgCrAccountNo;
            }
            AddMetaHead();
        }
        //自動ログアウト設定ヘッダー追加
        protected void AddMetaHead()
        {
            System.Web.UI.HtmlControls.HtmlHead head = Page.Header;
            System.Web.UI.HtmlControls.HtmlMeta meta = new System.Web.UI.HtmlControls.HtmlMeta();

            meta.HttpEquiv = "Refresh";
            meta.Content = GlobalVar.logouttime + ";URL= Account/Login.aspx";
            head.Controls.Add(meta);
        }
        //------------- タブ変更 --------------------------
        protected void C1Tabs2_SelectedChanged(object sender, EventArgs e)
        {
            GetCompanyProfile();
            string pagestr = ((C1.Web.Wijmo.Controls.C1Tabs.C1Tabs)sender).SelectedPage.Text;
            if (pagestr == GlobalVar.TabCfgDispSite) //表示施設
            {
                ShowDispSites();
                Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgDispSiteNo;
            }
            else if (pagestr == GlobalVar.TabCfgRmAccount) //アカウント削除
            {
                ShowAccountList();
                Page.DataBind();
                Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgRmAccountNo;
            }
            else //アカウント作成
            {
                ShowCreAcc();
                Session[GlobalVar.CDisplayNo] = GlobalVar.TabCfgCrAccountNo;
            }
            AddMetaHead();
        }

        //---------------- 表示施設 --------------------------------------
        protected void ShowDispSites()
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaUserSite ust = new SynaUserSite();
                ust.OpenTableCompany(compname);              //現在のアカウントでの閲覧可否リスト作成表示
                AccountNameDDList.DataSource = ust.GetUserAccList(true, false);    //アカウント一覧リスト作成
                AccountNameDDList.Text = User.Identity.Name;

                ust.OpenTable(User.Identity.Name, compname, false);
                UserSSGrid.DataSource = ust.GetCheckTable();
                Page.DataBind();
                int numtbl = UserSSGrid.Rows.Count;
                for (int i = 0; i < numtbl; i++)                //閲覧可否リストのチェックボックスを有効に
                {
                    try
                    {
                        C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow grow = UserSSGrid.Rows[i];
                        CheckBox ctl = (CheckBox)grow.Cells[0].Controls[0];
                        ctl.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //ユーザー名変更
        protected void AccountNameDDList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                string accname = AccountNameDDList.SelectedValue;   //選択されたユーザー取り出し
                SynaUserSite ust = new SynaUserSite();
                ust.OpenTable(accname, compname, false);
                UserSSGrid.DataSource = ust.GetCheckTable();    //選択されたユーザーでの閲覧可否リスト作成
                Page.DataBind();
                int numtbl = UserSSGrid.Rows.Count;
                for (int i = 0; i < numtbl; i++)                //閲覧可否リストのチェックボックスを有効に
                {
                    try
                    {
                        C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow grow = UserSSGrid.Rows[i];
                        CheckBox ctl = (CheckBox)grow.Cells[0].Controls[0];
                        ctl.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //施設登録ボタン
        protected void RegButton_Click(object sender, EventArgs e)
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                int numtbl = UserSSGrid.Rows.Count;
                List<string> chksitear = new List<string>();
                string accname = AccountNameDDList.SelectedValue;   //選択されているアカウント取得
                SynaUserSite ust = new SynaUserSite();
                ust.OpenTable(accname, compname, false);
                DataTable dtb = ust.GetCheckTable();    //選択されたユーザーでの閲覧可否リスト作成

                for (int i = 0; i < numtbl; i++)        //閲覧可否リスト中のチェックされているチェックボックスのSKKコードをリストに取り出し
                {
                    C1.Web.Wijmo.Controls.C1GridView.C1GridViewRow grow = UserSSGrid.Rows[i];
                    CheckBox ctl = (CheckBox)grow.Cells[0].Controls[0];

                    if (ctl.Checked == true)
                    {
                        string skkcode = dtb.Rows[i][2].ToString().TrimEnd();
                        chksitear.Add(skkcode);
                    }
                }
                bool ret = SynaUserSite.RegTable(chksitear, accname);  //選択アカウントに対して、閲覧可能なSKKコードのリストを渡し登録する
                if( ret == true )
                {
                    WebResponseCtrl.WriteResponse(Response, "登録しました。");
                }
                else
                {
                    WebResponseCtrl.WriteResponse(Response, "登録できませんでした。");
                }
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            WebResponseCtrl.WriteResponse(Response, "登録できませんでした2。");
        }

        //アップロードボタン
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            // ファイルがアップロードされていなければリターン
            if (FileUpload1.HasFile == false )
            {
                WebResponseCtrl.WriteResponse(Response, "ファイルがありません。");
                return;
            }
            try
            {
                // 一時ファイルに保存
                string fpath = HttpContext.Current.Server.MapPath("./") + "C1WebChartTemp\\temp.csv";
                FileUpload1.SaveAs(fpath);

                SynaUserSite ust = new SynaUserSite();
                ust.LoadUploadedCSV(fpath);         //アップロードされたCSVファイルをファイルに取り込み
                bool res = ust.RegistUploadedCSV();            //ファイルを解析後、ユーザーの閲覧可能なリストDBを更新する。

                // 一時ファイルを削除
                File.Delete(fpath);
                ShowDispSites();
                if (res == true)
                {
                    WebResponseCtrl.WriteResponse(Response, "登録しました。");
                }
                else
                {
                    WebResponseCtrl.WriteResponse(Response, "登録できませんでした。");
                }
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            WebResponseCtrl.WriteResponse(Response, "登録できませんでした2。");
        }

        //ダウンロードボタン
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaUserSite ust = new SynaUserSite();
                ust.OpenTableCompany(compname);              //現在のアカウントでの閲覧可否リスト作成表示
                List<string> acclst = ust.GetUserAccList(true, false);    //アカウント一覧リスト作成
                string sendtext = "";

                //各アカウント毎に対応SSを取り出す
                int row = 0;
                foreach( string account in acclst )
                {
                    SynaUserSite synacc = new SynaUserSite();
                    synacc.OpenTable(account, compname, false);
                    DataTable dtble = synacc.GetCheckTable();
                    int numtble = dtble.Rows.Count;
                    if( row == 0)
                    {
                        sendtext += "アカウント,";
                        for(int i=0; i<numtble; i++)
                        {
                            string ssname = dtble.Rows[i][1].ToString().TrimEnd();
                            string skkcode = dtble.Rows[i][2].ToString().TrimEnd();
                            sendtext += ssname + "(" + skkcode + "),";
                        }
                        sendtext = sendtext.Substring(0, sendtext.Length - 1);
                        sendtext += "\r\n";
                    }
                    row += 1;
                    sendtext += account + "," + synacc.GetCheckString() + "\r\n";
                }
                string sDownloadFileName = "閲覧施設" + dt.ToString("yyyyMMdd") + ".csv"; //ダウンロードファイル名設定
                WebResponseCtrl.CreateResponse(Response, sendtext, sDownloadFileName);  //取り出した文字列をダウンロード
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            WebResponseCtrl.WriteResponse(Response, "ダウンロードできませんでした。");
        }

        //--------- アカウント登録タブ -----------------------------------
        protected void ShowCreAcc()
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaUserSite synu = new SynaUserSite();
                synu.OpenTableCompany(compname);
                string[] SiteArr = synu.GetUserAccList(true, false).ToArray();  //現在のアカウントで参照可能な施設リストを取り出す
                ExiAccLB.DataSource = SiteArr;      //アカウント一覧表
                Page.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected void AccRegButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Account/Register.aspx");
        }

        //----------------------- アカウント削除画面処理 ------------------------------------------------
        protected void ShowAccountList()
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaUserSite synu = new SynaUserSite();
                synu.OpenTableCompany(compname);
                string[] SiteArr = synu.GetUserAccList(false, true).ToArray();
                AccDelDDL.DataSource = SiteArr;         //削除アカウントリスト
                AccRegMngDDL.DataSource = SiteArr;      //権限変更用アカウントリスト

                string accname;
                try
                {
                    accname = (string)Session[GlobalVar.SAccName];
                    AccDelDDL.Text = accname;
                    AccRegMngDDL.Text = accname;
                }
                catch (Exception ex)
                {
                    accname = SiteArr[0];
                    Session[GlobalVar.SAccName] = accname;
                    AccDelDDL.Text = accname;
                    AccRegMngDDL.Text = accname;
                }
                Page.DataBind();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //削除ボタン
        protected void AccDelButton_Click(object sender, EventArgs e)
        {
            try
            {
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                string accname = AccDelDDL.SelectedValue;
                int pos = accname.IndexOf("(");
                accname = accname.Substring(0, pos);
                string curaccname = User.Identity.Name; //指定アカウントの取り出し
                if (accname == curaccname)
                {
                    WebResponseCtrl.WriteResponse(Response, "現在ログインしているアカウントは削除できません。");
                    return;
                }

                SynaUserSite synu = new SynaUserSite();
                synu.OpenTable(accname, compname, false);
                if( synu.CheckAdmin(accname) == true )
                {
                    WebResponseCtrl.WriteResponse(Response, "管理者アカウントは削除できません。");
                    return;
                }
                bool ret = synu.DelAccount(accname);     //アカウント削除
                synu.OpenTableCompany(compname);
                string[] SiteArr = synu.GetUserAccList(false, true).ToArray();
                if (SiteArr.Length > 0)
                {
                    accname = SiteArr[0];

                    AccDelDDL.DataSource = SiteArr;         //削除用アカウントリスト
                    AccDelDDL.Text = accname;
                    AccRegMngDDL.DataSource = SiteArr;      //権限変更用アカウントリスト
                    AccRegMngDDL.Text = accname;

                    Session[GlobalVar.SAccName] = accname;
                }
                Page.DataBind();
                if (ret == true)
                {
                    WebResponseCtrl.WriteResponse(Response, "削除しました。");
                }
                else
                {
                    WebResponseCtrl.WriteResponse(Response, "削除できませんでした。");
                }
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            WebResponseCtrl.WriteResponse(Response, "削除できませんでした2。");
        }

        //拠点管理者として設定
        protected void AccRegMngBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string accname = AccRegMngDDL.SelectedValue;    //指定アカウント取り出し
                int pos = accname.IndexOf("(");
                accname = accname.Substring(0, pos);
                string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
                SynaUserSite synu = new SynaUserSite();
                synu.OpenTableCompany(compname);
                bool ret = synu.UpgradeToSubAdmin(accname);
                string[] SiteArr = synu.GetUserAccList(false, true).ToArray();
                accname = SiteArr[0];

                AccDelDDL.DataSource = SiteArr;             //削除用アカウントリスト
                //AccDelDDL.Text = accname;
                AccRegMngDDL.DataSource = SiteArr;          //権限変更用アカウントリスト
                //AccRegMngDDL.Text = accname;

                Session[GlobalVar.SAccName] = accname;
                Page.DataBind();
                if (ret == true)
                {
                    WebResponseCtrl.WriteResponse(Response, "拠点管理者として設定しました。");
                }
                else
                {
                    WebResponseCtrl.WriteResponse(Response, "拠点管理者に設定できませんでした。");
                }
                return;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            WebResponseCtrl.WriteResponse(Response, "拠点管理者に設定できませんでした2。");
        }
        //会社番号、情報取り出し
        protected void GetCompanyProfile()
        {
            //companyno = (int)Session[GlobalVar.SCompanyNo];
            string compname = Session[GlobalVar.SCompanyName].ToString().TrimEnd();
            lvcprof = new LeveCompanyProfile();
            lvcprof.OpenTable(compname);
        }
        //---------------- ヘッダーボタン処理 ------------
        protected void SetLevevisionBtn_Click(object sender, EventArgs e)
        {
            Session[GlobalVar.DispType] = GlobalVar.TabsLevevision;
            Session[GlobalVar.SDisplayNo] = 0;
            Response.Redirect("SkkDefault.aspx");
        }

        protected void SetAP8Btn_Click(object sender, EventArgs e)
        {
            Session[GlobalVar.DispType] = GlobalVar.TabsAP8;
            Session[GlobalVar.SDisplayNo] = 0;
            Session[GlobalVar.ADisplayNo] = 0;
            Response.Redirect("SkkAP8.aspx");
        }

        protected void SettingLogoutBtn_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            Response.Redirect("Account\\Login.aspx");
        }

    }
}