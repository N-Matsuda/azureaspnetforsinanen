﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkkMaster.Master" AutoEventWireup="true" CodeBehind="SkkDefault.aspx.cs" Inherits="WebApplication.SkkDefault" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Tabs" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1AppView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Splitter" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.C1WebChart.4" namespace="C1.Web.C1WebChart" tagprefix="C1WebChart" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="Panel1" runat="server" BackColor="#102C44" ForeColor="#99CCFF" Height="30px">
          &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="TitleLB" runat="server" Text="Label" ForeColor="White" Width="600px"></asp:Label>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="AP8Button" runat="server" OnClick="AP8Button_Click" Text="ジムボーイ  " Height="28px" style="margin-top: 0px" Width="84px" />
          &nbsp;
          <asp:Button ID="SettingButton" runat="server" OnClick="SettingButton_Click" Text="  設定  " Height="28px" Width="84px" />
          &nbsp;
          <asp:Button ID="LogOutBtn" runat="server" OnClick="LogOutBtn_Click" Text="ログアウト" Height="28px" Width="84px" />
          　　　　　　　<br />
        <br />
    </asp:Panel>
    <br />
    <wijmo:C1Tabs ID="C1Tabs1" runat="server" AutoPostBack="True" Height="4200px" OnSelectedChanged="C1Tab1_SelectedChanged">
        <Pages>
            <wijmo:C1TabPage runat="server" Text="在庫一覧" StaticKey="" ID="C1Tabs1_Tab1">
                <asp:Label ID="Label32" runat="server" Font-Size="Large" Text="施設を選択すると表示をスクロールします。　都道府県名　"></asp:Label>
                　<asp:ListBox ID="PrefListBox1" runat="server" OnSelectedIndexChanged="PrefListBox1_SelectedIndexChanged" Rows="1" Width="150px" AutoPostBack="True"></asp:ListBox>
                &nbsp;&nbsp;
                <asp:Label ID="Label33" runat="server" Font-Size="Large" Text="施設選択　"></asp:Label>
                <asp:ListBox ID="SSListBox" runat="server" Rows="1" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="SSListBox_SelectedIndexChanged"></asp:ListBox>
                &nbsp;&nbsp;<br />
                <br />
                <asp:Button ID="ShowLowInvSitesBtn" runat="server" OnClick="ShowLowInvSitesBtn_Click" Text="減警報施設のみ表示" />
                　　　<asp:Button ID="NxtPageBtn" runat="server" OnClick="NxtPageBtn_Click" Text="次ページ" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="PrvPageBtn" runat="server" OnClick="PrvPageBtn_Click" Text="前ページ" />
                　　　<asp:Button ID="DownloadBtn" runat="server" OnClick="DownloadBtn_Click" Text="ダウンロード" />
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ1" runat="server" Font-Size="X-Large" Text="昭和機器工業SS  A000100001"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ1" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ1" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ1" runat="server" Height="289px" LastDesignUpdate="636795979208862492" Width="499px" ImageRenderMethod="File">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ2" runat="server" Font-Size="X-Large" Text="昭和機器工業SS2  A000100002"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ2" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ2" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ2" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304179308880966" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ3" runat="server" Font-Size="X-Large" Text="昭和機器工業SS3  A000100003"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ3" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ3" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ3" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ4" runat="server" Font-Size="X-Large" Text="昭和機器工業SS4  A000100004"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ4" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ4" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ4" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ5" runat="server" Font-Size="X-Large" Text="昭和機器工業SS5  A000100005"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ5" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ5" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ5" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ6" runat="server" Font-Size="X-Large" Text="昭和機器工業SS6  A000100006"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ6" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ6" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ6" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ7" runat="server" Font-Size="X-Large" Text="昭和機器工業SS7  A000100007"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ7" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ7" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ7" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664706677529010" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ8" runat="server" Font-Size="X-Large" Text="昭和機器工業SS8  A000100008"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ8" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ8" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ8" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636664709285661271" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ9" runat="server" Font-Size="X-Large" Text="昭和機器工業SS9  A000100009"></asp:Label>
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ9" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ9" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ9" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="636784886252079878" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ10" runat="server" Font-Size="X-Large" Text="昭和機器工業SS10  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ10" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ10" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ10" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ11" runat="server" Font-Size="X-Large" Text="昭和機器工業SS11  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ11" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ11" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ11" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ12" runat="server" Font-Size="X-Large" Text="昭和機器工業SS12  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ12" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ12" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ12" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ13" runat="server" Font-Size="X-Large" Text="昭和機器工業SS13  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ13" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ13" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ13" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ14" runat="server" Font-Size="X-Large" Text="昭和機器工業SS14  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ14" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ14" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ14" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ15" runat="server" Font-Size="X-Large" Text="昭和機器工業SS15  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ15" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ15" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ15" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ16" runat="server" Font-Size="X-Large" Text="昭和機器工業SS16  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ16" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ16" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ16" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ17" runat="server" Font-Size="X-Large" Text="昭和機器工業SS17  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ17" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ17" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ17" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ18" runat="server" Font-Size="X-Large" Text="昭和機器工業SS18  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ18" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ18" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ18" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ19" runat="server" Font-Size="X-Large" Text="昭和機器工業SS19  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ19" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ19" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ19" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ20" runat="server" Font-Size="X-Large" Text="昭和機器工業SS20  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ20" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ20" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ20" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ21" runat="server" Font-Size="X-Large" Text="昭和機器工業SS21  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ21" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ21" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ21" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ22" runat="server" Font-Size="X-Large" Text="昭和機器工業SS22  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ22" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ22" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ22" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ23" runat="server" Font-Size="X-Large" Text="昭和機器工業SS23  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ23" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ23" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ23" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ24" runat="server" Font-Size="X-Large" Text="昭和機器工業SS24  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ24" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ24" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ24" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304147894691969" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ25" runat="server" Font-Size="X-Large" Text="昭和機器工業SS25  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ25" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ25" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ25" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304182662279859" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ26" runat="server" Font-Size="X-Large" Text="昭和機器工業SS26  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ26" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ26" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ26" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304182662279859" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <asp:Label ID="SSNameLabelZ27" runat="server" Font-Size="X-Large" Text="昭和機器工業SS27  A000100009"></asp:Label>
                <br />
                <br />
                <br />
                <wijmo:C1Splitter ID="C1SplitterZ27" runat="server" Height="300px" ShowExpander="False" SplitterDistance="710" Width="1127px">
                    <Panel1>
                        <ContentTemplate>
                            <wijmo:C1GridView ID="C1GridViewZ27" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: -202px; left: -55px">
                                <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                                <Columns>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク番号" HeaderText="タンク" HtmlEncode="False" ReadOnly="True" Width="70px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="液種" HeaderText="液種" HtmlEncode="False" ReadOnly="True" Width="100px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="全容量" HeaderText="申請容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="在庫量" HeaderText="在庫量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" ConvertEmptyStringToNull="False" DataField="空間容量" HeaderText="空間容量(L)" HtmlEncode="False" ReadOnly="True" Width="140px">
                                    </wijmo:C1BoundField>
                                    <wijmo:C1BoundField AllowMoving="False" AllowSizing="False" DataField="タンク状況" HeaderText="状況" HtmlEncode="False" ReadOnly="True" Width="120px">
                                    </wijmo:C1BoundField>
                                </Columns>
                            </wijmo:C1GridView>
                        </ContentTemplate>
                    </Panel1>
                    <Panel2>
                        <ContentTemplate>
                            <C1WebChart:C1WebChart ID="C1WebChartZ27" runat="server" Height="289px" ImageRenderMethod="File" LastDesignUpdate="637304182662279859" Width="499px">
                                <serializer value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Top;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot; ChartType=&quot;Bar&quot; Use3D=&quot;False&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; Inverted=&quot;True&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5.5&quot; Min=&quot;0.5&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;26&quot; Min=&quot;0&quot; UnitMajor=&quot;2&quot; UnitMinor=&quot;1&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;2&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;North&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                            </C1WebChart:C1WebChart>
                        </ContentTemplate>
                    </Panel2>
                </wijmo:C1Splitter>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="C1Tabs1_Tab2" runat="server" StaticKey="" Text="在庫履歴（タンクごと）" TabIndex="1">
                <asp:Label ID="SSNameLabelTH" runat="server" BackColor="#CCFFFF" Font-Size="X-Large" Text="昭和機器工業 A000100001"></asp:Label>
                &nbsp;&nbsp;<asp:Label ID="Label34" runat="server" Text="都道府県名　"></asp:Label>
                &nbsp;<asp:ListBox ID="PrefListBox2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="PrefListBox2_SelectedIndexChanged" Rows="1" Width="150px"></asp:ListBox>
                　<asp:Label runat="server" Text="施設選択　" ID="Label2"></asp:Label>

                &nbsp;<asp:ListBox ID="THSSListBox" runat="server" AutoPostBack="True" OnSelectedIndexChanged="THSSListBox_SelectedIndexChanged" Rows="1" Width="250px"></asp:ListBox>
                &nbsp;&nbsp;
                <asp:Label ID="Label1" runat="server" Text="タンク　"></asp:Label>
                &nbsp;<asp:DropDownList ID="SelectTankDDL" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SelectTankDDL_SelectIndexChanged" Width="100px">
                </asp:DropDownList>
                <br />
                <br />
                <br />
                <asp:Label ID="SelectTankLabel0" runat="server" BackColor="#CCFFFF" Font-Size="X-Large" Text="半月間の推移（タンク毎)     タンク1 ハイオク(20kL)"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="LVDataDownloadBtn" runat="server" OnClick="LVDataDownloadBtn_Click" Text="ダウンロード" />
                <br />
                <br />
                <br />
                <C1WebChart:C1WebChart ID="C1WebChartTH1" runat="server" Height="221px" ImageRenderMethod="File" LastDesignUpdate="637387813673115467" Width="564px">
                    <Serializer Value="&lt;?xml version=&quot;1.0&quot;?&gt;
&lt;Chart2DPropBag Version=&quot;4.0.20161.147&quot;&gt;
  &lt;StyleCollection&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;BackColor=Transparent;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend&quot; ParentName=&quot;Legend.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control&quot; ParentName=&quot;Control.default&quot; StyleData=&quot;BackColor=Window;Border=Solid,ControlDark,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Header&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Legend.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;Wrap=False;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisX&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Center;AlignVert=Bottom;ForeColor=ControlDarkDark;Rotation=Rotate0;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY2&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Far;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate90;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Footer&quot; ParentName=&quot;Control&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Control.default&quot; ParentName=&quot;&quot; StyleData=&quot;BackColor=Control;Border=None,Transparent,1;ForeColor=ControlText;&quot; /&gt;
    &lt;NamedStyle Name=&quot;LabelStyleDefault&quot; ParentName=&quot;LabelStyleDefault.default&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area.default&quot; ParentName=&quot;Control&quot; StyleData=&quot;AlignVert=Top;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;PlotArea&quot; ParentName=&quot;Area&quot; StyleData=&quot;Border=None,Transparent,1;&quot; /&gt;
    &lt;NamedStyle Name=&quot;AxisY&quot; ParentName=&quot;Area&quot; StyleData=&quot;AlignHorz=Near;AlignVert=Center;ForeColor=ControlDarkDark;Rotation=Rotate270;&quot; /&gt;
    &lt;NamedStyle Name=&quot;Area&quot; ParentName=&quot;Area.default&quot; StyleData=&quot;Border=Solid,ControlDark,1;Rounding=10 10 10 10;&quot; /&gt;
  &lt;/StyleCollection&gt;
  &lt;ChartGroupsCollection&gt;
    &lt;ChartGroup Name=&quot;Group1&quot;&gt;
      &lt;DataSerializer DefaultSet=&quot;True&quot;&gt;
        &lt;DataSeriesCollection&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightPink&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightPink&quot; OutlineColor=&quot;DeepPink&quot; Shape=&quot;Box&quot; /&gt;
            &lt;SeriesLabel&gt;series 0&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;20;22;19;24;25&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightBlue&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightBlue&quot; OutlineColor=&quot;DarkBlue&quot; Shape=&quot;Dot&quot; /&gt;
            &lt;SeriesLabel&gt;series 1&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;8;12;10;12;15&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;LightGreen&quot; /&gt;
            &lt;SymbolStyle Color=&quot;LightGreen&quot; OutlineColor=&quot;DarkGreen&quot; Shape=&quot;Tri&quot; /&gt;
            &lt;SeriesLabel&gt;series 2&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;10;16;17;15;23&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
          &lt;DataSeriesSerializer&gt;
            &lt;LineStyle Color=&quot;Orchid&quot; /&gt;
            &lt;SymbolStyle Color=&quot;Orchid&quot; OutlineColor=&quot;DarkOrchid&quot; Shape=&quot;Diamond&quot; /&gt;
            &lt;SeriesLabel&gt;series 3&lt;/SeriesLabel&gt;
            &lt;X&gt;1;2;3;4;5&lt;/X&gt;
            &lt;Y&gt;16;19;15;22;18&lt;/Y&gt;
            &lt;DataTypes&gt;Single;Single;Double;Double;Double&lt;/DataTypes&gt;
            &lt;FillStyle /&gt;
            &lt;Histogram /&gt;
          &lt;/DataSeriesSerializer&gt;
        &lt;/DataSeriesCollection&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
    &lt;ChartGroup Name=&quot;Group2&quot;&gt;
      &lt;DataSerializer&gt;
        &lt;Highlight /&gt;
      &lt;/DataSerializer&gt;
    &lt;/ChartGroup&gt;
  &lt;/ChartGroupsCollection&gt;
  &lt;Header Compass=&quot;North&quot; Visible=&quot;False&quot; /&gt;
  &lt;Footer Compass=&quot;South&quot; Visible=&quot;False&quot; /&gt;
  &lt;Legend Compass=&quot;East&quot; Visible=&quot;False&quot; /&gt;
  &lt;ChartArea LocationDefault=&quot;-1, -1&quot; SizeDefault=&quot;-1, -1&quot; PlotLocation=&quot;-1, -1&quot; PlotSize=&quot;-1, -1&quot;&gt;
    &lt;Margin /&gt;
  &lt;/ChartArea&gt;
  &lt;Axes&gt;
    &lt;Axis Max=&quot;5&quot; Min=&quot;1&quot; UnitMajor=&quot;1&quot; UnitMinor=&quot;0.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;South&quot;&gt;
      &lt;Text&gt;Axis X&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;1&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;25&quot; Min=&quot;5&quot; UnitMajor=&quot;5&quot; UnitMinor=&quot;2.5&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;West&quot;&gt;
      &lt;Text&gt;Axis Y&lt;/Text&gt;
      &lt;GridMajor Visible=&quot;True&quot; Spacing=&quot;5&quot; /&gt;
    &lt;/Axis&gt;
    &lt;Axis Max=&quot;0&quot; Min=&quot;0&quot; UnitMajor=&quot;0&quot; UnitMinor=&quot;0&quot; AutoMajor=&quot;True&quot; AutoMinor=&quot;True&quot; AutoMax=&quot;True&quot; AutoMin=&quot;True&quot; Compass=&quot;East&quot;&gt;
      &lt;Text&gt;Axis Y2&lt;/Text&gt;
    &lt;/Axis&gt;
  &lt;/Axes&gt;
  &lt;AutoLabelArrangement /&gt;
  &lt;VisualEffectsData&gt;45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group1=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1;Group0=45,1,0.6,0.1,0.5,0.9,0,0,0.15,0,0,1,0.5,-25,0,0,0,1,64,1&lt;/VisualEffectsData&gt;
&lt;/Chart2DPropBag&gt;" />
                </C1WebChart:C1WebChart>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="C1Tabs1_Tab4" runat="server" StaticKey="SK3" Text="施設管理" TabIndex="2">
                <br />
                <asp:Label ID="Label15" runat="server" Font-Size="X-Large" Text="警報発生中の施設"></asp:Label>
                　　　　　　<asp:Label ID="SSManageDateLabel" runat="server" Text="警報発生中の施設なし"></asp:Label>
                <br />
                <br />
                <wijmo:C1GridView ID="C1GridViewWrgSS1" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                    <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                    <Columns>
                        <wijmo:C1BoundField DataField="施設名" HeaderText="施設名" Width="200px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="タンク" HeaderText="タンク" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="液種" HeaderText="液種" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="警報種類" HeaderText="警報種類" Width="300px">
                        </wijmo:C1BoundField>
                    </Columns>
                </wijmo:C1GridView>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="C1Tabs1_Tab5" runat="server" StaticKey="" Text="施設登録" TabIndex="3">
                <br />
                <asp:Label ID="Label16" runat="server" Font-Size="X-Large" Text="施設登録内容"></asp:Label>
                　　　　　<asp:Button ID="PrevButton" runat="server" OnClick="PrevButton_Click" Text=" 前の施設 " />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="NextButton" runat="server" OnClick="NextButton_Click" Text=" 次の施設 " />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <br />
                <br />
                <asp:Label ID="Label35" runat="server" Text="都道府県名　"></asp:Label>
                <asp:ListBox ID="PrefListBox3" runat="server" AutoPostBack="True" OnSelectedIndexChanged="PrefListBox3_SelectedIndexChanged" Rows="1" Width="150px"></asp:ListBox>
                　　　<asp:Label ID="Label17" runat="server" Text="施設選択　"></asp:Label>
                <asp:ListBox ID="SSNameLB" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SSNameLB_SelectedIndexChanged" Rows="1" Width="250px"></asp:ListBox>
                　　　　<asp:Label ID="Label18" runat="server" Text="施設コード　"></asp:Label>
                <asp:TextBox ID="SSCodeText" runat="server" ReadOnly="True"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label19" runat="server" Text="タンク容量"></asp:Label>
                <br />
                <wijmo:C1GridView ID="TankVolGrid" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                    <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                    <Columns>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ1" HeaderText="ﾀﾝｸ1" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ2" HeaderText="ﾀﾝｸ2" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ3" HeaderText="ﾀﾝｸ1" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ4" HeaderText="ﾀﾝｸ4" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ5" HeaderText="ﾀﾝｸ5" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ6" HeaderText="ﾀﾝｸ6" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ7" HeaderText="ﾀﾝｸ7" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ8" HeaderText="ﾀﾝｸ8" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ9" HeaderText="ﾀﾝｸ9" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ10" HeaderText="ﾀﾝｸ10" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                    </Columns>
                </wijmo:C1GridView>
                <br />
                <asp:Label ID="Label20" runat="server" Text="液種"></asp:Label>
                <br />
                <wijmo:C1GridView ID="OilTypeGrid" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: 1px; left: 0px">
                    <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                    <Columns>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ1" HeaderText="ﾀﾝｸ1" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ2" HeaderText="ﾀﾝｸ2" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ3" HeaderText="ﾀﾝｸ1" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ4" HeaderText="ﾀﾝｸ4" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ5" HeaderText="ﾀﾝｸ5" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ6" HeaderText="ﾀﾝｸ6" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ7" HeaderText="ﾀﾝｸ7" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ8" HeaderText="ﾀﾝｸ8" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ9" HeaderText="ﾀﾝｸ9" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="ﾀﾝｸ10" HeaderText="ﾀﾝｸ10" ReadOnly="True" Width="100px">
                        </wijmo:C1BoundField>
                    </Columns>
                </wijmo:C1GridView>
                <br />
                <asp:Label ID="Label21" runat="server" Text="減警報容量   　値変更後、右の登録ボタンをクリック"></asp:Label>
                &nbsp;&nbsp;
                <asp:Button ID="LWVRegButton" runat="server" OnClick="LWVRegButton_Click" Text="登録" />
                <br />
                <br />
                <asp:Label ID="Label22" runat="server" Text="タンク１減警報容量　　"></asp:Label>
                <asp:TextBox ID="LWVTB1" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 　<asp:Label ID="Label23" runat="server" Text="タンク2減警報容量　　"></asp:Label>
                <asp:TextBox ID="LWVTB2" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label24" runat="server" Text="タンク3減警報容量　　"></asp:Label>
                &nbsp;<asp:TextBox ID="LWVTB3" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label25" runat="server" Text="タンク4減警報容量　　"></asp:Label>
                <asp:TextBox ID="LWVTB4" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label26" runat="server" Text="タンク5減警報容量　 　"></asp:Label>
                <asp:TextBox ID="LWVTB5" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label27" runat="server" Text="タンク6減警報容量　　"></asp:Label>
                <asp:TextBox ID="LWVTB6" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label28" runat="server" Text="タンク7減警報容量　 　"></asp:Label>
                <asp:TextBox ID="LWVTB7" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label29" runat="server" Text="タンク8減警報容量　　"></asp:Label>
                <asp:TextBox ID="LWVTB8" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label30" runat="server" Text="タンク9減警報容量　 　"></asp:Label>
                <asp:TextBox ID="LWVTB9" runat="server"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label31" runat="server" Text="タンク10減警報容量　 "></asp:Label>
                <asp:TextBox ID="LWVTB10" runat="server"></asp:TextBox>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
    <p>
    </p>
    <p>
    </p>
    <p>
    </p>
<asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Enabled="False" />
</asp:Content>
