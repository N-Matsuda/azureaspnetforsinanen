﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace WebApplication.Models
{
    //シナネンの各ユーザーごとに表示可能なSKKコードを管理するクラス
    public class SynaUserSite
    {
        private DataTable SynaUserSiteDT;
        private DataTable CheckTableDT;
        private DataTable UploadTableDT;
        private const string stDisp = "表示";
        private const string stSkkcode = "SKKコード";
        private const string stSiteName = "施設名";

        //コンストラクター
        public SynaUserSite()
        {
            DataTableCtrl.InitializeTable(SynaUserSiteDT);
        }

        //新規ユーザー登録
        public void CreateNewuser(string username, string cmpname)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                //aspnet_UsersよりUserNameを削除するまえにUserIDを取り出します。
                SqlConnection con = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                string sqlstr;
                con.ConnectionString = GlobalVar.DBCONNECTION;
                con.Open();
                cmd.CommandText = "SELECT ユーザー名 FROM SynaUserSite WHERE ユーザー名='" + username + "'";
                cmd.Connection = con;

                // SQLを実行します。
                SqlDataReader reader = cmd.ExecuteReader();

                string uname = "";
                // 結果を表示します。
                while (reader.Read())
                {
                    uname = (string)reader.GetValue(0);
                }
                reader.Close();
                if (uname == "")
                {
                    sqlstr = "INSERT INTO SynaUserSite (ユーザー名,会社名,権限,サイトグループ) VALUES ('" + username + "','" + cmpname + "','" + GlobalVar.GeneralName + "','')";
                    cmd = new SqlCommand(sqlstr, con);
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //会社毎のユーザーリストを取り出す 
        public void OpenTableCompany(string compname)
        {
            try
            {
                //ユーザー名に対応したSKKコードリストを取り出す。
                string sqlstr = "SELECT ユーザー名,会社名,権限,サイトグループ FROM SynaUserSite WHERE 会社名='" + compname + "'";
                DataTableCtrl.InitializeTable(SynaUserSiteDT);
                SynaUserSiteDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynaUserSiteDT);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //SKKコードリストリスト取得
        public List<string> GetSkkcodeList()
        {
            List<string> list = new List<string>();
            try
            {
                if (SynaUserSiteDT != null)
                {
                    for (int i = 0; i < SynaUserSiteDT.Rows.Count; i++)
                    {
                        list.Add(SynaUserSiteDT.Rows[i][1].ToString().TrimEnd());
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }

        //指定された会社、ユーザーの全てのSSに対して表示する。しないを表すテーブル(CheckTableDT)作成
        public List<string> OpenTable(string username, string compname, bool AP8only)
        {
            List<string> stringList = new List<string>();
            try
            {
                //ユーザー名に対応したSKKコードリストを取り出す。
                string sqlstr = "SELECT ユーザー名,会社名,権限,サイトグループ FROM SynaUserSite WHERE ユーザー名  ='" + username + "'";

                DataTableCtrl.InitializeTable(SynaUserSiteDT);
                SynaUserSiteDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynaUserSiteDT);

                string unamestr = SynaUserSiteDT.Rows[0][3].ToString();
                string[] chkdskkcode = new string[0];
                if (unamestr != "")
                {
                    chkdskkcode = unamestr.Split(',');
                }
                int numcode = chkdskkcode.Length;
                for(int i=0; i<numcode; i++)
                {
                    try
                    {
                        string scode = chkdskkcode[i].ToString().Substring(0, 10);
                        stringList.Add(scode);
                    } catch(Exception ex )
                    {
                        Debug.WriteLine(ex.ToString());
                        continue;
                    }
                }

                CheckTableDT = new DataTable();
                CheckTableDT.Columns.Add(new DataColumn(stDisp, typeof(bool)));
                CheckTableDT.Columns.Add(new DataColumn(stSiteName, typeof(string)));
                CheckTableDT.Columns.Add(new DataColumn(stSkkcode, typeof(string)));

                //全ての施設データを取り出す
                if (compname == "*")
                    return stringList;

                string[] ssname;
                string[] skkcode;
                if (AP8only == false) //AP8に限らず全てのSSを取り出す
                {
                    LeveCompanyProfile lvcprof = new LeveCompanyProfile();                 //会社名に対応する会社情報を開き会社番号を取り出す
                    lvcprof.OpenTable(compname);
                    string skkcd = lvcprof.mrdefcode;
                    DataUnitTable dtu = new DataUnitTable();
                    dtu.OpenTableSkkcode(skkcd);
                    ssname = dtu.GetSiteList().ToArray();
                    skkcode = dtu.GetSKKCodeList().ToArray();
                }
                else
                {
                    SynaSiteDat sysite = new SynaSiteDat();
                    sysite.OpenTable("*", "*", compname);
                    ssname = sysite.GetSiteList().ToArray();
                    skkcode = sysite.GetSKKCodeList().ToArray();
                }
                bool found = false;
                //各施設のSKKコードに対して、指定ユーザーのSKKコードリスト中にあれば、"stDisp"にtrueを、なければfalseをセット
                for (int i = 0; i < ssname.Length; i++, found = false)
                {
                    DataRow drow = CheckTableDT.NewRow();
                    drow[stSkkcode] = skkcode[i];
                    drow[stSiteName] = ssname[i];
                    for (int j = 0; j < chkdskkcode.Length; j++)
                    {
                        string chkskkcode = "";
                        try
                        {
                            chkskkcode = chkdskkcode[j].ToString().Substring(0,10);
                            if (skkcode[i] == chkskkcode )
                            {
                                found = true;
                                break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.ToString());
                            break;
                        }
                    }
                    if (found == true)
                        drow[stDisp] = true;
                    else
                        drow[stDisp] = false;
                    CheckTableDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return stringList;
        }

        //会社名取り出し
        public string GetCompanyName()
        {
            string compname = "";
            try
            {
                if (SynaUserSiteDT != null)
                {
                    compname = SynaUserSiteDT.Rows[0][1].ToString().TrimEnd();
                    return compname;
                }
                else
                {
                    return compname;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return compname;
            }
        }

        //User accountリスト取得
        public List<string> GetUserAccList(bool incAdmin, bool incPriv)
        {
            List<string> list = new List<string>();
            try
            {
                if (SynaUserSiteDT != null)
                {
                    for (int i = 0; i < SynaUserSiteDT.Rows.Count; i++)
                    {
                        if ((incAdmin == true) || (SynaUserSiteDT.Rows[i][2].ToString().TrimEnd() != GlobalVar.AdminName))
                        {
                            string userac = SynaUserSiteDT.Rows[i][0].ToString().TrimEnd();
                            if( incPriv == true )
                            {
                                string priv = SynaUserSiteDT.Rows[i][2].ToString().TrimEnd();
                                if (priv == GlobalVar.AdminName)
                                    userac += "(システム管理者)";
                                else if (priv == GlobalVar.SubAdminName)
                                    userac += "(拠点管理者)";
                                else
                                    userac += "(一般ユーザー)";
                            }
                            list.Add(userac);
                        }
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }

        //全てのSSに対して表示する。しないを表すテーブル(CheckTableDT)取り出し
        public DataTable GetCheckTable()
        {
            try
            {
                return CheckTableDT;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }
        }
        //全てのSSに対して表示する。しないを表す文字列を取得
        public string GetCheckString()
        {
            string retchar = "";
            try
            {
                int numrow = CheckTableDT.Rows.Count;
                for(int i = 0; i < numrow; i++ )
                {
                    if ((bool)CheckTableDT.Rows[i][0] == true)
                        retchar += "1,";
                    else
                        retchar += "0,";
                }
                retchar = retchar.Substring(0, retchar.Length - 1);
                return retchar;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return retchar;
            }
        }

        //指定ユーザーに対して登録可能なSKKコード一覧を指定し、登録する。
        public static bool RegTable(List<string> chksites, string username)
        {
            bool ret = true;
            try
            {
                string sitelst = "";
                bool firstss = true;
                //リストchksitesの全てのskkcodeを列挙する。
                foreach (string siten in chksites)
                {
                    if (firstss == true)
                    {
                        sitelst += siten;
                        firstss = false;
                    }
                    else
                    {
                        sitelst += "," + siten;
                    }
                }
                //列挙されたskkcodeを登録する
                string sqlstr = "UPDATE SynaUserSite SET サイトグループ = '" + sitelst + "' WHERE ユーザー名 ='" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ret = false;
            }
            return ret;
        }

        //ユーザー名とユーザー名でアクセスできる施設(SKKコード)の対応表(0:未対応 1:対応)をロードする。
        public bool LoadUploadedCSV(string path)
        {
            bool res = true;
            DataTableCtrl.InitializeTable(UploadTableDT);
            CsvData csv = new CsvData();
            UploadTableDT = csv.ReadCSV(path, ',', false);

            return res;
        }
        //上記でロードした対応表よりユーザーテーブルを作成して、DB更新
        public bool RegistUploadedCSV()
        {
            bool res = false;
            try
            {
                if (UploadTableDT == null)
                    return res;
                string uname;
                List<string> sitelist = new List<string>();
                List<string> chksite = new List<string>();
                //1行目よりSKKコード一覧を取得
                for (int i = 1; i < UploadTableDT.Columns.Count; i++)
                {
                    string skkcode = UploadTableDT.Rows[0][i].ToString();
                    int pos = skkcode.IndexOf("(");
                    skkcode = skkcode.Substring(pos + 1, 10);
                    sitelist.Add(skkcode);
                }

                //2行目以降は　ユーザー名 + 各SKKコードごとの 0(未対応), 1(対応)
                string[] sitear = sitelist.ToArray();
                res = true;
                for (int i = 1; i < UploadTableDT.Rows.Count; i++, chksite.Clear())
                {
                    uname = UploadTableDT.Rows[i][0].ToString();  //ユーザー名取得

                    //ここでunameが有効なアカウントか登録する処理必要あり？

                    for (int j = 1; j < UploadTableDT.Columns.Count; j++)
                    {
                        if (UploadTableDT.Rows[i][j].ToString() == "1")
                            chksite.Add(sitear[j - 1]);
                    }
                    res = RegTable(chksite, uname);
                    if (res == false)
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                res = false;
            }
            return res;
        }
        //管理者権限チェック
        public bool CheckAdminOrSubAdmin(string username)
        {
            bool badmin = false;
            if (CheckAdmin(username) == true)
                badmin = true;
            else if (CheckSubAdmin(username) == true)
                badmin = true;
            //else if (username.StartsWith("n-matsuda") == true)
            //    badmin = true;
            return badmin;
        }

        //主管理者権限チェック
        public bool CheckAdmin(string username)
        {
            bool badmin = false;
            try
            {
                if (SynaUserSiteDT != null)
                {
                    for (int i = 0; i < SynaUserSiteDT.Rows.Count; i++)
                    {
                        if (username == SynaUserSiteDT.Rows[i][0].ToString().TrimEnd())
                        {
                            string privname = SynaUserSiteDT.Rows[i][2].ToString().TrimEnd();
                            if (privname.IndexOf("PrimeAdmin") > 0)
                            {
                                badmin = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return badmin;
        }
        //サブ管理者権限チェック
        public bool CheckSubAdmin(string username)
        {
            bool badmin = false;
            try
            {
                if (SynaUserSiteDT != null)
                {
                    for (int i = 0; i < SynaUserSiteDT.Rows.Count; i++)
                    {
                        if (username == SynaUserSiteDT.Rows[i][0].ToString().TrimEnd())
                        {
                            string cmpname = SynaUserSiteDT.Rows[i][2].ToString().TrimEnd();
                            if (cmpname.IndexOf("SubAdmin") > 0)
                            {
                                badmin = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return badmin;
        }

        //地域(サブ)管理者に昇格する
        public bool UpgradeToSubAdmin(string username)
        {
            try
            {
                if (SynaUserSiteDT == null)
                {
                    return false;
                }
                for (int i = 0; i < SynaUserSiteDT.Rows.Count; i++) //DB　AspNetUserのPhoneNumberのところを地域管理者名の保存場所として使用する。
                {
                    if (SynaUserSiteDT.Rows[i][0].ToString().TrimEnd() == username)
                    {
                        string subadname = GlobalVar.SubAdminName;
                        SynaUserSiteDT.Rows[i][2] = subadname;
                        string sqlstr = "UPDATE SynaUserSite SET 権限= '" + subadname + "' WHERE ユーザー名 ='" + username + "'";
                        DBCtrl.ExecNonQuery(sqlstr);
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
        }

        //一般利用者に降格する
        public bool DegradeToUser(string username)
        {
            try
            {
                if (SynaUserSiteDT != null)
                {
                    for (int i = 0; i < SynaUserSiteDT.Rows.Count; i++)
                    {
                        if (SynaUserSiteDT.Rows[i][0].ToString().TrimEnd() == username)
                        {
                            string accname = GlobalVar.GeneralName;
                            SynaUserSiteDT.Rows[i][2] = accname;
                            string sqlstr = "UPDATE SynaUserSite SET 権限= '" + accname + "' WHERE UserName ='" + username + "'";
                            DBCtrl.ExecNonQuery(sqlstr);
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return false;
            }
        }

        public bool DelAccount(string accname)
        {
            bool ret = false;
            try
            {
                string sqlstr = "DELETE FROM SynaUserSite WHERE ユーザー名 ='" + accname + "' AND 権限 !='" + GlobalVar.AdminName + "'";
                DBCtrl.ExecNonQuery(sqlstr);

                sqlstr = "DELETE FROM AspNetUsers WHERE Email ='" + accname + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                return true;
            }
            catch ( Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return ret;
        }

    }
}