﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace WebApplication.Models
{
    public class WgMlSysSite
    {
        private DataSet WgMlSysSiteDS;
        private DataTable WgMlSysSiteDT;
        private DataTable CheckTableDT;
        private const string stWgMlSysSite = "メール警報システム施設データ";
        private const int COLSITENAME = 1;   //施設名
        private const int COLCMPYNAME = 2;   //会社名
        private const int COLSKKCODE = 3;   //SKKコード
        private const int COLLWMAIL = 4;    //減警報発生
        private const int COLATGWRGSTS = 5; //液面計警報状態
        private const int COLODCTSYSSTS = 6; //油漏えい検知警報状態
        private const int LKSYSSNSNO = 8;     //油漏えい検知システム１台当たりのセンサー数
        private const int LKSYSSNSSTSNO = 3;  //油漏えい検知システムセンサー当たりのステータスバイト数
        private const string stCom = "送信";
        private const string stSKKCode = "SKKコード";
        private const string stSiteName = "施設名";

        private string cmpnyname;   //会社名
        private string sitename;    //施設名
        byte[] atgwrgsts;           //液面計警報状態
        byte[] odctsyssts;          //油漏えい検知警報状態

        private const int TANKOFST = 20;
        //コンストラクター
        public WgMlSysSite()
        {
            if (WgMlSysSiteDS != null)
            {
                WgMlSysSiteDS.Clear();
                WgMlSysSiteDS.Tables.Clear();
                WgMlSysSiteDS.Dispose();
                WgMlSysSiteDS = null;
            }
        }

        //テーブル読み込み
        public bool OpenTableSite(string cmpname, string sname)
        {
            bool bret = true;
            try
            {
                sitename = sname;
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                if (((cmpname == "*") || (cmpname == "")) && ((sname == "*") || (sname == "")))
                    sqlstr = "SELECT * FROM WgMlSysSite";
                else if ((sname == "*") || (sname == ""))
                    sqlstr = "SELECT * FROM WgMlSysSite WHERE 会社名= '" + cmpname + "'";
                else
                    sqlstr = "SELECT * FROM WgMlSysSite WHERE 会社名= '" + cmpname + "' AND 施設名= '" + sname + "'";

                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysSiteDS != null)
                {
                    WgMlSysSiteDS.Clear();
                    WgMlSysSiteDS.Tables.Clear();
                    WgMlSysSiteDS.Dispose();
                    WgMlSysSiteDS = null;
                }
                WgMlSysSiteDS = new DataSet("WgMlSysSite");
                WgMlSysSiteDS.Tables.Add(stWgMlSysSite);
                WgMlSysSiteDT = WgMlSysSiteDS.Tables[stWgMlSysSite];

                dAdp.Fill(WgMlSysSiteDS, stWgMlSysSite);
                cn.Close();
            }
            catch (Exception ex)
            {
                bret = false;
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }

        public bool OpenTableSkkcode(string skkcode)
        {
            bool bret = true;
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                sqlstr = "SELECT * FROM WgMlSysSite WHERE SKKコード= '" + skkcode + "'";

                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysSiteDS != null)
                {
                    WgMlSysSiteDS.Clear();
                    WgMlSysSiteDS.Tables.Clear();
                    WgMlSysSiteDS.Dispose();
                    WgMlSysSiteDS = null;
                }
                WgMlSysSiteDS = new DataSet("WgMlSysSite");
                WgMlSysSiteDS.Tables.Add(stWgMlSysSite);
                WgMlSysSiteDT = WgMlSysSiteDS.Tables[stWgMlSysSite];

                dAdp.Fill(WgMlSysSiteDS, stWgMlSysSite);
                cn.Close();
            }
            catch (Exception ex)
            {
                bret = false;
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }

        public bool OpenTable(string cmpny, string sitenm)
        {
            bool bret = true;
            try
            {
                //string dbpath;
                cmpnyname = cmpny;

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                if (((cmpny == "*") || (cmpny == "")) && ((sitenm == "*") || (sitenm == "")))
                    sqlstr = "SELECT * FROM WgMlSysSite";
                else if ((sitenm == "*") || (sitenm == ""))
                    sqlstr = "SELECT * FROM WgMlSysSite WHERE 会社名= '" + cmpny + "'";
                else
                    sqlstr = "SELECT * FROM WgMlSysSite WHERE 会社名= '" + cmpny + "' AND 施設名= '" + sitenm + "'";

                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysSiteDS != null)
                {
                    WgMlSysSiteDS.Clear();
                    WgMlSysSiteDS.Tables.Clear();
                    WgMlSysSiteDS.Dispose();
                    WgMlSysSiteDS = null;
                }
                WgMlSysSiteDS = new DataSet("WgMlSysSite");
                WgMlSysSiteDS.Tables.Add(stWgMlSysSite);
                WgMlSysSiteDT = WgMlSysSiteDS.Tables[stWgMlSysSite];

                dAdp.Fill(WgMlSysSiteDS, stWgMlSysSite);
                cn.Close();
            }
            catch (Exception ex)
            {
                bret = false;
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }

        public void OpenCheckTable(string cmpname)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr = "SELECT * FROM WgMlSysSite WHERE 会社名= '" + cmpname + "'";
                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysSiteDS != null)
                {
                    WgMlSysSiteDS.Clear();
                    WgMlSysSiteDS.Tables.Clear();
                    WgMlSysSiteDS.Dispose();
                    WgMlSysSiteDS = null;
                }
                WgMlSysSiteDS = new DataSet("WgMlSysSite");
                WgMlSysSiteDS.Tables.Add(stWgMlSysSite);
                WgMlSysSiteDT = WgMlSysSiteDS.Tables[stWgMlSysSite];

                dAdp.Fill(WgMlSysSiteDS, stWgMlSysSite);
                cn.Close();

                CheckTableDT = new DataTable();
                CheckTableDT.Columns.Add(new DataColumn(stCom, typeof(bool)));
                CheckTableDT.Columns.Add(new DataColumn(stSiteName, typeof(string)));
                CheckTableDT.Columns.Add(new DataColumn(stSKKCode, typeof(string)));

                int lwm;
                for (int i = 0; i < WgMlSysSiteDT.Rows.Count; i++)
                {
                    DataRow drow = CheckTableDT.NewRow();
                    lwm = (int)WgMlSysSiteDT.Rows[i][COLLWMAIL];
                    if (lwm == 0)
                        drow[stCom] = false;
                    else
                        drow[stCom] = true;
                    drow[stSKKCode] = (string)WgMlSysSiteDT.Rows[i][COLSKKCODE];
                    drow[stSiteName] = (string)WgMlSysSiteDT.Rows[i][COLSITENAME];

                    CheckTableDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public DataTable GetCheckTable()
        {
            return CheckTableDT;
        }

        public bool UpdateRegLWMail(List<string> skkcodear, List<bool> chkar)
        {
            bool bret = true;
            try
            {
                string[] skkcodes = skkcodear.ToArray();
                bool[] chks = chkar.ToArray();
                int num = skkcodes.Count();
                string cnstr = GlobalVar.DBCONNECTION;
                string sqlstr;
                string skkcode;
                int lwm;

                for (int i = 0; i < num; i++)
                {
                    System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                    cn.Open();
                    System.Data.SqlClient.SqlCommand Com;
                    if (chks[i] == true)
                        lwm = 1;
                    else
                        lwm = 0;
                    skkcode = skkcodes[i];
                    sqlstr = "UPDATE WgMlSysSite SET 減警報メール= '" + lwm.ToString() + "'WHERE SKKコード= '" + skkcode + "'";
                    Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                    Com.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                bret = false;
            }
            return bret;
        }

        public void InsertNewSite(int idno, string sitename, string skkcode)
        {
            bool bret = true;
            try
            {
                idno = idno + 3;
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                sqlstr = "INSERT INTO WgMlSysSite (id, 施設名,会社名,SKKコード,減警報メール,液面計警報状態,油漏えい検知警報状態) VALUES('" + idno.ToString() + "','" + sitename + "','EMG','" + skkcode + "','0','000000000','000000000')";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                bret = false;
                Console.WriteLine(e.ToString());
            }
        }


        //警報ステータスのアップデート
        public bool UpdateATGWrgSts(string skkcode)
        {
            bool bret = true;
            try
            {
                string wrgstr = System.Text.Encoding.ASCII.GetString(atgwrgsts);
                string odstr = System.Text.Encoding.ASCII.GetString(odctsyssts);
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                sqlstr = "UPDATE WgMlSysSite SET 液面計警報状態= '" + wrgstr + "', 油漏えい検知警報状態= '" + odstr + "' WHERE SKKコード= '" + skkcode + "'";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                bret = false;
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //施設名リスト取得
        //会社名一覧取得
        public List<string> GetSiteNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < WgMlSysSiteDT.Rows.Count; i++)
            {
                list.Add(WgMlSysSiteDT.Rows[i][COLSITENAME].ToString().TrimEnd());
            }
            return list;
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return WgMlSysSiteDT.Rows.Count;
        }

        //SKKコード取得
        public string GetSKKCode(int lineno)
        {
            try
            {
                if ((WgMlSysSiteDT != null) && (lineno < WgMlSysSiteDT.Rows.Count))
                {
                    return WgMlSysSiteDT.Rows[lineno][COLSKKCODE].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "";
            }
        }

        //施設名取得
        public string GetSiteName(int lineno)
        {
            try
            {
                if ((WgMlSysSiteDT != null) && (lineno < WgMlSysSiteDT.Rows.Count))
                {
                    return WgMlSysSiteDT.Rows[lineno][COLSITENAME].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return "";
            }
        }

        //減警報メールありなし取得
        public bool GetWLMailStatus(int lineno)
        {
            bool ret = false;
            int gwml = 0;
            try
            {
                if ((WgMlSysSiteDT != null) && (lineno < WgMlSysSiteDT.Rows.Count))
                {
                    gwml = (int)WgMlSysSiteDT.Rows[lineno][COLLWMAIL];
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
            if (gwml == 0)
                return false;
            else
                return true;
        }

        //減警報メールありなし設定
        public void GetWLMailStatus(int lineno, bool wml)
        {
            try
            {
                if ((WgMlSysSiteDT != null) && (lineno < WgMlSysSiteDT.Rows.Count))
                {
                    if (wml == true)
                        WgMlSysSiteDT.Rows[lineno][COLLWMAIL] = 1;
                    else
                        WgMlSysSiteDT.Rows[lineno][COLLWMAIL] = 0;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //減警報メールありなしアップデート
        public void UpdateWLMailSts(int lineno)
        {
            try
            {
                string skkcode;
                int wml;
                if ((WgMlSysSiteDT != null) && (lineno < WgMlSysSiteDT.Rows.Count))
                {
                    skkcode = (string)WgMlSysSiteDT.Rows[lineno][COLSKKCODE];
                    wml = (int)WgMlSysSiteDT.Rows[lineno][COLLWMAIL];
                    string cnstr = GlobalVar.DBCONNECTION;
                    System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                    cn.Open();
                    System.Data.SqlClient.SqlCommand Com;
                    string sqlstr;
                    sqlstr = "UPDATE WgMlSysSite SET 減警報メール= '" + wml.ToString() + "' WHERE SKKコード= '" + skkcode + "'";
                    Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                    Com.ExecuteNonQuery();
                    cn.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //指定行の 液面計警報有無　液面計警報状態　油漏えい検知警報有無　油漏えい検知警報状態を取り出す
        public void OpenLine(int lineno)
        {
            try
            {
                if ((WgMlSysSiteDT != null) && (lineno < WgMlSysSiteDT.Rows.Count))
                {
                    atgwrgsts = Encoding.ASCII.GetBytes(WgMlSysSiteDT.Rows[lineno][COLATGWRGSTS].ToString().TrimEnd());
                    odctsyssts = Encoding.ASCII.GetBytes(WgMlSysSiteDT.Rows[lineno][COLODCTSYSSTS].ToString().TrimEnd());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の減警報状態を取り出す
        public bool ChkLowWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }
        //指定施設の現在行の減警報状態を取り出す
        public bool ChkLowWrgStsBySite(string skkcode, int tno)
        {
            bool bret = false;
            bool bfound = false;
            try
            {
                for (int i = 0; i < WgMlSysSiteDT.Rows.Count; i++)
                {
                    if (skkcode == WgMlSysSiteDT.Rows[i][COLSKKCODE].ToString().TrimEnd())
                    {
                        atgwrgsts = Encoding.ASCII.GetBytes(WgMlSysSiteDT.Rows[i][COLATGWRGSTS].ToString().TrimEnd());
                        bfound = true;
                        break;
                    }
                }
                if ((bfound == true) && (atgwrgsts[TANKOFST * tno] == 0x31))
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の満警報状態を取り出す
        public bool ChkHighWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 1] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の水検知警報状態を取り出す
        public bool ChkWtrWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 2] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のセンサー異常警報状態を取り出す
        public bool ChkSnsWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 3] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のオイルリーク警報状態を取り出す
        public bool ChkLkWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 4] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の測定下限警報状態を取り出す
        public bool ChkLowLimWrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 5] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-1限警報状態を取り出す
        public bool ChkLC1WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 6] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-4限警報状態を取り出す
        public bool ChkLC4WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 7] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-5限警報状態を取り出す
        public bool ChkLC5WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 8] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-7限警報状態を取り出す
        public bool ChkLC7WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 9] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-8限警報状態を取り出す
        public bool ChkLC8WrgSts(int tno)
        {
            bool bret = false;
            try
            {
                if (atgwrgsts[TANKOFST * tno + 10] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムのセンサー通信エラー状態を取り出す
        public bool ChkLkSysComErr(int Sysno, int Sensno)
        {
            bool bret = false;
            try
            {
                if (odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムの水検知エラー状態を取り出す
        public bool ChkLkSysWtrErr(int Sysno, int Sensno)
        {
            bool bret = false;
            try
            {
                if (odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 1] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムの油検知エラー状態を取り出す
        public bool ChkLkSysOilErr(int Sysno, int Sensno)
        {
            bool bret = false;
            try
            {
                if (odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 2] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の減警報状態を変更する
        public void ChgLowWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の満警報状態を変更する
        public void ChgHighWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 1] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 1] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の水検知警報状態を変更する
        public void ChgWtrWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 2] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 2] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のセンサー異常警報状態を変更する
        public void ChgSnsWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 3] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 3] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のオイルリーク警報状態を変更する
        public void ChgLkWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 4] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 4] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の測定下限警報状態を変更する
        public void ChgLowLimWrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 5] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 5] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-1限警報状態を変更する
        public void ChgLC1WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 6] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 6] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-4限警報状態を変更する
        public void ChgLC4WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 7] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 7] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-5限警報状態を変更する
        public void ChgLC5WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 8] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 8] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-7限警報状態を変更する
        public void ChgLC7WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 9] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 9] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-8限警報状態を変更する
        public void ChgLC8WrgSts(int tno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    atgwrgsts[TANKOFST * tno + 10] = 0x31;
                else
                    atgwrgsts[TANKOFST * tno + 10] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムのセンサー通信エラー状態を変更する
        public void ChgLkSysComErr(int Sysno, int Sensno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO] = 0x31;
                else
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムの水検知エラー状態を変更する
        public void ChgLkSysWtrErr(int Sysno, int Sensno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 1] = 0x31;
                else
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 1] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムの油検知エラー状態を変更する
        public void ChgLkSysOilErr(int Sysno, int Sensno, bool bnew)
        {
            try
            {
                if (bnew == true)
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 2] = 0x31;
                else
                    odctsyssts[(Sysno * LKSYSSNSNO + Sensno) * LKSYSSNSSTSNO + 2] = 0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }
}