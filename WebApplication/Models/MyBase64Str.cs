﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApplication.Models
{
    public class MyBase64Str
    {
        private Encoding enc;
        public MyBase64Str(string encStr)
        {
            enc = Encoding.GetEncoding(encStr);
        }

        public string Encode(string str)
        {
            return Convert.ToBase64String(enc.GetBytes(str));
        }

        public string Decode(string str)
        {
            return enc.GetString(Convert.FromBase64String(str));
        }

        public static byte[] Decode2Byte(string str)
        {
            return Convert.FromBase64String(str);
        }
    }
}