﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web;

namespace WebApplication.Models
{
    public class HuffmanComp
    {
        // バイトごとの出現回数を計算
        private int[] GetFrequency(byte[] bytes)
        {
            var freq = Enumerable.Repeat(0, 256).ToArray();
            foreach (var b in bytes)
            {
                freq[b]++;
            }
            return freq;
        }

        // ハフマンツリー生成
        private HaffmanNode GenerateHaffmanTree(int[] freq)
        {
            var nodeList = new List<HaffmanNode>();
            for (int i = 0; i < 256; i++)
            {
                if (freq[i] == 0) continue;
                var node = new HaffmanNode((byte)i, freq[i]);
                nodeList.Add(node);
            }

            // 残り1つになるまで処理をする
            while (nodeList.Count > 1)
            {
                // 先頭に最小値が来るようにソートする
                // ソートではなく2分ヒープを使うほうが効率的
                nodeList = nodeList.OrderBy(e => e.Frequency).ToList();

                // 出現頻度が最小値のノード2つを取り出す
                var left = nodeList[0];
                var right = nodeList[1];
                nodeList.RemoveAt(0);
                nodeList.RemoveAt(0);

                // 左右の出現頻度の和を持つノードを生成しキューに追加
                var node = new HaffmanNode(left, right);
                nodeList.Add(node);
            }

            // キューに残っているノードがハフマンツリーのルートノード
            var root = nodeList[0];

            // 1種類のデータしか出現しない場合はダミーノードを用意
            if (root.IsLeaf)
            {
                var dummyRoot = new HaffmanNode();
                dummyRoot.IsLeaf = false;
                dummyRoot.Left = root;
                dummyRoot.Right = root; // 左右にノードが必要なので
                return dummyRoot;
            }
            return root;
        }

        // 圧縮用ハフマンコード表生成
        private Dictionary<byte, bool[]> GetHaffmanCodeTable(HaffmanNode node)
        {
            var dict = new Dictionary<byte, bool[]>();
            for (int i = 0; i < 256; i++)
            {
                var b = (byte)i;
                dict[b] = GenerateHaffmanCode(node, b);
            }
            return dict;
        }

        // 指定データについてのハフマンコード生成
        private bool[] GenerateHaffmanCode(HaffmanNode node, byte symbol, List<bool> bits = null)
        {
            if (bits == null) bits = new List<bool>();
            if (node.IsLeaf && (node.Value == symbol))
            {
                // 葉ノードでデータが一致する場合
                return bits.ToArray();
            }

            if (node.Left != null)
            {
                // 左の子ノードがある場合、false(0) を追加して再帰呼び出し
                bits.Add(false);
                var result = GenerateHaffmanCode(node.Left, symbol, bits);
                if (result != null) return result;

                // 左の子ノードに見つからなかった場合、追加したデータを取消
                bits.RemoveAt(bits.Count - 1);
            }
            if (node.Right != null)
            {
                // 右の子ノードがある場合、true(1) を追加して再帰呼び出し
                bits.Add(true);
                var result = GenerateHaffmanCode(node.Right, symbol, bits);
                if (result != null) return result;

                // 右の子ノードに見つからなかった場合、追加したデータを取消
                bits.RemoveAt(bits.Count - 1);
            }
            return null; // 葉ノードで見つからなかった場合
        }

        // ハフマンツリー自体をビット化
        private bool[] ConvertHaffmanTreeToBits(HaffmanNode root)
        {
            var bits = new List<bool>();

            // スタックを使って深さ優先探索
            var stack = new Stack<HaffmanNode>();
            stack.Push(root);

            while (stack.Count > 0)
            {
                var node = stack.Pop();
                if (node.IsLeaf)
                {
                    // 葉ノードは true 出力
                    bits.Add(true);
                    // 実データ 8bit 出力
                    for (int i = 0; i < 8; i++)
                    {
                        bits.Add((node.Value >> 7 - i & 1) == 1);
                    }
                }
                else
                {
                    // 葉ノードではないので false 出力
                    bits.Add(false);
                    // 左から優先的に走査したいので右から積む
                    if (node.Right != null) stack.Push(node.Right);
                    if (node.Left != null) stack.Push(node.Left);
                }
            }

            return bits.ToArray();
        }

        // ハフマンツリーを再構成
        private HaffmanNode RegenerateHaffmanTree(ref IEnumerable<bool> bits)
        {
            if (bits.First())
            {
                var symbol = bits.Skip(1).BitsToByte();
                bits = bits.Skip(9); // 葉ノードフラグとデータ分読み飛ばし
                return new HaffmanNode() { Value = symbol, IsLeaf = true };
            }
            else
            {
                bits = bits.Skip(1); // 葉ノードフラグ読み飛ばし
                var left = RegenerateHaffmanTree(ref bits);
                var right = RegenerateHaffmanTree(ref bits);
                return new HaffmanNode(left, right);
            }
        }

        // 圧縮
        public void Encode(string inputFile, string outputFile)
        {
            // ファイル読み込み
            var inputBytes = File.ReadAllBytes(inputFile);

            // 出現回数(頻度)を計算
            var freq = GetFrequency(inputBytes);

            // ハフマンツリー生成
            var root = GenerateHaffmanTree(freq);
            DebugHaffmanTree(root, 0, "");

            // ハフマンコード表生成
            var haffmanDict = GetHaffmanCodeTable(root);
            DebugHaffmanTable(haffmanDict);

            // 書き込み対象のファイル
            using (var writer = new BinaryWriter(File.OpenWrite(outputFile)))
            {
                // 圧縮前のファイルサイズ(バイト単位)出力
                var size = BitConverter.GetBytes(inputBytes.Count());
                writer.Write(size);

                // ハフマンツリー自体をビット化(連結のための型にキャスト)
                IEnumerable<bool> treeBits = ConvertHaffmanTreeToBits(root);

                // ハフマン符号化したデータ
                var haffmanCodes = inputBytes.Select(b =>
                {
                    return haffmanDict[b];
                });

                // 書き込む全ビットデータからバイトデータ取得
                var allByte = GetAllBytes(treeBits.ToArray(), haffmanCodes);
                //writer.Write(allByte.ToArray());
                foreach (var bt in allByte)
                {
                    writer.Write(bt); // 1Byte毎の書き込みの方が速かった
                }
            }
        }

        // すべてのビットデータとして列挙
        private IEnumerable<byte> GetAllBytes(bool[] treeBits, IEnumerable<bool[]> codes)
        {
            int i = 0;
            byte result = 0;
            foreach (var bit in treeBits)
            {
                // 指定桁数について1を立てる
                if (bit) result |= (byte)(1 << 7 - i);

                if (i == 7)
                {
                    // 1バイト分で出力しビットカウント初期化
                    yield return result;
                    i = 0;
                    result = 0;
                }
                else
                {
                    i++;
                }
            }
            foreach (var bits in codes)
            {
                foreach (var bit in bits)
                {
                    // 指定桁数について1を立てる
                    if (bit) result |= (byte)(1 << 7 - i);

                    if (i == 7)
                    {
                        // 1バイト分で出力しビットカウント初期化
                        yield return result;
                        i = 0;
                        result = 0;
                    }
                    else
                    {
                        i++;
                    }
                }
            }

            // 8ビットに足りない部分も出力
            if (i != 0) yield return result;
        }

        // 復号
        public void Decode(string inputFile, string outputFile)
        {
            // ファイル読み込む(4バイト分とばしてビット化)
            var inputBytes = File.ReadAllBytes(inputFile);
            var bits = inputBytes.Skip(4).BytesToBits();

            // 圧縮前ファイルサイズ
            int size = BitConverter.ToInt32(inputBytes.Take(4).ToArray(), 0);

            // ハフマンツリーの再構成
            var root = RegenerateHaffmanTree(ref bits);
            //DebugHaffmanTree(root, 0, "");

            // 圧縮前のファイルサイズ分バイトデータ書き込み
            using (var writer = new BinaryWriter(File.OpenWrite(outputFile)))
            {
                int counter = 0;
                var node = root;
                foreach (var bit in bits.ToArray())
                {

                    if (node.IsLeaf)
                    {
                        // 見つかったらデータを出力し、ルートに戻す
                        writer.Write(node.Value);
                        counter++;
                        node = root;
                        if (counter >= size) break;
                    }
                    if (!node.IsLeaf)
                    {
                        // 葉ノードに到達するまで探す
                        if (bit == true)
                            node = node.Right;
                        else
                            node = node.Left;
                    }
                }
            }

        }

        // 復号2
        public string DecodeByteArr(byte[] inputstr)
        {
            string outstr = "";
            // ファイル読み込む(4バイト分とばしてビット化)
            var inputBytes = inputstr;
            var bits = inputBytes.Skip(4).BytesToBits();

            // 圧縮前ファイルサイズ
            int size = BitConverter.ToInt32(inputBytes.Take(4).ToArray(), 0);

            // ハフマンツリーの再構成
            var root = RegenerateHaffmanTree(ref bits);
            //DebugHaffmanTree(root, 0, "");

            // 圧縮前のファイルサイズ分バイトデータ書き込み
            int counter = 0;
            var node = root;
            foreach (var bit in bits.ToArray())
            {

                if (node.IsLeaf)
                {
                    // 見つかったらデータを出力し、ルートに戻す
                    outstr += node.Value;
                    counter++;
                    node = root;
                    if (counter >= size) break;
                }
                if (!node.IsLeaf)
                {
                    // 葉ノードに到達するまで探す
                    if (bit == true)
                        node = node.Right;
                    else
                        node = node.Left;
                }
            }
            return outstr;
        }

        // 動作確認用処理
        private void DebugHaffmanTable(Dictionary<byte, bool[]> table)
        {
            Console.WriteLine();
            Console.WriteLine($"DebugHaffmanTable");
            for (int i = 0; i < 256; i++)
            {
                if (table[(byte)i] != null)
                {
                    var code = table[(byte)i].Select(b => b ? '1' : '0').ToArray();
                    Console.WriteLine($"{i.ToString("X2")}: {new string(code)}");
                }
            }
        }
        private void DebugHaffmanTree(HaffmanNode node, int indent, string bitCode)
        {
            if (indent == 0)
            {
                Console.WriteLine();
                Console.WriteLine("DebugHaffmanTree");
            }
            var bits = new List<bool>();
            bits.AddRange(bits);

            int indent_ = indent > 0 ? indent - 1 : 0;
            var indentStr = new string(' ', 2 * indent_) + "  --";

            if (node.IsLeaf)
            {
                Console.WriteLine(indentStr + $"{node.Value.ToString("X2")}({bitCode})");
            }
            else
            {
                Console.WriteLine(indentStr + $"");
                if (node.Left != null)
                {
                    DebugHaffmanTree(node.Left, indent + 1, bitCode + "0");
                }
                if (node.Right != null)
                {
                    DebugHaffmanTree(node.Right, indent + 1, bitCode + "1");
                }
            }
        }
        public void DebugString(string str)
        {
            var bytes = Encoding.ASCII.GetBytes(str);
            var freq = GetFrequency(bytes);
            var root = GenerateHaffmanTree(freq);
            DebugHaffmanTree(root, 0, "");
            var table = GetHaffmanCodeTable(root);
            DebugHaffmanTable(table);

        }

        // ハフマンノードクラス
        public class HaffmanNode
        {
            public bool IsLeaf { get; set; }
            public byte Value { get; set; }
            public int Frequency { get; set; }
            public HaffmanNode Left { get; set; }
            public HaffmanNode Right { get; set; }

            // 末端のノードを生成するためのコンストラクタ
            public HaffmanNode(byte value, int frequency)
            {
                // データと出現頻度を保持する
                this.Value = value;
                this.Frequency = frequency;
                this.IsLeaf = true;
            }

            // 子ノードを持つ節を生成するためのコンストラクタ
            public HaffmanNode(HaffmanNode left, HaffmanNode right)
            {
                this.Left = left;
                this.Right = right;
                this.Frequency = left.Frequency + right.Frequency;
                this.IsLeaf = false;
            }
            public HaffmanNode() { }
        }
    }

    public static class Extentions
    {
        // bool[] => byte
        public static byte BitsToByte(this IEnumerable<bool> bits)
        {
            return bits.BitsToBytes().FirstOrDefault();
        }

        // bool[] => byte[]
        public static IEnumerable<byte> BitsToBytes(this IEnumerable<bool> bits)
        {
            int i = 0;
            byte result = 0;
            foreach (var bit in bits)
            {
                // 指定桁数について1を立てる
                if (bit) result |= (byte)(1 << 7 - i);

                if (i == 7)
                {
                    // 1バイト分で出力しビットカウント初期化
                    yield return result;
                    i = 0;
                    result = 0;
                }
                else
                {
                    i++;
                }
            }
            // 8ビットに足りない部分も出力
            if (i != 0) yield return result;
        }

        // byte[] => bool[]
        public static IEnumerable<bool> BytesToBits(this IEnumerable<byte> bytes)
        {
            foreach (var bt in bytes)
            {
                for (int i = 0; i < 8; i++)
                {
                    yield return ((bt >> 7 - i) & 1) == 1;
                }
            }
        }
    }
}