﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;

namespace WebApplication.Models
{
    //ASP.NETユーザーアカウント管理用クラス
    public class UserAccount
    {
        private DataTable UserAccDT;

        //コンストラクター
        public UserAccount()
        {
            DataTableCtrl.InitializeTable(UserAccDT);
        }

        //ASP.NETユーザーテーブル読み込み
        public string OpenTable()
        {
            string cmpname = "";
            try
            {
                string sqlstr = "SELECT AspNetUsers.UserName, AspNetUsers.PhoneNumber FROM AspNetUsers WHERE PhoneNumber = '" + GlobalVar.CompanyName2 + "'";
                DataTableCtrl.InitializeTable(UserAccDT);
                UserAccDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, UserAccDT);
                cmpname = GetCompanyName();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return cmpname;
        }

        //会社名取得
        public string GetCompanyName()
        {
            string cmpname = "";
            int idx = 0;
            try
            {
                cmpname = UserAccDT.Rows[0][1].ToString().TrimEnd();
                if ((idx = cmpname.IndexOf("PrimeAdmin")) > 0)
                    cmpname = cmpname.Substring(0, idx);
                else if ((idx = cmpname.IndexOf("SubAdmin")) > 0)
                    cmpname = cmpname.Substring(0, idx);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return cmpname;
        }

        //User accountリスト取得
        public List<string> GetUserAccList()
        {
            List<string> list = new List<string>();
            try
            {
                if (UserAccDT != null)
                {
                    for (int i = 0; i < UserAccDT.Rows.Count; i++)
                    {
                        list.Add(UserAccDT.Rows[i][0].ToString());
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }

        //ユーザーアカウント削除
        public bool DelAccount(string username)
        {
            bool retv = false;
            try
            {
                //aspnet_UsersよりUserNameを削除するまえにUserIDを取り出します。
                string sqlstr = "DELETE FROM AspNetUsers WHERE UserName='" + username + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                retv = true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return retv;
            }
            return retv;
        }

    }
}