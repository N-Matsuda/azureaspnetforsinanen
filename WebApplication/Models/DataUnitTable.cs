﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace WebApplication.Models
{
    //データテーブル作成クラス ゲートウェイユニット、モバイルルーターどちらから取得した在庫データも取り出せるようにするクラス
    public class DataUnitTable
    {
        private DataTable DataUnitDT;
        private DataTable DataUnitTempDT;
        private string cmpnyname;
        private const int SKKCodeColNo = 0;     //SKKコード
        private const int DataSw = 1;           //在庫データ書き込みSW
        private const int DataBuf1 = 2;         //在庫データ書き込みBuffer1
        private const int DataBuf2 = 3;         //在庫データ書き込みBuffer1
        private const int SSNameColNo = 4;     //SS名
        private const int PrefNameColNo = 5;   //県名

        //コンストラクター
        public DataUnitTable()
        {
            DataTableCtrl.InitializeTable(DataUnitDT);
        }

        //指定した会社名で登録されたモバイルルーター情報、Digi ConnectPort情報をテーブルに取得します。
        public void OpenTable(string cmpny, string username)
        {
            try
            {
                //string dbpath;
                cmpnyname = cmpny;
                DataTableCtrl.InitializeTable(DataUnitTempDT);
                DataUnitTempDT = new DataTable();

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr = "";

                if (cmpny == "*")
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable";
                else
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable WHERE 会社名=(N'" + cmpny + "')";
                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                dAdp.Fill(DataUnitTempDT);
                cn.Close();

                //取り出したテーブルより、指定のユーザーが参照できる施設のみを抜き出す。
                SynaUserSite ust = new SynaUserSite();
                List<string> sslistar = ust.OpenTable(username, "*", false);
                string[] sslist = sslistar.ToArray();       //指定ユーザーの参照施設のSSコードリストの取り出し
                int rownum = DataUnitTempDT.Rows.Count;
                int ssnum = sslist.Length;
                string skcode;

                DataUnitDT = new DataTable();
                DataUnitDT.Columns.Add(new DataColumn("SKKコード", typeof(string)));
                DataUnitDT.Columns.Add(new DataColumn("DataSw", typeof(int)));
                DataUnitDT.Columns.Add(new DataColumn("在庫データ1", typeof(string)));
                DataUnitDT.Columns.Add(new DataColumn("在庫データ2", typeof(string)));
                DataUnitDT.Columns.Add(new DataColumn("施設名", typeof(string)));
                DataUnitDT.Columns.Add(new DataColumn("県名", typeof(string)));

                for (int i = 0; i < ssnum; i++)
                {
                    skcode = sslist[i];
                    for (int j = 0; j < rownum; j++)
                    {
                        if (skcode == DataUnitTempDT.Rows[j][SKKCodeColNo].ToString().TrimEnd())
                        {
                            DataUnitDT.ImportRow(DataUnitTempDT.Rows[j]);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたSKKコードの配列で登録されたモバイルルーター情報、Digi ConnectPort情報をテーブルに取得します。
        public void OpenTableSkkcodes(string[] skkcodes)
        {
            try
            {
                DataTableCtrl.InitializeTable(DataUnitDT);
                DataUnitDT = new DataTable();
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                System.Data.SqlClient.SqlDataAdapter dAdp;
                foreach (string idc in skkcodes)
                {
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable WHERE SKKコード='" + idc + "'";
                    dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                    dAdp.Fill(DataUnitDT);
                }
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //指定されたSKKコードで登録されたモバイルルーター情報、Digi ConnectPort情報をテーブルに取得します。
        public void OpenTableSkkcode(string skkcode)
        {
            try
            {
                DataTableCtrl.InitializeTable(DataUnitDT);
                DataUnitDT = new DataTable();
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                System.Data.SqlClient.SqlDataAdapter dAdp;
                if( skkcode.Length == 10 )
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable WHERE SKKコード='" + skkcode + "'";
                else
                    sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable WHERE SKKコード LIKE '" + skkcode + "%'"; 
                dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                dAdp.Fill(DataUnitDT);
                cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //SKKコードの列(GWU, モバイルルーターのワイルドカード指定 4桁:相光石油 M012* A060*)からテーブルを開く
        public void OpenTableSkkcodesMbl(string mblc, string uname, string prefcname, string cmpname)
        {
            try
            {
                DataTableCtrl.InitializeTable(DataUnitTempDT);
                DataUnitTempDT = new DataTable();
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                System.Data.SqlClient.SqlDataAdapter dAdp;

                //SQLサーバー側の県名をnvcharで定義すると県名がShift-JISになり、上記のSQL文でSELECT検索できなくなるが、テーブルに取り出すと文字化けしていないため、以降の処理に使用できる。
                //とりあえず県名はnvcharで定義する。
                if (prefcname == "全国")
                {
                    if( mblc.Length < 10 )
                        sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable WHERE SKKコード LIKE '" + mblc + "%'";
                    else
                        sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable WHERE SKKコード ='" + mblc + "'";
                }
                else
                {
                    if( mblc.Length < 10)
                        sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable WHERE 県名=(N'" + prefcname + "') AND SKKコード LIKE '" + mblc + "%'";
                    else
                        sqlstr = "SELECT SKKコード,DataSw,在庫データ1,在庫データ2,施設名,県名 FROM MobileRouterTable WHERE 県名=(N'" + prefcname + "') AND SKKコード='" + mblc + "'";
                }
                dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                dAdp.Fill(DataUnitTempDT);
                //取り出したテーブルより、指定のユーザーが参照できる施設のみを抜き出す。
                SynaUserSite ust = new SynaUserSite();
                List<string> sslistar = ust.OpenTable(uname,"*", false);

                string[] sslist = sslistar.ToArray();
                int rownum = DataUnitTempDT.Rows.Count;
                int ssnum = sslist.Length;
                string skcode, prefname;
                bool bfound;

                DataUnitDT = new DataTable();
                DataUnitDT.Columns.Add(new DataColumn("SKKコード", typeof(string)));
                DataUnitDT.Columns.Add(new DataColumn("DataSw", typeof(int)));
                DataUnitDT.Columns.Add(new DataColumn("在庫データ1", typeof(string)));
                DataUnitDT.Columns.Add(new DataColumn("在庫データ2", typeof(string)));
                DataUnitDT.Columns.Add(new DataColumn("施設名", typeof(string)));
                DataUnitDT.Columns.Add(new DataColumn("県名", typeof(string)));

                if (GlobalVar.levetestmode == false)
                {
                    for (int i = 0; i < ssnum; i++)
                    {
                        bfound = false;
                        skcode = sslist[i];
                        for (int j = 0; j < rownum; j++)
                        {
                            if (skcode == DataUnitTempDT.Rows[j][SKKCodeColNo].ToString().TrimEnd())
                            {
                                //prefname = DataUnitTempDT.Rows[j][PrefNameColNo].ToString().TrimEnd();
                                //if( prefname == "福岡" )
                                DataUnitDT.ImportRow(DataUnitTempDT.Rows[j]);
                                break;
                            }
                        }
                    }
                }
                else //テスト用
                {
                    DataUnitDT = DataUnitTempDT.Copy();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            try
            {
                return DataUnitDT.Rows.Count;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return 0;
            }
        }

        //テーブル先頭の在庫文字列を取得
        public string GetZaikoString()
        {
            string zstr = "";
            try
            {
                if (DataUnitDT != null)
                {
                    if ((int)DataUnitDT.Rows[0][DataSw] == 0)
                    {
                        zstr = (string)DataUnitDT.Rows[0][DataBuf2].ToString().TrimEnd();
                    }
                    else
                    {
                        zstr = (string)DataUnitDT.Rows[0][DataBuf1].ToString().TrimEnd();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }

        //テーブル指定行の在庫文字列を取得
        public string GetZaikoStringLine(int lineno)
        {
            string zstr = "";
            try
            {
                if (DataUnitDT != null)
                {
                    if ((int)DataUnitDT.Rows[lineno][DataSw] == 0)
                    {
                        zstr = (string)DataUnitDT.Rows[lineno][DataBuf2].ToString().TrimEnd();
                    }
                    else
                    {
                        zstr = (string)DataUnitDT.Rows[lineno][DataBuf1].ToString().TrimEnd();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return zstr;
        }

        //テーブル指定行のSKKコードを取得
        public string GetSSCode(int lineno)
        {
            try
            {
                if ((DataUnitDT != null) && (lineno < DataUnitDT.Rows.Count))
                {
                    return DataUnitDT.Rows[lineno][SKKCodeColNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
        //テーブル指定行の県名を取得
        public string GetPrefname(int lineno)
        {
            try
            {
                if ((DataUnitDT != null) && (lineno < DataUnitDT.Rows.Count))
                {
                    return DataUnitDT.Rows[lineno][PrefNameColNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //施設名より相当するSKKコード取得
        public string GetSSCodeBySitename(string sitename)
        {
            string skkcode = "";
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++)
                    {
                        if (sitename == DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd())
                        {
                            skkcode = DataUnitDT.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return skkcode;
        }

        //施設名より相当する施設番号取得
        public int GetSSCodeBySitenum(string sitename)
        {
            int sitenum = 0;
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++, sitenum++)
                    {
                        if (sitename == DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd())
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return sitenum;
        }

        //指定した文字を含む施設を検索する。
        public string SearchSite(string sword)
        {
            string findname = "";
            string sitename = "";
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++)
                    {
                        sitename = DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd();
                        int idx = sitename.IndexOf(sword);
                        if (idx >= 0)
                        {
                            findname = sitename;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return findname;
        }

        //指定した施設番号のテーブル上の行数を返す
        public int GetSiteNo(string sitename)
        {
            string sname = "";
            int idx = 1;
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++, idx++)
                    {
                        sname = DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd();
                        if (sname == sitename)
                        {
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return idx;
        }

        //施設名より相当するSKKコード取得
        public string GetSSName(int lineno)
        {
            try
            {
                if ((DataUnitDT != null) && (lineno < DataUnitDT.Rows.Count))
                {
                    return DataUnitDT.Rows[lineno][SSNameColNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //SKKコードより相当するSS名取得
        public string GetSSNameBySkkcode(string skkcode)
        {
            string ssname = "";
            try
            {
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < DataUnitDT.Rows.Count; i++)
                    {
                        if (skkcode == DataUnitDT.Rows[i][SKKCodeColNo].ToString().TrimEnd())
                        {
                            ssname = DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd();
                        }
                    }
                    return ssname;
                }
                else
                {
                    return ssname;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return ssname;
            }
            return ssname;
        }

        //SKK名リスト取得
        public List<string> GetSiteList()
        {
            List<string> list = new List<string>();
            try
            {
                int cnt = DataUnitDT.Rows.Count;
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        list.Add(DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd());
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }

        //SKKコードリスト取得
        public List<string> GetSKKCodeList()
        {
            List<string> list = new List<string>();
            try
            {
                int cnt = DataUnitDT.Rows.Count;
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        list.Add(DataUnitDT.Rows[i][SKKCodeColNo].ToString().TrimEnd());
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }

        //指定した施設番号のテーブル上の行数を返す
        public int GetSiteNumber(string sitename)
        {
            int num = 0;
            int i;
            try
            {
                if (DataUnitDT != null)
                {
                    for (i = 0; i < DataUnitDT.Rows.Count; i++)
                    {
                        if (sitename == DataUnitDT.Rows[i][SSNameColNo].ToString().TrimEnd())
                        {
                            num = i;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return num;
        }

        //テーブルよりSKKコードリスト取得
        public List<string> GetSkkcodeList()
        {
            List<string> list = new List<string>();
            try
            {
                int cnt = DataUnitDT.Rows.Count;
                if (DataUnitDT != null)
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        list.Add(DataUnitDT.Rows[i][SKKCodeColNo].ToString().TrimEnd());
                    }
                    return list;
                }
                else
                {
                    return list;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return list;
            }
        }

    }
}