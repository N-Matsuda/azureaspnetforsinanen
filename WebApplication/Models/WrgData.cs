﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApplication.Models
{
    public class WrgData
    {
        private DataTable SynaWrgDataDT;

        public WrgData()
        {
            DataTableCtrl.InitializeTable(SynaWrgDataDT);
        }

        public void OpenTable()
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr = "SELECT * FROM SynaWrgData";
                DataTableCtrl.InitializeTable(SynaWrgDataDT);
                SynaWrgDataDT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynaWrgDataDT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //警報書き込み
        public static void WriteWrg(string sitename, string codeno, string status)
        {
            //新しいレコードの追加
            try
            {
                DateTime dt = DateTime.Now.ToLocalTime();
                string sqlstr = "INSERT INTO SynaWrgData (施設名,SKKコード,日時,警報) VALUES (N'" + sitename + "','" + codeno + "','" + dt.ToString("yyyyMMddHHmm") + "',N'" + status + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void DeleteOldLog()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            int hour = dt.Hour;
            if (hour < 23)  //23時以降に実施
                return;

            try
            {
                dt = dt.AddHours(-720); //テスト 30日前
                string sqlstr = "DELETE FROM SynaWrgData WHERE 日付 < '" + dt.ToString("yyMMddHHmm") + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

    }
}