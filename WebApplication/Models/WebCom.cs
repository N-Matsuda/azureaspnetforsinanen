﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Data;
using System.Diagnostics;
using System.Xml;
using System.Runtime.Serialization;


namespace WebApplication.Models
{
    //DIGIサーバーとHTTP通信を行うクラスです
    public class WebCom
    {
        private string iDigiRciUrl = "http://devicecloud.digi.com/ws/sci";
        private string iDigiDataUrl = "https://devicecloud.digi.com/ws/data/~/";
        private string iDigiFolder = "/LeveVision/";
        //private string iDigiFolder = "/xbee_echo/";
        //Digi Data URL
        private string iDigiAccount = "YasafumiidigiTakai";
        private string iDigiPasswd = "skkkai";
        //private string PrgFilename = "KignusLeveCloudTest.py";
        private string PrgFilename = "test.py";

        //指定デバイスIDのリアルタイム在庫を取得
        public string GetRtZaikoFile(string devid, string groupname)
        {
            var list = new List<string>();
            string fltext = "NO FILE";
            string fname = "";
            try
            {
                string url = iDigiDataUrl + groupname + "/00000000-00000000-" + devid + iDigiFolder;
                Uri u = new Uri(url);
                HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(u);
                //ログインユーザー名とパスワードを設定
                httpReq.Credentials = new System.Net.NetworkCredential(iDigiAccount, iDigiPasswd);
                httpReq.Timeout = 50000;
                httpReq.Method = "GET";
                httpReq.ContentType = "text/xml";
                HttpWebResponse response = (HttpWebResponse)httpReq.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    //受信結果はStreamより取得する
                    Stream HttpStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(HttpStream);
                    bool findfl = false;
                    string rcvtxt = reader.ReadToEnd();
                    int pos;
                    reader.Close();
                    HttpStream.Close();
                    response.Close();
                    if (rcvtxt.Length > 10)
                    {
                        string[] dirArray = rcvtxt.Split('<');
                        foreach (string DirItem in dirArray)
                        {
                            if (DirItem.StartsWith("exist:resource name="))
                            {
                                if ((pos = DirItem.IndexOf(".xml")) > 0)
                                {
                                    fname = DirItem.Substring(21, pos + 4 - 21);
                                    if (fname.StartsWith("R") == true)
                                    {
                                        findfl = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (findfl == true)
                        {
                            url = iDigiDataUrl + groupname + "/00000000-00000000-" + devid + iDigiFolder + "/" + fname;
                            u = new Uri(url);
                            httpReq = (HttpWebRequest)WebRequest.Create(u);
                            //ログインユーザー名とパスワードを設定
                            httpReq.Credentials = new System.Net.NetworkCredential(iDigiAccount, iDigiPasswd);
                            httpReq.Timeout = 50000;
                            httpReq.Method = "GET";
                            httpReq.ContentType = "text/xml";
                            response = (HttpWebResponse)httpReq.GetResponse();
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                //受信結果はStreamより取得する
                                HttpStream = response.GetResponseStream();
                                reader = new StreamReader(HttpStream);
                                fltext = reader.ReadToEnd();
                                reader.Close();
                                HttpStream.Close();

                                //リアルタイム在庫ファイルを削除する。
                                httpReq = (HttpWebRequest)WebRequest.Create(u);
                                httpReq.Credentials = new System.Net.NetworkCredential(iDigiAccount, iDigiPasswd);
                                httpReq.Method = "DELETE";
                                response = (HttpWebResponse)httpReq.GetResponse();
                                response.Close();
                            }
                            else
                            {
                                Console.WriteLine("ファイル取得失敗 " + response.StatusCode.ToString());
                                fltext = "ERROR";
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("ファイルなし ");
                        fltext = "NO FILE";
                    }
                }
                else
                {
                    Console.WriteLine("ファイル取得失敗 " + response.StatusCode.ToString());
                    fltext = "ERROR";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                if (ex.ToString().IndexOf("404") >= 0)
                {
                    fltext = "NO FILE";
                }
                else
                {
                    fltext = "ERROR";
                }
            }
            return fltext;
        }

        //DIGIサーバーに指定コマンドメッセージを送る
        private HttpWebRequest SendSCIReq(string pmessage)
        {
            Uri u = new Uri(iDigiRciUrl);
            byte[] postDataBytes = Encoding.ASCII.GetBytes(pmessage);
            HttpWebRequest httpReq = (HttpWebRequest)WebRequest.Create(u);
            //ログインユーザー名とパスワードを設定
            httpReq.Credentials = new System.Net.NetworkCredential(iDigiAccount, iDigiPasswd);
            httpReq.Timeout = 50000;
            httpReq.Method = "POST";
            httpReq.ContentType = "text/xml";
            //POST送信するデータの長さを指定
            httpReq.ContentLength = pmessage.Length;
            //データをPOST送信するためのStreamを取得
            Stream reqStream = httpReq.GetRequestStream();
            //送信するデータを書き込む
            reqStream.Write(postDataBytes, 0, pmessage.Length);
            reqStream.Close();
            return (HttpWebRequest)httpReq;
        }

        //DIGIサーバーに在庫取得コマンドを送る
        public bool PostRCIRequest(string devid)
        {
            string postmessage =
                "<sci_request version=\"1.0\">\r\n" +
                "  <send_message>\r\n" +
                "    <targets>\r\n" +
                "      <device id=\"00000000-00000000-" + devid + "\"/>\r\n" +
                "    </targets>\r\n" +
                "    <rci_request version = \"1.1\">\r\n" +
                "    <do_command target=\"rci_getLeveVisionData\"/>\r\n" +
                "    </rci_request>\r\n" +
                "  </send_message>\r\n" +
                "</sci_request>\r\n";

            bool ret = true;
            try
            {
                HttpWebRequest httpReq = SendSCIReq(postmessage);
                HttpWebResponse response = (HttpWebResponse)httpReq.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("RCI送信成功");
                }
                else
                {
                    Console.WriteLine("RCI送信失敗 " + response.StatusCode.ToString());
                    ret = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ret = false;
            }
            return ret;
        }

    }
}