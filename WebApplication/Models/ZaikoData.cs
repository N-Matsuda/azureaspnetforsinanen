﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using C1.Web.Wijmo.Controls;
using System.Data;
using System.Text;


namespace WebApplication.Models
{
    public class ZaikoData
    {
        private DataTable ZaikoDT;
        public DateTime zdate { get; set; }
        const int NUMTANK = 20; //最大タンク数
        const int OFFSHEAD = 10; //時間データサイズ
        const int OFFSTANK = 42; //1タンクあたりのサイズ

        //コンストラクタ
        public ZaikoData()
        {
            if (ZaikoDT != null)
            {
                ZaikoDT.Clear();
                ZaikoDT.Dispose();
                ZaikoDT = null;
            }
        }

        //在庫文字列を解析
        public void OpenZaiko(string zstr)
        {
            if (zstr.Length < OFFSHEAD + OFFSTANK)
                return;
            int iNumTank = 0;
            try
            {
                //時間文字列
                string dstr = zstr.Substring(0, OFFSHEAD);
                dstr = dstr.Insert(8, ":"); //ファイル名より抽出した日付を1207211410 → 2012/07/21 14:10 のようなフォーマットにする。
                dstr = dstr.Insert(6, " ");
                dstr = dstr.Insert(4, "/");
                dstr = dstr.Insert(2, "/");
                dstr = "20" + dstr;
                zdate = DateTime.Parse(dstr);
                ZaikoDT = new DataTable();
                ZaikoDT.Columns.Add(new DataColumn("液種1", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像1", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("液種2", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像2", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("液種3", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像3", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("液種4", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像4", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("液種5", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像5", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("液種6", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像6", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("液種7", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像7", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("液種8", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像8", typeof(string)));

                DataRow dr;
                string strwk, strwk2, olname;
                int capa;
                int inv;

                ZaikoStr zs = new ZaikoStr(zstr);
                zs.Analyze();

                for (int i = 0; i < zs.numtank; i++)       //最大タンク数分ループ
                {
                    dr = ZaikoDT.NewRow();
                    //タンク番号取得
                    strwk = zs.tanknolst[i];
                    //dr["タンク番号"] = int.Parse(strwk);
                    //dr["液種"] = "タンク" + int.Parse(strwk) + "<br />";
                    //液種
                    strwk = zs.lqtypelst[i] + zs.lqtypelst2[i];
                    olname = GetOilName(strwk);
                    dr["液種" + (i + 1).ToString()] = olname + "<br />";
                    //在庫量, タンク容量取得
                    inv = int.Parse(zs.vollst[i]);
                    strwk = inv.ToString("#,0") + "<br />";
                    dr["液種" + (i + 1).ToString()] += strwk;
                    capa = int.Parse(zs.capalst[i]);
                    //残容量
                    if (inv >= capa)
                        dr["液種" + (i + 1).ToString()] += "0";
                    else
                    {
                        strwk = (capa - inv).ToString("#,0");
                        dr["液種" + (i + 1).ToString()] += strwk;
                    }
                    if (capa == 0)
                        dr["画像" + (i + 1).ToString()] = GetBitmapURL(0, olname);
                    else
                        dr["画像" + (i + 1).ToString()] = GetBitmapURL(inv * 1000 / capa, olname);
                    strwk = zs.sslan2stat[i];
                    strwk2 = zs.sslanstat[i];
                    dr["液種" + (i + 1).ToString()] += GetWrngStr(strwk, strwk2);
                    //新規行を追加
                    ZaikoDT.Rows.Add(dr);
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //在庫文字列を解析
        public void OpenZaiko2(string zstr, string skkcode)
        {
            if (zstr.Length < OFFSHEAD + OFFSTANK)
                return;
            int iNumTank = 0;
            int ofsad = 0;
            try
            {
                SiteInfData SiteDat = new SiteInfData();
                SiteDat.OpenTableSkkcode(skkcode);

                //時間文字列
                string dstr;
                if (zstr.StartsWith("20") == true)
                {
                    dstr = zstr.Substring(0, OFFSHEAD + 2);
                    dstr = dstr.Insert(10, ":"); //ファイル名より抽出した日付を1207211410 → 2012/07/21 14:10 のようなフォーマットにする。
                    dstr = dstr.Insert(8, " ");
                    dstr = dstr.Insert(6, "/");
                    dstr = dstr.Insert(4, "/");
                    ofsad = 12;
                }
                else
                {
                    dstr = zstr.Substring(0, OFFSHEAD);
                    dstr = dstr.Insert(8, ":"); //ファイル名より抽出した日付を1207211410 → 2012/07/21 14:10 のようなフォーマットにする。
                    dstr = dstr.Insert(6, " ");
                    dstr = dstr.Insert(4, "/");
                    dstr = dstr.Insert(2, "/");
                    dstr = "20" + dstr;
                }
                zdate = DateTime.Parse(dstr);
                ZaikoDT = new DataTable();
                ZaikoDT.Columns.Add(new DataColumn("タンク番号", typeof(int)));
                ZaikoDT.Columns.Add(new DataColumn("液種", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("画像", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("容量", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("在庫量", typeof(string)));
                ZaikoDT.Columns.Add(new DataColumn("警報", typeof(string)));

                DataRow dr;
                string strwk, strwk2, strwk3, olname;
                int capa, invper;
                int inv;
                ZaikoStr zs = new ZaikoStr(zstr);
                zs.Analyze();

                for (int i = 0; i < zs.numtank; i++)       //最大タンク数分ループ
                {
                    //在庫量取得
                    inv = int.Parse(zs.vollst[i]);
                    if (inv == 0) //在庫0ならスキップ
                        continue;
                    dr = ZaikoDT.NewRow();
                    //タンク番号取得
                    strwk = zs.tanknolst[i];;
                    dr["タンク番号"] = int.Parse(strwk);
                    //液種
                    strwk = zs.lqtypelst[i] + zs.lqtypelst2[i];
                    olname = GetOilName(strwk);
                    if (olname == "レギュラー")
                        dr["液種"] = "R";
                    else if (olname == "ハイオク")
                        dr["液種"] = "H";
                    else if (olname == "軽油")
                        dr["液種"] = "D";
                    else if (olname == "灯油")
                        dr["液種"] = "K";
                    else
                        dr["液種"] = "廃";
                    //タンク容量取得
                    //タンク容量取得
                    strwk = inv.ToString("#,0");
                    dr["在庫量"] = strwk;

                    capa = SiteDat.GetCapacityByTank(i + 1);
                    if (capa == 0)
                        capa = 10000;
                    if (olname == "廃油")
                        capa = 2000;
                    dr["容量"] = capa.ToString("#,0");

                    dr["画像"] = GetBitmapURL2(inv * 1000 / capa, olname);
                    strwk = zs.sslan2stat[i];
                    strwk2 = zs.sslanstat[i];
                    strwk3 = GetWrngStr(strwk, strwk2);
                    if (strwk3.Contains("減") == true)
                        dr["警報"] = "減";
                    else
                        dr["警報"] = "";
                    //新規行を追加
                    ZaikoDT.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //画像URLの取得
        private string GetBitmapURL(int PerVal, string olname)
        {
            string bmurl;
            if (PerVal > 975)
            {
                bmurl = "tank100per.png"; //100%
            }
            else if (PerVal > 925)
            {
                bmurl = "tank95per.png"; //95%
            }
            else if (PerVal > 875)
            {
                bmurl = "tank90per.png"; //90%
            }
            else if (PerVal > 825)
            {
                bmurl = "tank85per.png"; //85%
            }
            else if (PerVal > 775)
            {
                bmurl = "tank80per.png"; //80%
            }
            else if (PerVal > 725)
            {
                bmurl = "tank75per.png"; //75%
            }
            else if (PerVal > 675)
            {
                bmurl = "tank70per.png"; //70%
            }
            else if (PerVal > 625)
            {
                bmurl = "tank65per.png"; //65%
            }
            else if (PerVal > 575)
            {
                bmurl = "tank60per.png"; //60%
            }
            else if (PerVal > 525)
            {
                bmurl = "tank55per.png"; //55%
            }
            else if (PerVal > 475)
            {
                bmurl = "tank50per.png"; //50%
            }
            else if (PerVal > 425)
            {
                bmurl = "tank45per.png"; //45%
            }
            else if (PerVal > 375)
            {
                bmurl = "tank40per.png"; //40%
            }
            else if (PerVal > 325)
            {
                bmurl = "tank35per.png"; //35%
            }
            else if (PerVal > 275)
            {
                bmurl = "tank30per.png"; //30%
            }
            else if (PerVal > 225)
            {
                bmurl = "tank25per.png"; //25%
            }
            else if (PerVal > 175)
            {
                bmurl = "tank20per.png"; //20%
            }
            else if (PerVal > 125)
            {
                bmurl = "tank15per.png"; //15%
            }
            else if (PerVal > 75)
            {
                bmurl = "tank10per.png"; //10%
            }
            else if (PerVal > 25)
            {
                bmurl = "tank5per.png"; //5%
            }
            else
            {
                bmurl = "tank0per.png"; //00%
            }
            if (olname == "レギュラー")
                bmurl = "images/Blue" + bmurl;
            else if (olname == "ハイオク")
                bmurl = "images/Green" + bmurl;
            else if (olname == "軽油")
                bmurl = "images/Orange" + bmurl;
            else if (olname == "灯油")
                bmurl = "images/Purple" + bmurl;
            else
                bmurl = "images/Gray" + bmurl;
            return bmurl;
        }

        //画像URLの取得
        private string GetBitmapURL2(int PerVal, string olname)
        {
            string bmurl;
            if (PerVal > 975)
            {
                bmurl = "tank100per2.png"; //100%
            }
            else if (PerVal > 925)
            {
                bmurl = "tank95per2.png"; //95%
            }
            else if (PerVal > 875)
            {
                bmurl = "tank90per2.png"; //90%
            }
            else if (PerVal > 825)
            {
                bmurl = "tank85per2.png"; //85%
            }
            else if (PerVal > 775)
            {
                bmurl = "tank80per2.png"; //80%
            }
            else if (PerVal > 725)
            {
                bmurl = "tank75per2.png"; //75%
            }
            else if (PerVal > 675)
            {
                bmurl = "tank70per2.png"; //70%
            }
            else if (PerVal > 625)
            {
                bmurl = "tank65per2.png"; //65%
            }
            else if (PerVal > 575)
            {
                bmurl = "tank60per2.png"; //60%
            }
            else if (PerVal > 525)
            {
                bmurl = "tank55per2.png"; //55%
            }
            else if (PerVal > 475)
            {
                bmurl = "tank50per2.png"; //50%
            }
            else if (PerVal > 425)
            {
                bmurl = "tank45per2.png"; //45%
            }
            else if (PerVal > 375)
            {
                bmurl = "tank40per2.png"; //40%
            }
            else if (PerVal > 325)
            {
                bmurl = "tank35per2.png"; //35%
            }
            else if (PerVal > 275)
            {
                bmurl = "tank30per2.png"; //30%
            }
            else if (PerVal > 225)
            {
                bmurl = "tank25per2.png"; //25%
            }
            else if (PerVal > 175)
            {
                bmurl = "tank20per2.png"; //20%
            }
            else if (PerVal > 125)
            {
                bmurl = "tank15per2.png"; //15%
            }
            else if (PerVal > 75)
            {
                bmurl = "tank10per2.png"; //10%
            }
            else if (PerVal > 25)
            {
                bmurl = "tank5per2.png"; //5%
            }
            else
            {
                bmurl = "tank0per.png"; //00%
            }
            if (olname == "レギュラー")
                bmurl = "images/Blue" + bmurl;
            else if (olname == "ハイオク")
                bmurl = "images/Green" + bmurl;
            else if (olname == "軽油")
                bmurl = "images/Orange" + bmurl;
            else if (olname == "灯油")
                bmurl = "images/Purple" + bmurl;
            else
                bmurl = "images/Gray" + bmurl;
            return bmurl;
        }

        //油種コードから油種名への変換
        private string GetOilName(string oilstr)
        {
            string oilname = "";
            int oilnum = 0;
            if (oilstr.Substring(1) != "00")
            {
                oilstr = oilstr.Substring(1, 2);
                oilnum = int.Parse(oilstr);
                switch (oilnum)
                {
                    case 45:
                    default:
                        oilname = "レギュラー";
                        break;
                    case 53:
                        oilname = "ハイオク";
                        break;
                    case 57:
                        oilname = "軽油";
                        break;
                    case 58:
                        oilname = "灯油";
                        break;
                    case 59:
                        oilname = "廃油";
                        break;
                    case 60:
                        oilname = "A重油";
                        break;
                    case 61:
                        oilname = "重油";
                        break;
                    case 68:
                        oilname = "プレミアム軽油";
                        break;
                }
            }
            else
            {
                oilstr = oilstr.Substring(0, 1);
                oilnum = int.Parse(oilstr);
                switch (oilnum)
                {
                    case 1:
                        oilname = "ハイオク";
                        break;
                    case 2:
                    default:
                        oilname = "レギュラー";
                        break;
                    case 3:
                        oilname = "灯油";
                        break;
                    case 4:
                        oilname = "軽油";
                        break;
                    case 6:
                        oilname = "プレミアム軽油";
                        break;
                    case 9:
                        oilname = "廃油";
                        break;
                }
            }
            return oilname;
        }
        //警報文字列取得
        private string GetWrngStr(string str, string str2)
        {
            string wrgstr = "";
            byte[] WrgArray = Encoding.ASCII.GetBytes(str);
            byte sts1 = WrgArray[0];
            byte sts2 = WrgArray[1];
            if ((sts1 & 0x10) == 0x10)
                wrgstr += "減,";
            if ((sts1 & 0x08) == 0x08)
                wrgstr += "満,";
            if ((sts1 & 0x04) == 0x04)
                wrgstr += "水,";
            if ((sts1 & 0x02) == 0x02)
                wrgstr += "センサ,";
            if ((sts1 & 0x01) == 0x01)
                wrgstr += "リーク,";
            if ((sts2 & 0x20) == 0x20)
                wrgstr += "下限,";
            if ((sts2 & 0x10) == 0x20)
                wrgstr += "LC-1,";
            if ((sts2 & 0x08) == 0x08)
                wrgstr += "LC-4,";
            if ((sts2 & 0x04) == 0x04)
                wrgstr += "LC-5,";
            if ((sts2 & 0x02) == 0x02)
                wrgstr += "LC-7,";
            if ((sts2 & 0x01) == 0x01)
                wrgstr += "LC-8,";
            if (wrgstr.Length > 1)
                wrgstr = wrgstr.Substring(0, wrgstr.Length - 1); //最後の","をとる
            if ((wrgstr.Length == 0) && (str2 != "0"))
            {
                switch (str2)
                {
                    case "1":
                        wrgstr = "満";
                        break;
                    case "2":
                        wrgstr = "減";
                        break;
                    case "3":
                        wrgstr = "水";
                        break;
                    case "4":
                        wrgstr = "満, 水";
                        break;
                    case "5":
                        wrgstr = "減, 水";
                        break;
                }
            }


            return wrgstr;
        }
        //文字列表示
        public void ShowZaikoList(System.Web.UI.WebControls.GridView pGridView)
        //public void ShowZaikoList(  C1.Web.Wijmo.Controls.C1GridView.C1GridView pGridView )
        {
            try
            {
                pGridView.RowStyle.Wrap = true;
                pGridView.DataSource = ZaikoDT;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //データテーブル取得
        public DataTable GetDataTable()
        {
            return ZaikoDT;
        }
    }
}