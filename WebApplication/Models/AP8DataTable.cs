﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using WebApplication.Models;

namespace WebApplication.Models
{
    public class AP8DataTable
    {
        private DataTable AP8STable;    //AP8全施設状況テーブル
        private DataTable AP8IndSTable; //AP8個別施設状況テーブル
        //AP8StatusTable定義
        private const string stCollect = "集信";
        private const string stSSName = "施設名";
        private const string stAP8Status = "状況";
        private const string stSKKcode = "SKKコード";

        enum APStatusCol
        {
            Collect = 0, //集信
            SSName, //施設名
            AP8Status, //AP8集信状況
            Skkcode //SKKコード
        }

        //コンストラクタ
        public AP8DataTable()
        {
            DataTableCtrl.InitializeTable(AP8STable);
        }

        //AP8集信状況タブ表示用テーブル(AP8StatusTable)作成
        public void CreateAP8StatusTable(string username, string compname)
        {
            SynaSiteDat ssdat = new SynaSiteDat();
            ssdat.OpenTable("*", username, compname);
            int num = ssdat.GetNumOfRecord();
            if (num == 0)
                return;

            DataTableCtrl.InitializeTable(AP8STable);
            //テーブルのフィールドを定義する
            AP8STable = new DataTable();

            AP8STable.Columns.Add(new DataColumn(stCollect, typeof(bool)));
            AP8STable.Columns.Add(new DataColumn(stSSName, typeof(string)));
            AP8STable.Columns.Add(new DataColumn(stAP8Status, typeof(string)));
            AP8STable.Columns.Add(new DataColumn(stSKKcode, typeof(string)));
            string timestr = ""; //時間
            int coltype = (int)Ap8ComType.GetAP8Dat; //集信タイプ 
            string logstr = "";     //ログ

            for (int i = 0; i < num; i++)
            {
                SqlConnection con = new System.Data.SqlClient.SqlConnection(GlobalVar.DBCONNECTION);
                try
                {
                    DataRow dTblRow = AP8STable.NewRow();
                    dTblRow[(int)APStatusCol.Collect] = false;
                    string sscode = ssdat.GetSKKCode(i); //SKKコード
                    dTblRow[(int)APStatusCol.SSName] = ssdat.GetSiteName(i);
                    dTblRow[(int)APStatusCol.Skkcode] = sscode;

                    con.Open();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.CommandText = @"SELECT TOP(1) 時間,集信タイプ,ログ FROM SynaSiteAP8Log WHERE SKKコード='" + sscode + "' AND 集信タイプ IN ('0','1' ) ORDER BY 時間 DESC";

                    // SQLの実行
                    int count = 0;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read() == true)
                        {
                            timestr = ((string)reader["時間"]).TrimEnd();
                            coltype = (int)reader["集信タイプ"];
                            logstr = ((string)reader["ログ"]).TrimEnd();
                            count++;
                        }
                    }
                    if (count != 0)
                    {
                        DateTime dt = new DateTime(int.Parse(timestr.Substring(0, 4)), int.Parse(timestr.Substring(4, 2)), int.Parse(timestr.Substring(6, 2)), int.Parse(timestr.Substring(8, 2)), int.Parse(timestr.Substring(10, 2)), 0);
                        timestr = dt.ToString("yyyy/MM/dd HH:mm");
                        if (coltype == 0)
                        {
                            if (logstr == GlobalVar.CollectOK)
                                dTblRow[(int)APStatusCol.AP8Status] = timestr + " 集信成功";
                            else if(logstr == GlobalVar.AccessNG)
                                dTblRow[(int)APStatusCol.AP8Status] = timestr + " 通信障害";
                            else
                                dTblRow[(int)APStatusCol.AP8Status] = timestr + " 集信失敗";
                        }
                        else
                        {
                            if (logstr == GlobalVar.RecollectOK)
                                dTblRow[(int)APStatusCol.AP8Status] = timestr + " 再集信成功";
                            else if( logstr == GlobalVar.AccessNG)
                                dTblRow[(int)APStatusCol.AP8Status] = timestr + " 通信障害";
                            else
                                dTblRow[(int)APStatusCol.AP8Status] = timestr + " 再集信失敗";
                        }
                        if(logstr != "200")
                            AP8STable.Rows.Add(dTblRow);
                    } else
                    {
                        dTblRow[(int)APStatusCol.AP8Status] ="集信履歴なし";
                        AP8STable.Rows.Add(dTblRow);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    con.Close();
                }

            }
        }

        //AP8集信状況タブ表示用テーブル(AP8StatusTable)取得
        public DataTable GetAP8StatusTable()
        {
            try
            {
                return AP8STable;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }
        }

        //AP8集信状況タブ表示用テーブル(AP8StatusTable)よりSKKコード取得
        public string GetSkkcodeFromStatusTable(int lineno)
        {
            try
            {
                if ((AP8STable != null) && (lineno < AP8STable.Rows.Count))
                {
                    return AP8STable.Rows[lineno][(int)APStatusCol.Skkcode].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //AP8個別集信状況タブ表示用テーブル(AP8IndSTable)作成
        public void CreateAP8IndvStatusTable(string skkcode, string username, string compname)
        {
            SqlConnection con = new System.Data.SqlClient.SqlConnection(GlobalVar.DBCONNECTION);

            try
            {
                DataTableCtrl.InitializeTable(AP8IndSTable);
                //テーブルのフィールドを定義する
                AP8IndSTable = new DataTable();
                AP8IndSTable.Columns.Add(new DataColumn(stAP8Status, typeof(string)));
                string timestr = ""; //時間
                int coltype = (int)Ap8ComType.GetAP8Dat; //集信タイプ
                string logstr = "";     //ログ
                SynaSiteDat ssdat = new SynaSiteDat();
                ssdat.OpenTable("*", username, compname);

                con.Open();
                SqlCommand cmd = con.CreateCommand();
                cmd.CommandText = @"SELECT TOP(10) 時間,集信タイプ,ログ FROM SynaSiteAP8Log WHERE SKKコード='" + skkcode + "' AND 集信タイプ IN ('0','1' ) ORDER BY 時間 DESC";

                // SQLの実行
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read() == true)
                    {
                        timestr = ((string)reader["時間"]).TrimEnd();
                        coltype = (int)reader["集信タイプ"];
                        logstr = ((string)reader["ログ"]).TrimEnd();

                        DataRow dTblRow = AP8IndSTable.NewRow();
                        DateTime dt = new DateTime(int.Parse(timestr.Substring(0, 4)), int.Parse(timestr.Substring(4, 2)), int.Parse(timestr.Substring(6, 2)), int.Parse(timestr.Substring(8, 2)), int.Parse(timestr.Substring(10, 2)), 0);
                        timestr = dt.ToString("yyyy/MM/dd HH:mm");
                        if (coltype == 0)
                        {
                            if (logstr == GlobalVar.CollectOK)
                                dTblRow[0] = timestr + " 集信成功";
                            else if (logstr == GlobalVar.AccessNG)
                                dTblRow[0] = timestr + " 通信障害";
                            else
                                dTblRow[0] = timestr + " 集信失敗";
                        }
                        else
                        {
                            if (logstr == GlobalVar.RecollectOK)
                                dTblRow[0] = timestr + " 再集信成功";
                            else if (logstr == GlobalVar.AccessNG)
                                dTblRow[0] = timestr + " 通信障害";
                            else
                                dTblRow[0] = timestr + " 再集信失敗";
                        }
                        AP8IndSTable.Rows.Add(dTblRow);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                con.Close();
            }
        }

        //AP8個別集信状況タブ表示用テーブル(AP8IndSTable)取得
        public DataTable GetAP8IndStatusTable()
        {
            return AP8IndSTable;
        }

    }
}