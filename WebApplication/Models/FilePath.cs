﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Diagnostics;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

namespace WebApplication.Models
{
    public class FilePath
    {
        private string ap8blobcontainer = "skkatg-ap8data";
        private string leveblobcontainer = "skkatg-levelog";
        private string sirablobcontainer = "skkatg-sira";
        private string lc4blobcontainer = "skkatg-lc4";

        private string accountName = "skkatgstorage";
        private string accessKey = "nNT1et0cM46CxVgHWCeDbMrEmtMffz8F50EjVGqOuwVCeDus17HMa2Anhe2JnOZJcI69rsnTlbQPc1p9y9sQlA==";

        public FilePath()
        {
        }

        public string GetTodayAP8DataName(string skkcode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return skkcode + "-" + dt.ToString("yyyyMMdd") + ".csv";
        }

        public string GetSpecifiedDaysAP8DataName(string skkcode, DateTime dt)
        {
            return skkcode + "-" + dt.ToString("yyyyMMdd") + ".csv";
        }

        public string GetLatestAP8DataName(string skkcode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return skkcode + "-" + dt.ToString("yyyyMMdd") + "latest.csv";
        }

        //LC4パスの取り出し
        public string GetLC4DataPath()
        {
            return "lc4blobcontainer";
        }
        //今月の漏えい点検ファイル名の取り出し
        public string GetThisMonthLC4DataPath(string SkkCode)
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            return "L" + SkkCode + dt.ToString("yyMM") + ".csv";
        }

        //指定月の点検ファイル名の取り出し
        public string GetMonthLC4DataPath(string SkkCode, DateTime dt)
        {
            return "L" + SkkCode + dt.ToString("yyMM") + ".csv";
        }
        //指定月の点検ファイル名の取り出し
        public string GetMonthLC4DataWDayStrPath(string SkkCode, string DayStr)
        {
            return "L" + SkkCode + DayStr + ".csv";
        }

        public void WriteTodayAP8Data(string skkcode, string data)
        {
            try
            {
                string filename = GetTodayAP8DataName(skkcode);
                WriteBlob(filename, ap8blobcontainer, data, false);
                filename = GetLatestAP8DataName(skkcode);
                WriteBlob(filename, ap8blobcontainer, data, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public string ReadTodayAP8Data(String skkcode)
        {
            string ap8dat = "";
            try
            {
                string filename = GetLatestAP8DataName(skkcode);
                ap8dat = ReadBlob(filename, ap8blobcontainer);
                //DownloadBlob(filename, ap8blobcontainer, "ap8file.txt");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return ap8dat;
        }

        public string ReadSpecifiedDayAP8Data(string skkcode, DateTime dt)
        {
            string ap8dat = "";
            try
            {
                string filename = GetSpecifiedDaysAP8DataName(skkcode, dt);
                ap8dat = ReadBlob(filename, ap8blobcontainer);
                //DownloadBlob(filename, ap8blobcontainer, "ap8file.txt");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return ap8dat;
        }

        //blobファイルの書き込み
        private bool WriteBlob(string filename, string containername, string data, bool overwrite)
        {
            bool res = true;
            try
            {
                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer container = blobClient.GetContainerReference(containername);

                //appendblob
                //BlockBlobではなくAppenDBlobのリファレンスを取得する
                CloudAppendBlob appendBlob = container.GetAppendBlobReference(filename);

                //書き込み
                if (overwrite == false)     //追加
                {
                    //無かったら作る
                    if (!appendBlob.Exists()) appendBlob.CreateOrReplace();
                    appendBlob.AppendText(data);
                }
                else                        //上書き
                {
                    try
                    {
                        appendBlob.Delete();
                    }
                    catch (Exception ex)
                    {
                        ;
                    }
                    appendBlob.CreateOrReplace();
                    appendBlob.AppendText(data);
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return res;
        }

        //blobファイルの読み込み
        private string ReadBlob(string filename, string containername)
        {
            string ap8dat = "";
            try
            {
                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer container = blobClient.GetContainerReference(containername);

                CloudAppendBlob blockBlob_read = container.GetAppendBlobReference(filename);

                string text;
                using (var memoryStream = new MemoryStream())
                {
                    blockBlob_read.DownloadToStream(memoryStream);
                    text = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                }

                return text;


            }
            catch (Exception ex)
            {
                try
                {
                    var credential = new StorageCredentials(accountName, accessKey);
                    var storageAccount = new CloudStorageAccount(credential, true);

                    //blob
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    //container
                    CloudBlobContainer container = blobClient.GetContainerReference(containername);

                    CloudBlockBlob blockBlob_read = container.GetBlockBlobReference(filename);

                    string text;
                    using (var memoryStream = new MemoryStream())
                    {
                        blockBlob_read.DownloadToStream(memoryStream);
                        text = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                    }

                    return text;

                }
                catch ( Exception ex2 )
                {
                    Debug.WriteLine(ex2.ToString());
                }
            }
            return ap8dat;
        }

        //blobファイルのダウンロード
        private bool DownloadBlob(string filename, string containername, string tofilename)
        {
            bool res = false;
            try
            {
                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer container = blobClient.GetContainerReference(containername);

                CloudBlockBlob blockBlob_download = container.GetBlockBlobReference(filename);

                //ダウンロード後のパスとファイル名を指定。
                blockBlob_download.DownloadToFile(@tofilename, System.IO.FileMode.CreateNew);

                res = true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return res;
        }

        //指定されたコンテナー中から古いファイルを削除
        public void DeleteOldAP8Data()
        {
            try
            {
                long fileCount = 0;
                long count = 0;

                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer blobContainer = blobClient.GetContainerReference(ap8blobcontainer);

                //継続トークン
                BlobContinuationToken blobtoken = null;


                do
                {
                    var blobResult = blobContainer.ListBlobsSegmented(blobtoken);

                    //継続トークの取得
                    blobtoken = blobResult.ContinuationToken;

                    //取得したBlob情報を直接取得
                    var blobList = blobResult.Results.ToList();
                    string filename;

                    foreach (var item in blobList)
                    {
                        filename = item.Uri.ToString();
                        filename = System.IO.Path.GetFileName(filename);
                        CloudAppendBlob blockBlob_del = blobContainer.GetAppendBlobReference(filename);
                        blockBlob_del.Delete();
                    }

                } while (blobtoken != null);


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }
        //指定されたコンテナー中から指定SKKコードのファイル一覧取得
        public List<string> ListAP8FilesDate(string skkcode)
        {
            List<string> ap8filedate = new List<string>();
            try
            {
                long fileCount = 0;
                long count = 0;

                var credential = new StorageCredentials(accountName, accessKey);
                var storageAccount = new CloudStorageAccount(credential, true);

                //blob
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                //container
                CloudBlobContainer blobContainer = blobClient.GetContainerReference(ap8blobcontainer);

                //継続トークン
                BlobContinuationToken blobtoken = null;

                do
                {
                    var blobResult = blobContainer.ListBlobsSegmented(blobtoken);

                    //継続トークの取得
                    blobtoken = blobResult.ContinuationToken;

                    //取得したBlob情報を直接取得
                    var blobList = blobResult.Results.ToList();
                    string filename;
                    string skcode, datestr;
                    foreach (var item in blobList)
                    {
                        filename = item.Uri.ToString();
                        filename = System.IO.Path.GetFileName(filename);
                        skcode = filename.Substring(0, 10);
                        if (filename.IndexOf("latest") < 0)
                        {
                            if (skcode == skkcode)
                            {
                                datestr = filename.Substring(11, 8);
                                datestr = datestr.Insert(8, "日");
                                datestr = datestr.Insert(6, "月");
                                datestr = datestr.Insert(4, "年");
                                ap8filedate.Add(datestr);
                            }
                        }
                    }

                } while (blobtoken != null);


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return ap8filedate;
        }

        //カードロックファイルの名前指定
        private string GetCardLockName()
        {
            string clname = "CardLockFile.dat";
            return clname;
        }

        //指定された文字列をカードロックデータとしてブロブ(コンテナ:"skkatg-ap8data")に書き込む。
        public bool UploadCardLockFile(string cldata)
        {
            bool res = WriteBlob(GetCardLockName(), ap8blobcontainer, cldata, true); //Cardlockファイルのブロブへの書き込み
            return res;
        }

        //カードロックファイルを読み出し文字列として返す
        public string DownloadCardLockFile()
        {
            string cldata = "";
            cldata += ReadBlob(GetCardLockName(), ap8blobcontainer);
            return cldata;
        }
        //ワークファイル1へのパス
        public string GetBase64file()
        {
            //return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "temp1.dat";
            return "e:\\temp\\send01.bs6";
        }
        //ワークファイル1へのパス
        public string GetTemp1Filepath()
        {
            //return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "temp1.dat";
            return "e:\\temp\\temp1.dat";
        }
        //ワークファイル2へのパス
        public string GetTemp2Filepath()
        {
            //return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "temp2.dat";
            return "e:\\temp\\temp2.dat";
        }
    }
}