﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApplication.Models
{
    /// プログラム全体で共有する変数、ルーチンを集めています。
    public class GlobalVar
    {
        public static int NUMTANK = 20;
        public static int TankStatColNo = 5;
        public static int MaxTankNo = 9;

        //    public static string DBCONNECTION = "Data Source = skk-atgs.jp;Initial Catalog = r_15180_skk;User ID=r_15180_skk;Password = Eq6GwE8u";
        public static string DBCONNECTION = "Data Source = skkatgsdb.database.windows.net;Initial Catalog = SkkAtgsDB;User ID=skkkaiadmin;Password = skkkaidb-201806";

        public static string ServerPath { get; set; }
        public static bool loginreq = true  ;             //trueならばAccount認証必要
        public static bool demomode = false;
        public static bool levetestmode = false;
        public static bool accdeltest = true;
        public static string logouttime = "36000"; //ログアウトタイマー　秒 シナネン様からはログアウトは基本なしにということでかなり長い時間(10時間分)にしている。

        public static int SHOWDATE = 15;    //履歴表示日数
        public static string Adminusername = "n-matsuda@showa-kiki.co.jp";
        public static string Subadminusername = "n-matsuda@showa-kiki.co.jp";

        //public static string EmgAdminusername = "emgmarket";
        //public static string Subadminusername = "emgmarket2";
        //public static string DemoUsername = "skkdemo";
        public static string SinanenUsername1 = "sinaleve";

        //------ 対象会社名 -------------------
        //------- シナネン -------------------------------
        public static int CompanyNo = 0;
        public static string CompanyName = "シナネン株式会社";
        public static string CompanyName2 = "Synanen";
        public static string CompanySubAdminName = "SynanenSubAdmin";
        public static string CompanyNameAlpha = "Synanen";
        public static string MlSysCompanyName = "Synanen";
        public static string MlCompanyName = "Synanen";
        public static string CompanyFolder = "Synanen";
        public static string CompMBUSkkcode = "M099";
        public static string CompGWUSkkcode = "A099";
        public static string DefSkkcode1 = "M099000001";
        public static string DefSiteName1 = "SKKテスト";
        public static string AdminName = "SynanenPrimeAdmin";
        public static string SubAdminName = "SynanenSubAdmin";
        public static string GeneralName = "SynanenGeneral";
        public static string UserName = "Synanen";
        public static string MlAdr1 = "n-matsuda@showa-kiki.co.jp";


        //-------- 相光石油 -----------------------------
        //public static string CompanyName = "相光石油";
        //public static string CompanyName3 = "相光石油";
        //public static string MlSysCompanyName = "相光石油";
        //public static string MlCompanyName = "相光石油";
        //public static string CompanyFolder = "Test1";
        //public static string CompGWUSkkcode = "A060";
        //public static string CompMBUSkkcode = "M012";
        //public static string DefSkkcode1 = "M012000001";
        //public static string DefSiteName1 = "大江SS";
        //public static int MaxTankNo = 9;

        //-------- エネオス株式会社 -----------------------------
        //public static string CompanyName = "エネオス株式会社";
        //public static string CompanyName3 = "エネオス株式会社";
        //public static string MlSysCompanyName = "エネオス株式会社";
        //public static string MlCompanyName = "エネオス株式会社";
        //public static string CompanyFolder = "Test1";
        //public static string CompGWUSkkcode = "A099";
        //public static string CompMBUSkkcode = "M013";
        //public static string DefSkkcode1 = "M013000001";
        //public static string DefSiteName1 = "青森インターSS";
        //public static int MaxTankNo = 11;
        public static string DispType = "画面種類";

        //Session用文字列 --- れべビジョン集信用
        public static string SCompanyName = "Company"; //会社名
        public static string SCompanyNo = "CompanyNo";  //会社番号
        public static string SDisplayNo = "Display"; //表示番号
        public static string SDispWrgOnly = "ShowWrgOnly"; //警報発生施設のみ表示
        public static string SSiteName = "SiteName"; //施設名
        public static string SSkkcode = "Skkcode"; //SKKコード
        public static string SSelectSite = "SelectSite"; //リストボックスで選択されている施設
        public static string STankNo = "TankNo"; //タンク番号
        public static string SPrefName = "Prefecture"; //県名
        public static string SIPageNo = "InvPageNo"; //在庫表示用ページ番号
        public static string SOiltype = "OilType"; //油種ｓ
        public static string SSortby = "SortBy"; //タンク番号/液種順表示
        public static string SSiteNo = "SiteNo"; //施設番号
        public static string SDstname = "DstName"; //特約店名
        public static string SUsername = "UserName"; //ユーザー名(メール用)
        public static string SMlSitename = "Mlsitename"; //メール宛先施設名
        public static string SComStatus = "CommSts"; //指定集信ステータス
        public static string SAccName = "AccountName"; //設定用アカウント名
        public static string SAccNo = "AccountNo";  //アカウント削除用番号
        public static string SValSet = "ValSet";    //設定画面でのテキストボックス文字列設定確認
        public static string SAllAP8Chk = "true";
        public static int ValNoSet = 0;             //文字列未設定
        public static int ValSet = 1;               //文字列設定済
        
        //Session用文字列 --- 設定用
        public static string CDisplayNo = "CDisplay"; //表示番号

        //Session用文字列 --- AP-8用
        public static string ADisplayNo = "ADisplay"; //表示番号
        public static string ASiteName = "ASiteName"; //施設名
        public static string ASkkcode = "ASkkcode"; //SKKコード
        public static string ASiteNo = "ASiteNo"; //施設番号
        public static string ADefSkkcode1 = "M099000001";
        public static string ADefSiteName1 = "SKK開発";

        //画面表示用文字列
        public static string ShowAllSites = "全ての施設を表示";
        public static string ShowWrgSites = "警報発生施設のみを表示";
        public static string ShowComSites = "指定集信を行う施設をチェックして、右の集信開始ボタンをクリック　　  ";
        public static string AllSites = "全て";
        public static string JavaHeader = "<script language=javascript>";
        public static string JavaEnd = "</script>";

        //タンクステータス文字列
        public static string TankNoError = "通常";
        public static string TankFull = "満";

        //画面
        public static string TabsLevevision = "レベビジョン";
        public static string TabsSettings = "設定";
        public static string TabsAP8 = "AP-8";

        //タブ名 -- レベビジョン
        public static string TabTable = "在庫一覧";
        public static string TabComm = "指定集信";
        public static string TabSSTHistory = "在庫履歴（タンクごと）";
        public static string TabSSOHistory = "在庫履歴（液種ごと）";
        public static string TabSSWrg = "施設管理";
        public static string TablSiteInf = "登録施設";
        //タブ名 -- 設定
        public static string TabCfgDispSite = "表示施設";
        public static string TabCfgCrAccount = "アカウント作成";
        public static string TabCfgRmAccount = "アカウント管理";
                
        //タブ名 -- AP-8
        public static string TabAP8Status = "集信状況";
        public static string TabAP8IndvStatus = "個別集信状況";
        public static string TabAP8Config = "集信設定";
        public static string TabAP8CardLock = "カードロック配信";
        public static string TabAP8CardLockMaster = "カードロックマスター管理";

        //タブ番号 --- レベビジョン
        public static int TabTableNo = 0;   //表表示
        public static int TabCommNo = 1;    //指定集信
        public static int TabSSTHisNo = 1;  //在庫履歴(タンク毎)
        public static int TabSSOHisNo = 1;  //在庫履歴(液種毎)
        public static int TabSSWrgNo = 2;   //施設管理
        public static int TabSiteInfNo = 3; //登録施設

        //タブ番号 --- 設定
        public static int TabCfgDispSiteNo = 0;        //表示施設
        public static int TabCfgRmAccountNo = 1;       //アカウント管理
        public static int TabCfgCrAccountNo = 2;       //アカウント作成
        
        //タブ番号 --- AP-8
        public static int TabAP8StatusNo = 0;        //集信状況
        public static int TabAP8IndvStatusNo = 1;    //個別集信状況
        public static int TabAP8ConfigNo = 2;        //集信設定
        public static int TabAP8CardLockNo = 3;      //カードロック配信
        public static int TabAP8CardLockMasterNo = 4;   //カードロックマスター管理

        //シナネンAP-8関連
        public static string GetAP8Data = "GetAP8Data";
        public static string RegetAP8Data = "RegetAP8Dt";
        public static string PostCardLockData = "PostCLData";
        public static string SetAtDtGetCMD = "SetAtDtGet";

        //カードロック配信結果
        public static string CollectOK = "000"; //集信成功
        public static string RecollectOK = "100"; //再集信成功
        public static string DelivOK = "200";   //配信成功
        public static string AccessNG = "500";  //ネットワーク障害

        public static string SetDateStr(string zstr)
        {
            zstr = zstr.Insert(8, ":"); //ファイル名より抽出した日付を1207211410 → 2012/07/21 14:10 のようなフォーマットにする。
            zstr = zstr.Insert(6, " ");
            zstr = zstr.Insert(4, "/");
            zstr = zstr.Insert(2, "/");
            zstr = "20" + zstr;
            return zstr;
        }

        //DSV在庫情報
        public struct DsvInvData
        {
            public long FullVolume;       //申請容量
            public long OilLevel;         //液面高さ
        }

        //県別リスト
        public static string[] prefnames = { "北海道", "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県", "茨城県", "栃木県", "群馬県", "埼玉県", "千葉県", "東京都", "神奈川県", "新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県", "静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府", "兵庫県", "奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "山口県", "徳島県", "香川県", "愛媛県", "高知県", "福岡県", "佐賀県", "長崎県", "熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県" };
        public static string defprefname = "全国";
        public static string alljapan = "全国";
        public static DsvInvData tank1;

    	public static bool ShowHLOnly = true; //true --- 満、減警報のみ表示
    
        //文字列"yyyy年MM月"よりDateTimeに変換
        public static DateTime GetDateTimeFromMonthString(string selmonth)
        {
            selmonth = selmonth.Remove(selmonth.Length - 1);//xxxx年yy
            int year = int.Parse(selmonth.Substring(0, 4));      //xxxx
            int month = int.Parse(selmonth.Substring(5));
            return new DateTime(year, month, 1);
        }
        //文字列"yyyy年MM月"よりDateTimeに変換  bfirst=true 毎月1日 false 15日
        public static DateTime GetDateTimeByHalfMonthFromMonthString(string datestr, bool bfsthalf)
        {
            //datestr = datestr.Substring(1);   //xxxx年yy月
            datestr = datestr.Remove(datestr.Length - 1);//xxxx年yy
            int year = int.Parse(datestr.Substring(0, 4)); //xxxx
            int month = int.Parse(datestr.Substring(5));
            if (bfsthalf == true)
                return (new DateTime(year, month, 1));
            else
                return (new DateTime(year, month, 15));
        }
    }
    //AP-8集信タイプ
    public enum Ap8ComType
    {
        GetAP8Dat = 0, //AP-8集信
        GetAP8DatRetry = 1, //AP-8再集信
        PutCardLockDat = 2  //カードロックデータ配信
    }

    public enum WrgType
    {
        LowWrg = 0, //減
        HighWrg = 1,  //満ｓ
        WtrWrg = 2,    //水
        SnsErrWrg = 3, //センサー異常
        LC4Wrg = 4,    //漏えい点検
        LeakWrg = 5,   //リーク
        NoDataWrg = 6  //データ無し
    }

    //AP8自動集信設定
    public struct AP8AutoCol
    {
        public int autocol;
        public string coltime;
    }

    public class ErrDataDetail
    {
        public string SiteName;    //施設名
        public string Skkcode;     //SKKコード
        public string Companyname;  //会社名
        public int OilTypeno;       //液種名
        public int TankNo;          //タンク番号
        public string DateStr;      //日時
        public int WrgType;         //警報種類
        public int TrgOrRel;        //0:警報解除 1:発生
        public int WtrLvl;          //水位
        public int WtrVol;          //水量
        public string LC4Res;       //漏えい点検結果,判定値

        public ErrDataDetail(string SName, string Scode, string Cname, int Ono, int Tno, string DStr, int WType, int ToR, int Wl, int Wv, string res)
        {
            SiteName = SName;
            Skkcode = Scode;
            Companyname = Cname;
            OilTypeno = Ono;
            TankNo = Tno;
            DateStr = DStr;
            WrgType = WType;
            TrgOrRel = ToR;
            WtrLvl = Wl;
            WtrVol = Wv;
            LC4Res = res;
        }
    }

    public class OilName
    {
        public static string GetOilName(int oiltype)
        {
            string oilname = "";
            switch (oiltype)
            {
                case 1:
                    return "ハイオク";
                case 2:
                    return "レギュラー";
                case 3:
                    return "灯油";
                case 4:
                    return "軽油";
            }
            return oilname;
        }
    }

    public class ErrTypeName
    {
        public static string GetErrName(int errtype)
        {
            switch (errtype)
            {
                case (int)WrgType.LowWrg:
                    return "減警報";
                case (int)WrgType.HighWrg:
                    return "満警報";
                case (int)WrgType.WtrWrg:
                    return "水検知警報";
                case (int)WrgType.SnsErrWrg:
                    return "液面センサー異常警報";
                case (int)WrgType.LC4Wrg:
                    return "漏えい検知警報";
                case (int)WrgType.LeakWrg:
                    return "二重殻タンク間隙のリーク警報";
                case (int)WrgType.NoDataWrg:
                    return "データ更新なし警報";
            }
            return " ";
        }
    }

    public class WebResponseCtrl
    {
        //ファイルダウンロード用のHTTP Response作成 sendtext:ダウンロードする文字列 sDownloadFileName:ダウンロードファイル名
        static public void CreateResponse(System.Web.HttpResponse Response, string sendtext, string sDownloadFileName)
        {
            //Response情報クリア
            Response.ClearContent();
            //バッファリング
            Response.Buffer = true;
            //HTTPヘッダー情報設定
            // ヘッダとコンテンツのエンコードを「shift_jis」に設定。
            Response.HeaderEncoding = System.Text.Encoding.GetEncoding("shift_jis");
            Response.ContentEncoding = System.Text.Encoding.GetEncoding("shift_jis");
            // HTTPのキャラセットを設定
            Response.Charset = "shift_jis";
            // ファイルのMIMEタイプを設定ですが…、とりあえず「その他」を設定(OS側で拡張子に従って開いてもらう為)
            Response.ContentType = "application/octet-stream";
            // コンテキスト(メッセージ)の扱いに関するヘッダ情報です。ここにファイル名を設定します。
            //Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName));

            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", HttpUtility.UrlEncode(sDownloadFileName)));
            Response.ContentType = "text/plain";
            //ファイル書込
            Response.Write(sendtext);
            //フラッシュ
            Response.Flush();
            //レスポンス終了
            Response.End();
        }

        //Popupメッセージを表示する  sendtext:ポップアップに表示する文字列
        static public void WriteResponse(System.Web.HttpResponse Response, string sendtext)
        {
            string script = GlobalVar.JavaHeader + "window.alert('" + sendtext + "')" + GlobalVar.JavaEnd;
            Response.Write(script);
        }
    }

    //データテーブル初期化用クラス
    public class DataTableCtrl
    {
        //データテーブルの消去
        static public void InitializeTable(DataTable dt)
        {
            if (dt != null)
            {
                dt.Clear();
                dt.Dispose();
                dt = null;
            }
        }

        //データセットの消去
        static public void InitializeDataSet(DataSet ds)
        {
            if (ds != null)
            {
                ds.Clear();
                ds.Tables.Clear();
                ds.Dispose();
                ds = null;
            }
        }
    }

    //データベースアクセスクラス
    public class DBCtrl
    {
        //sql分を実施して、指定されたデータテーブルにDB中のデータをコピーする
        static public void ExecSelectAndFillTable(string sqlstr, DataTable dt)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
            dAdp.Fill(dt);
            cn.Close();
        }
        //sql分を実施する。
        static public void ExecNonQuery(string sqlstr)
        {
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            System.Data.SqlClient.SqlCommand Com;
            Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            Com.ExecuteNonQuery();
            cn.Close();
        }
    }

    //水履歴情報用クラス
    public class WtrHistory
    {
        //戻り値 true: 指定SKKコードの水履歴が存在する false:存在しない
        public static bool ExistWtrHistory(string skkcode)
        {
            bool bexist = false;
            string cnstr = GlobalVar.DBCONNECTION;
            System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
            cn.Open();
            string sqlstr = "SELECT COUNT(*) FROM WtrHistory WHERE SKKコード='" + skkcode + "'"; ;
            System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
            int count = (int)Com.ExecuteScalar();
            cn.Close();
            if (count > 0)
                bexist = true;
            return bexist;
        }

        //1か月前以上の古い水履歴を消去する
        public static void DeleteOldWtrHist()
        {
            DateTime dt = DateTime.Now.ToLocalTime();
            int hour = dt.Hour;
            if (hour < 23)  //23時以降に実施
                return;

            try
            {
                dt = dt.AddDays(-31); //31日前
                string sqlstr = "DELETE FROM WtrHistory WHERE 日付 < '" + dt.ToString("yyyyMMddHHmm") + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //水履歴テーブルを作成する
        public static DataTable CreateWtrTable(ZaikoStr zs, SiteInfData sinf)
        {
            DataTable wtrtbl = new DataTable();
            wtrtbl.Columns.Add(new DataColumn("タンク番号", typeof(int)));
            wtrtbl.Columns.Add(new DataColumn("液種", typeof(string)));
            wtrtbl.Columns.Add(new DataColumn("水位", typeof(int)));
            wtrtbl.Columns.Add(new DataColumn("水量", typeof(int)));
            string[] wtrlvlar = zs.wtrlvllst.ToArray(); //水位取り出し
            string[] wtrvolar = zs.wtrvollst.ToArray(); //水量取り出し
            for (int i = 1; i <= zs.numtank; i++)
            {
                try
                {
                    DataRow drow = wtrtbl.NewRow();
                    drow["タンク番号"] = i;
                    drow["液種"] = sinf.GetOilTypeByTank(i);
                    int wtrlvl = int.Parse(wtrlvlar[i - 1]);
                    wtrlvl = (wtrlvl + 50) / 100;
                    drow["水位"] = wtrlvl;
                    int wtrvol = int.Parse(wtrvolar[i - 1]);
                    wtrvol = (wtrvol + 50) / 100;
                    drow["水量"] = wtrvol;
                    wtrtbl.Rows.Add(drow);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
            return wtrtbl;
        }
    }
}