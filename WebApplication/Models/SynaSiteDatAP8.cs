﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApplication.Models
{
    //シナネンAP-8のあるSSを管理するクラス
    public class SynaSiteDatAP8
    {
        private DataTable SynSiteDatAP8DT; //AP-8あり施設データテーブル
        private DataTable AP8CollectCfgDT; //AP-8自動集信ありなし管理テーブル

        //private const int IdColNo = 0;
        private const int SKKCodeColNo = 0;     //SKKコード
        private const int AutoCollectNo = 1;    //自動集信 0 -- 手動 1-- 自動
        private const int CollectTimeNo = 2;    //集信時間
        private const int SiteNameColNo = 3;

        //constructor
        public SynaSiteDatAP8()
        {
            DataTableCtrl.InitializeTable(SynSiteDatAP8DT);
        }

        //テーブル読み込み
        public void OpenTable()
        {
            try
            {
                string sqlstr = "SELECT SynaSiteDatAP8.SKKコード, SynaSiteDatAP8.AP8自動集信, SynaSiteDatAP8.集信時間, SynaSiteDat.SS名 FROM SynaSiteDatAP8 INNER JOIN SynaSiteDat ON SynaSiteDatAP8.SKKコード = SynaSiteDat.SKKコード";
                DataTableCtrl.InitializeTable(SynSiteDatAP8DT);
                SynSiteDatAP8DT = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SynSiteDatAP8DT);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            try
            {
                return SynSiteDatAP8DT.Rows.Count;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }

        //集信時刻設定テーブル作成
        public void CreateCollectDataCfg(List<string> ulist)
        {
            try
            {
                if (SynSiteDatAP8DT.Rows.Count == 0)
                    return;

                DataTableCtrl.InitializeTable(AP8CollectCfgDT);
                AP8CollectCfgDT = new DataTable();
                AP8CollectCfgDT.Columns.Add(new DataColumn("施設名", typeof(string)));
                AP8CollectCfgDT.Columns.Add(new DataColumn("集信条件", typeof(string)));

                int autocol = 0;
                string coltime = "";

                for (int i = 0; i < SynSiteDatAP8DT.Rows.Count; i++)
                {
                    string skkcode = SynSiteDatAP8DT.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                    bool bfound = false;
                    //drow["施設名"] = sysite.GetSiteNameBySkkcode(skkcode);
                    //施設データより自動集信ON/OFFをautocolに取り出す
                    foreach( string scode in ulist)
                    {
                        if( scode == skkcode)
                        {
                            bfound = true;
                            break;
                        }
                    }
                    if (bfound == false)
                        continue;
                    DataRow drow = AP8CollectCfgDT.NewRow();
                    drow["施設名"] = SynSiteDatAP8DT.Rows[i][SiteNameColNo].ToString().TrimEnd();
                    autocol = (int)SynSiteDatAP8DT.Rows[i][AutoCollectNo];
                    coltime = SynSiteDatAP8DT.Rows[i][CollectTimeNo].ToString().TrimEnd();

                    if (autocol == 0)
                        drow["集信条件"] = "手動集信";
                    else
                        drow["集信条件"] = "自動集信" + coltime.Insert(2, ":");
                    AP8CollectCfgDT.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //集信時刻設定テーブル作成
        public DataTable GetCollectDataCfgTbl()
        {
            try
            {
                return AP8CollectCfgDT;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }
        }
        //指定SKKコードのAP-8自動集信時間を書き込み
        public bool SetAutoCollectTime(string skkcode, string sttime)
        {
            bool res = false;
            try
            {
                string sqlstr = "UPDATE SynaSiteDatAP8 SET AP8自動集信='1', 集信時間='" + sttime + "' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return res;
            }
        }
        //指定SKKコードのAP-8自動集信をオフにする
        public bool SetAutoCollectOff(string skkcode)
        {
            bool res = false;
            try
            {
                string sqlstr = "UPDATE SynaSiteDatAP8 SET AP8自動集信='0' WHERE SKKコード= '" + skkcode + "'";
                DBCtrl.ExecNonQuery(sqlstr);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return res;
            }
        }
        //指定SS名に相当するSKKコードを取り出す
        public string GetSkkcodeBySiteName(string sitename)
        {
            string retskkcode = GlobalVar.DefSkkcode1;
            try
            {
                if (SynSiteDatAP8DT != null)
                {
                    for (int i = 0; i < SynSiteDatAP8DT.Rows.Count; i++)
                    {
                        if (SynSiteDatAP8DT.Rows[i][SiteNameColNo].ToString().TrimEnd() == sitename)
                            return SynSiteDatAP8DT.Rows[i][SKKCodeColNo].ToString().TrimEnd();
                    }
                    return retskkcode;
                }
                else
                {
                    return retskkcode;
                }
            }
            catch (Exception e)
            {
                //Debug.WriteLine(e.ToString());
                return retskkcode;
            }
        }
        //指定SS名に相当する自動集信設定を取り出す
        public AP8AutoCol GetAutoColBySiteName(string sitename)
        {
            AP8AutoCol ap8atcol = new AP8AutoCol();
            ap8atcol.autocol = 0;
            ap8atcol.coltime = "0000";
            try
            {
                if (SynSiteDatAP8DT != null)
                {
                    for (int i = 0; i < SynSiteDatAP8DT.Rows.Count; i++)
                    {
                        if (SynSiteDatAP8DT.Rows[i][SiteNameColNo].ToString().TrimEnd() == sitename)
                        {
                            ap8atcol.autocol = (int)SynSiteDatAP8DT.Rows[i][AutoCollectNo];
                            if( ap8atcol.autocol > 0 )
                                ap8atcol.coltime = SynSiteDatAP8DT.Rows[i][CollectTimeNo].ToString().TrimEnd();
                            break;
                        }
                    }
                    return ap8atcol;
                }
                else
                {
                    return ap8atcol;
                }
            }
            catch (Exception e)
            {
                //Debug.WriteLine(e.ToString());
                return ap8atcol;
            }
        }
        //指定行のSS名を取り出す
        public string GetSSNamebyLine(int lineno)
        {
            string sname = "";
            try
            {
                if (SynSiteDatAP8DT != null)
                {
                    sname = SynSiteDatAP8DT.Rows[lineno][SKKCodeColNo].ToString().TrimEnd();
                    return sname;
                }
                else
                {
                    return sname;
                }
            }
            catch (Exception e)
            {
                //Debug.WriteLine(e.ToString());
                return sname;
            }
        }


    }
}