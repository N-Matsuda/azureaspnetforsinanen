﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Diagnostics;

namespace WebApplication.Models
{
    public class CardLockData
    {
        DataTable CLStatusTbl;
        DataTable CLChkTbl;
        DataTable CLDataTbl;

        private const int CardtypeNo = 0;       //カード区分
        private const int CompcodeNo = 1;       //企業社コード
        private const int SscodeNo = 2;         //SSコード
        private const int ClientcodeNo= 3;      //得意先コード
        private const int CarnumNo=4;           //車両番号
        private const int ValidyearNo = 5;      //有効年
        private const int ValidmonthNo = 6;     //有効月
        private const int LockStatNo = 7;       //処理区分


        //constructor
        public CardLockData()
        {
            DataTableCtrl.InitializeTable(CLDataTbl);
        }
        //全てのCardLockデータを読み出す
        public void OpenCardLockDataAll()
        {
            try
            {
                string sqlstr = "SELECT * FROM SynaCardData WHERE 処理区分='01' ORDER BY 企業社コード ASC ";
                DataTableCtrl.InitializeTable(CLDataTbl);
                CLDataTbl = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, CLDataTbl);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //カード区分、企業社コード, SSコード, 得意先コード, 車両番号を指定して読み出す
        public void OpenCardLockData(string cardtype, string compcode, string sscode, string clientcode, string carnum)
        {
            try
            {
                string sqlstr = "SELECT * FROM SynaCardData WHERE カード区分='" + cardtype + "' AND 企業社コード='" + compcode + "' AND SSコード='" + sscode + "' AND 得意先コード ='" + clientcode + "' AND 車両番号='"+ carnum + "'";
                DataTableCtrl.InitializeTable(CLDataTbl);
                CLDataTbl = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, CLDataTbl);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //企業社コード一覧を取り出す
        public static List<string> GetCompanyCodeList()
        {
            List<string> cmpcode = new List<string>();
            try
            {
                string sqlstr = "SELECT DISTINCT 企業社コード FROM SynaCardData ORDER BY 企業社コード ASC";
                DataTable CompCodedt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, CompCodedt);
                cmpcode = CompCodedt.AsEnumerable().Select(row => row[0].ToString()).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return cmpcode;
        }
        //SS名一覧を取り出す
        public static List<string> GetSSCodeList()
        {
            List<string> sscode = new List<string>();
            try
            {
                string sqlstr = "SELECT DISTINCT SSコード FROM SynaCardData ORDER BY SSコード ASC";
                DataTable SSCodedt = new DataTable();
                DBCtrl.ExecSelectAndFillTable(sqlstr, SSCodedt);
                sscode = SSCodedt.AsEnumerable().Select(row => row[0].ToString()).ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return sscode;
        }

        //指定されたカード区分、企業社コード, SSコード, 得意先コード, 車両番号のカードロック状態を変更する
        public bool ChgCardLockStatus(string cardtype, string compcode, string sscode, string clientcode, string carnum, bool blocked)
        {
            bool bchged = true;
            try
            {
                string locksts = "01";
                if (blocked == false)
                    locksts = "02";
                string sqlstr = "UPDATE SynaCardData SET 処理区分='" + locksts + "' WHERE カード区分='" + cardtype + "' AND 企業社コード='" + compcode + "' AND SSコード='" + sscode + "' AND 得意先コード ='" + clientcode + "' AND 車両番号='" + carnum + "'";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                bchged = false;
                Console.WriteLine(ex.ToString());
            }
            return bchged;
        }

        //指定されたカード区分、企業社コード, SSコード, 得意先コード, 車両番号のカードロック状態を追加する
        public bool AddCardLockStatus(string cardtype, string compcode, string sscode, string clientcode, string carnum, bool blocked)
        {
            bool bchged = true;
            try
            {
                string locksts = "01";
                if (blocked == false)
                    locksts = "02";
                string sqlstr = "INSERT INTO SynaCardData VALUES('" + cardtype + "','" + compcode + "','" + sscode + "','" + clientcode + "','" + carnum + "','49','12','" + locksts + "')";
                DBCtrl.ExecNonQuery(sqlstr);
            }
            catch (Exception ex)
            {
                bchged = false;
                Console.WriteLine(ex.ToString());
            }
            return bchged;
        }

        //読み出したデータの数を返す
        public int GetNumCardLockData()
        {
            int num;
            try
            {
                num = CLDataTbl.Rows.Count;
            }
            catch (Exception ex)
            {
                num = 0;
                Console.WriteLine(ex.ToString());
            }
            return num;
        }
        
        //DBより読み出したカードデータより、送信用のカードデータ作成
        public string GetCardDataString()
        {
            string carddata = "C0000000000000000000000000000099\r\n";
            try
            {
                if (CLDataTbl != null) 
                {
                    for(int i=0; i<CLDataTbl.Rows.Count; i++)
                    {
                        carddata += "C00000";      //データ区分、予備
                        carddata += CLDataTbl.Rows[i][CardtypeNo].ToString().TrimEnd(); //カード区分
                        carddata += CLDataTbl.Rows[i][CompcodeNo].ToString().TrimEnd(); //企業社コード
                        carddata += CLDataTbl.Rows[i][SscodeNo].ToString().TrimEnd(); //SSコード
                        carddata += CLDataTbl.Rows[i][ClientcodeNo].ToString().TrimEnd(); //得意先コード
                        carddata += CLDataTbl.Rows[i][CarnumNo].ToString().TrimEnd(); //車両番号
                        carddata += CLDataTbl.Rows[i][ValidyearNo].ToString().TrimEnd(); //有効年
                        carddata += CLDataTbl.Rows[i][ValidmonthNo].ToString().TrimEnd(); //有効月
                        carddata += CLDataTbl.Rows[i][LockStatNo].ToString().TrimEnd(); //処理区分
                        carddata += "\r\n";
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return carddata;
        }

        //指定行カード区分取り出し
        public string GetCardType(int lineno)
        {
            try
            {
                if ((CLDataTbl != null) && (lineno < CLDataTbl.Rows.Count))
                {
                    return CLDataTbl.Rows[lineno][CardtypeNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //指定行企業社コード取り出し
        public string GetCompCode(int lineno)
        {
            try
            {
                if ((CLDataTbl != null) && (lineno < CLDataTbl.Rows.Count))
                {
                    return CLDataTbl.Rows[lineno][CompcodeNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //指定行SSコード取り出し
        public string GetSSCode(int lineno)
        {
            try
            {
                if ((CLDataTbl != null) && (lineno < CLDataTbl.Rows.Count))
                {
                    return CLDataTbl.Rows[lineno][SscodeNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
        //指定行得意先コード取り出し
        public string GetClientCode(int lineno)
        {
            try
            {
                if ((CLDataTbl != null) && (lineno < CLDataTbl.Rows.Count))
                {
                    return CLDataTbl.Rows[lineno][ClientcodeNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //指定行車両番号取り出し
        public string GetCarnumber(int lineno)
        {
            try
            {
                if ((CLDataTbl != null) && (lineno < CLDataTbl.Rows.Count))
                {
                    return CLDataTbl.Rows[lineno][CarnumNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
        //指定行有効年取り出し
        public string GetValidyear(int lineno)
        {
            try
            {
                if ((CLDataTbl != null) && (lineno < CLDataTbl.Rows.Count))
                {
                    return CLDataTbl.Rows[lineno][ValidyearNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
        //指定行有効月取り出し
        public string GetValidmonth(int lineno)
        {
            try
            {
                if ((CLDataTbl != null) && (lineno < CLDataTbl.Rows.Count))
                {
                    return CLDataTbl.Rows[lineno][ValidmonthNo].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }
        //指定行処理区分取り出し bool true = 登録(ロック解除状態)　false 削除(ロック状態)
        public bool GetLockstatus(int lineno)
        {
            bool stat = false;
            try
            {
                if ((CLDataTbl != null) && (lineno < CLDataTbl.Rows.Count))
                {
                    string statstr = CLDataTbl.Rows[lineno][LockStatNo].ToString().TrimEnd();
                    if (statstr == "01")
                        stat = true;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
            return stat;
        }

        //カードロックステータステーブル作成
        public void CreateCLStatusTbl(SynaSiteDat synsite)
        {
            try
            {
                DataTableCtrl.InitializeTable(CLStatusTbl);
                //テーブルのフィールドを定義する
                CLStatusTbl = new DataTable();
                CLStatusTbl.Columns.Add(new DataColumn("施設名", typeof(string)));
                CLStatusTbl.Columns.Add(new DataColumn("配信状況", typeof(string)));

                SynaSiteAP8Log ap8dly = new SynaSiteAP8Log();
                ap8dly.OpenDeliveryTableLatest(synsite);

                for ( int i=0; i< synsite.GetNumOfRecord(); i++)
                {
                    string timestr;
                    string logstr;
                    DataRow drow = CLStatusTbl.NewRow();
                    string sitename = ap8dly.GetCLSitename(i);
                    timestr = ap8dly.GetCLLogTime(i);
                    logstr = ap8dly.GetCLLog(i);
                    if (timestr != "")
                    {
                        DateTime dt = new DateTime(int.Parse(timestr.Substring(0, 4)), int.Parse(timestr.Substring(4, 2)), int.Parse(timestr.Substring(6, 2)), int.Parse(timestr.Substring(8, 2)), int.Parse(timestr.Substring(10, 2)), 0);
                        timestr = dt.ToString("yyyy/MM/dd HH:mm");
                        drow[0] = sitename;
                        if (logstr == GlobalVar.DelivOK)
                            drow[1] = timestr + " 配信成功";
                        else if(logstr == GlobalVar.AccessNG)
                            drow[1] = timestr + " 通信障害";
                        else
                            drow[1] = timestr + " 配信失敗";
                    } else
                    {
                        drow[0] = sitename;
                        drow[1] = "履歴なし";
                    }
                    CLStatusTbl.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        //カードロックステータステーブル取り出し
        public DataTable GetCLStatusTbl()
        {
            try
            {
                return CLStatusTbl;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }
        }

        //カードロックステータスチェックテーブル作成
        public void CreateCLChkTbl(SynaSiteDat synsite)
        {
            try
            {
                SynaSiteAP8Log ap8dly = new SynaSiteAP8Log();
                ap8dly.OpenDeliveryTableLatest(synsite);
                int num = ap8dly.GetNumOfDeliveryRecord();

                DataTableCtrl.InitializeTable(CLChkTbl);
                //テーブルのフィールドを定義する
                CLChkTbl = new DataTable();
                CLChkTbl.Columns.Add(new DataColumn("配信開始", typeof(bool)));
                CLChkTbl.Columns.Add(new DataColumn("施設名", typeof(string)));
                CLChkTbl.Columns.Add(new DataColumn("配信状況", typeof(string)));
                CLChkTbl.Columns.Add(new DataColumn("SKKコード", typeof(string)));

                string sitename;
                string timestr;
                string logstr;

                for (int i = 0; i < num; i++)
                {
                    DataRow drow = CLChkTbl.NewRow();
                    sitename = ap8dly.GetCLSitename(i);
                    timestr = ap8dly.GetCLLogTime(i);
                    logstr = ap8dly.GetCLLog(i);
                    
                    if( timestr != "")
                    {
                        DateTime dt = new DateTime(int.Parse(timestr.Substring(0, 4)), int.Parse(timestr.Substring(4, 2)), int.Parse(timestr.Substring(6, 2)), int.Parse(timestr.Substring(8, 2)), int.Parse(timestr.Substring(10, 2)), 0);
                        timestr = dt.ToString("yyyy/MM/dd HH:mm");
                        if (logstr == "200")
                            drow[2] = timestr + " 配信成功";
                        else
                            drow[2] = timestr + " 配信失敗";
                    } else
                    {
                        drow[2] = "履歴なし";
                    }
                    drow[0] = false;
                    drow[1] = sitename;
                    drow[3] = ap8dly.GetCLSkkcode(i);
                    CLChkTbl.Rows.Add(drow);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        //カードロックステータスチェックテーブル取り出し
        public DataTable GetCLChkTbl()
        {
            try
            {
                return CLChkTbl;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }
        }
        //カードロックステータスチェックテーブルの指定行よりSKKコードを取り出す
        public string GetSkkcodeFromStatusTable(int lineno)
        {
            try
            {
                if ((CLChkTbl != null) && (lineno < CLChkTbl.Rows.Count))
                {
                    return CLChkTbl.Rows[lineno][3].ToString().TrimEnd();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
                return "";
            }
        }

        //DBよりカードロックファイルを作成してブロブ上へアップロード
        public void CardLockUpload()
        {
            OpenCardLockDataAll();
            string cldata = GetCardDataString();
            FilePath fp = new FilePath();

            bool res = fp.UploadCardLockFile(cldata);
            //if (res == false)
            //    LogData.WriteLog("カードロックデータ書き込み失敗");
        }

        //カードロックファイルをブロブ上からダウンロードする。
        public string ReadCardLockData()
        {
            string cldata = "";
            FilePath fp = new FilePath();
            cldata += fp.DownloadCardLockFile();
            return cldata;
        }
        
    }
}