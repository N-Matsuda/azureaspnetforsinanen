﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class RunLengthComp
    {
        //AP8データ '0'のみRUNLENGTH圧縮
        //圧縮 '0'が4以上連続している場合, 例えば0が12連続している場合X12に置き換える。
        public static string RLDecompress(string data)
        {
            List<int> idxlst = new List<int>();
            List<int> sprptlst = new List<int>();
            int index = data.IndexOf("X");
            while (index >= 0)
            {
                idxlst.Add(index);
                string num = data.Substring(index + 1, 2);
                int rptcnt = int.Parse(num);
                sprptlst.Add(rptcnt);
                int nxtidx = index + 3;
                if (nxtidx < data.Length)
                {
                    index = data.IndexOf('X', nxtidx);
                }
                else
                {
                    break;
                }
            }
            int cnt = idxlst.Count;
            int[] idxarr = idxlst.ToArray();
            int[] sprptarr = sprptlst.ToArray();
            for (int i = cnt; i > 0; i--)
            {
                int idx = idxarr[i - 1];
                int rpt = sprptarr[i - 1];
                data = data.Remove(idx, 3);
                data = data.Insert(idx, new string('0', rpt));
            }
            data = data.Replace("da", "\r\n");
            if (!data.EndsWith("\n"))
            {
                data = data + "\r\n";
            }
            return data;
        }

        //圧縮
        public static string RLCompress(string data)
        {
            string outstr = "";
            int orglen = data.Length;
            int zerolen = 0;
            string ch;
            for (int i = 0; i < orglen; i++)
            {
                ch = data.Substring(i, 1);
                if (ch == "0")
                {
                    zerolen++;
                }
                else
                {
                    if (zerolen == 0)
                    {
                        outstr += ch;
                    }
                    else if (zerolen < 4)
                    {
                        for (int j = 0; j < zerolen; j++)
                        {
                            outstr += "0";
                        }
                        outstr += ch;
                    }
                    else if (zerolen >= 4)
                    {
                        outstr += "X";
                        outstr += zerolen.ToString("##");
                        outstr += ch;
                    }
                    zerolen = 0;
                }
            }
            return outstr;
        }

    }
}