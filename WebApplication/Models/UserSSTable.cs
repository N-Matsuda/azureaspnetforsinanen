﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebApplication.Models
{
    //各ユーザーが参照できるSkkコードを管理するクラスです
    public class UserSSTable
    {
        private DataTable UserSSTableDT;
        //private ConnectPortTable cpttbl;
        private DataTable CheckTableDT;
        private List<string> CheckedSSList;
        private const string stDisp = "表示";
        private const string stSkkcode = "SKKコード";
        private const string stSiteName = "施設名";

        //コンストラクター
        public UserSSTable()
        {
            DataTableCtrl.InitializeTable(UserSSTableDT);
        }

        //新規ユーザー作成
        public void CreateNewuser(string username)
        {
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                //aspnet_UsersよりUserNameを削除するまえにUserIDを取り出します。
                SqlConnection con = new SqlConnection();
                SqlCommand cmd = new SqlCommand();
                string sqlstr;
                con.ConnectionString = GlobalVar.DBCONNECTION;
                con.Open();
                cmd.CommandText = "SELECT ユーザー名 FROM SynaUserSite WHERE ユーザー名='" + username + "'";
                cmd.Connection = con;

                // SQLを実行します。
                SqlDataReader reader = cmd.ExecuteReader();

                string uname = "";
                // 結果を表示します。
                while (reader.Read())
                {
                    uname = (string)reader.GetValue(0);
                }
                reader.Close();
                if (uname == "")
                {
                    sqlstr = "INSERT INTO SynaUserSite (ユーザー名,サイトグループ1,サイトグループ2,サイトグループ3) VALUES ('" + username + "','','','')";
                    cmd = new SqlCommand(sqlstr, con);
                    cmd.ExecuteNonQuery();
                }
                con.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

        }

        //グループテーブル読み込み
        public List<string> OpenTable(string username)
        {
            CheckedSSList = new List<string>();
            try
            {
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr = "SELECT ユーザー名,サイトグループ1 FROM SynaUserSite WHERE ユーザー名  ='" + username + "'";
                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                DataTableCtrl.InitializeTable(UserSSTableDT);
                UserSSTableDT = new DataTable();
                dAdp.Fill(UserSSTableDT);
                cn.Close();

                string unamestr = UserSSTableDT.Rows[0][1].ToString();
                string[] chkdskkcode = new string[0];
                if (unamestr != "")
                {
                    chkdskkcode = unamestr.Split(',');
                }
                CheckedSSList.AddRange(chkdskkcode);

                //ConnectPortTable cpttbl = new ConnectPortTable();
                //cpttbl.OpenTable(GlobalVar.CompanyName);
                //CheckTableDT = new DataTable();
                //CheckTableDT.Columns.Add(new DataColumn(stDisp, typeof(bool)));
                //CheckTableDT.Columns.Add(new DataColumn(stSiteName, typeof(string)));
                //CheckTableDT.Columns.Add(new DataColumn(stSkkcode, typeof(string)));
                //string[] ssname = cpttbl.GetSiteNameList().ToArray();
                //string[] skkcode = cpttbl.GetSKKCodeList().ToArray();
#if false
            bool found = false;
                for( int i=0; i< ssname.Length; i++, found = false )
                {
                    DataRow drow = CheckTableDT.NewRow();
                    drow[stSkkcode] = skkcode[i];
                    drow[stSiteName] = ssname[i];
                    for (int j = 0; j < chkdskkcode.Length; j++)
                    {
                        if (skkcode[i] == chkdskkcode[j])
                        {
                            found = true;
                            break;
                        }
                    }
                    if( found == true )
                        drow[stDisp] = true;
                    else
                        drow[stDisp] = false;
                    CheckTableDT.Rows.Add(drow);
                }
#endif
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return CheckedSSList;
        }

        public DataTable GetCheckTable()
        {
            try
            {
                return CheckTableDT;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                DataTable dt = new DataTable();
                return dt;
            }

        }

        public bool RegTable(List<string> chksites, string username)
        {
            bool ret = true;
            try
            {
                string sitelst = "";
                bool firstss = true;
                foreach (string siten in chksites)
                {
                    if (firstss == true)
                    {
                        sitelst += siten;
                        firstss = false;
                    }
                    else
                    {
                        sitelst += "," + siten;
                    }
                }
                sitelst += ",";
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                string sqlstr = "UPDATE SynaUserSite SET サイトグループ1 = '" + sitelst + "' WHERE ユーザー名 ='" + username + "'";

                System.Data.SqlClient.SqlCommand Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                ret = false;
            }
            return ret;
        }

        //ユーザー名とユーザー名でアクセスできる施設(SKKコード)の対応表(0:未対応 1:対応)をロードする。
        public bool LoadUploadedCSV(string path)
        {
            bool res = true;
            DataTableCtrl.InitializeTable(UploadTableDT);
            CsvData csv = new CsvData();
            UploadTableDT = csv.ReadCSV(path, ',', false);

            return res;
        }
        //上記でロードした対応表よりユーザーテーブルを作成して、DB更新
        public bool RegistUploadedCSV()
        {
            bool res = false;
            try
            {
                if (UploadTableDT == null)
                    return res;
                string uname;
                List<string> sitelist = new List<string>();
                List<string> chksite = new List<string>();
                //1行目よりSKKコード一覧を取得
                for (int i = 1; i < UploadTableDT.Columns.Count; i++)
                {
                    sitelist.Add(UploadTableDT.Rows[0][i].ToString());
                }

                //2行目以降は　ユーザー名 + 各SKKコードごとの 0(未対応), 1(対応)
                string[] sitear = sitelist.ToArray();
                for ( int i=1; i< UploadTableDT.Rows.Count; i++, chksite.Clear())
                {
                    uname = UploadTableDT.Rows[i][0].ToString();  //ユーザー名取得

                    //ここでunameが有効なアカウントか登録する処理必要あり？

                    for (int j = 1; j < UploadTableDT.Columns.Count; j++)
                    {
                        if (UploadTableDT.Rows[i][j].ToString() == "1")
                            chksite.Add(sitear[i - 1]);
                    }
                    RegTable(chksite, uname);
                }
                res = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                res = false;
            }
            return res;
        }

    }
}