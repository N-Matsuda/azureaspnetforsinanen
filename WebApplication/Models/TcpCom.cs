﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Sockets;


namespace WebApplication.Models
{
    //AP8に対してTCP通信を行うクラス
    public class TcpCom
    {
        //TCP通信用
        protected byte[] tcpSndBuf;
        protected byte[] tcpRcvBuf;
        protected TcpListener TcpServer;
        protected const int PortNumber = 10001;
        protected const int tcpSndBufSize = 1024;
        protected const int tcpRcvBufSize = 1024;
        public string tcpStr;
        public string idcode;
        public bool IsBusy;

        //constructor
        public TcpCom()
        {
            tcpSndBuf = new byte[tcpSndBufSize];
            tcpRcvBuf = new byte[tcpRcvBufSize];
            IsBusy = true;
            tcpStr = "";
        }

        //実行モジュール 指定のSSに対してコマンドを送信
        public bool SendAP8Com(string skkcode, string comstr, string compname)
        {
            bool ret = false;
            try
            {
                TcpClient tcp;
                SynaSiteDat synsite = new SynaSiteDat();
                synsite.OpenTableWithSkkcode(skkcode, compname);
                string ipadr = synsite.GetAddress(0);
                int portno = synsite.GetPortNo(0);

                tcp = new System.Net.Sockets.TcpClient(ipadr, portno);
                tcp.SendTimeout = 500;
                tcp.ReceiveTimeout = 3000;
                NetworkStream stream = tcp.GetStream();
                byte[] sndbyte = System.Text.Encoding.ASCII.GetBytes(comstr);
                stream.Write(sndbyte, 0, comstr.Length);

                int readbyte;

                while (true)
                {
                    readbyte = stream.Read(tcpRcvBuf, 0, tcpRcvBufSize);
                    if (readbyte > 0)
                        break;
                }
                stream.Close();
                tcpStr += System.Text.Encoding.ASCII.GetString(tcpRcvBuf);
                tcp.Close();
                if( tcpStr.Substring(0,2) == "OK")
                    ret = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return ret;
        }

    }
}