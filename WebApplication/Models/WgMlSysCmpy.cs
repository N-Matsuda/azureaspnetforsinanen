﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;

namespace WebApplication.Models
{
    public class WgMlSysCmpy
    {
        private DataSet WgMlSysCmpyDS;
        private DataTable WgMlSysCmpyDT;
        private const string stWgMlSysCmpy = "メール警報システム会社情報";
        private const int COLCMPNAME = 1;   //会社名
        private const int COLATGWRGCFG = 2; //液面計警報有無
        private const int COLODCTSYSCFG = 3; //油漏えい検知警報有無
        private const int COLMLADR1 = 4;    //メールアドレス1
        private const int COLMLNAME1 = 5;    //メールアドレス1表示名
        private const int COLMLADR2 = 6;    //メールアドレス2
        private const int COLMLNAME2 = 7;    //メールアドレス2表示名
        private const int COLMLADR3 = 8;    //メールアドレス3
        private const int COLMLNAME3 = 9;    //メールアドレス3表示名
        private const int COLACCNT = 10;     //アカウント情報
        private const int COLDPNAME = 11;    //表示名

        private string cmpnyname;
        private string accountname;
        byte[] atgwrgcfg;           //液面計警報有無
        byte[] odctsyscfg;          //油漏えい検知警報有無

        //コンストラクター
        public WgMlSysCmpy()
        {
            if (WgMlSysCmpyDS != null)
            {
                WgMlSysCmpyDS.Clear();
                WgMlSysCmpyDS.Tables.Clear();
                WgMlSysCmpyDS.Dispose();
                WgMlSysCmpyDS = null;
            }
        }

        //会社名でテーブル読み込み
        public bool OpenTable(string cmpny)
        {
            bool bret = true;
            try
            {
                //string dbpath;
                cmpnyname = cmpny;

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                if (cmpny == "*")
                    sqlstr = "SELECT * FROM WgMlSysCmpy";
                else
                    sqlstr = "SELECT * FROM WgMlSysCmpy WHERE 会社名= '" + cmpnyname + "'";

                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysCmpyDS != null)
                {
                    WgMlSysCmpyDS.Clear();
                    WgMlSysCmpyDS.Tables.Clear();
                    WgMlSysCmpyDS.Dispose();
                    WgMlSysCmpyDS = null;
                }
                WgMlSysCmpyDS = new DataSet("WgMlSysCmpy");
                WgMlSysCmpyDS.Tables.Add(stWgMlSysCmpy);
                WgMlSysCmpyDT = WgMlSysCmpyDS.Tables[stWgMlSysCmpy];

                dAdp.Fill(WgMlSysCmpyDS, stWgMlSysCmpy);
                cn.Close();
            }
            catch (Exception ex)
            {
                bret = false;
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }

        //アカウント名でテーブル読み込み
        public bool OpenTableAccount(string account)
        {
            bool bret = true;
            try
            {
                //string dbpath;
                accountname = account;

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;
                if (account == "*")
                    sqlstr = "SELECT * FROM WgMlSysCmpy";
                else
                    sqlstr = "SELECT * FROM WgMlSysCmpy WHERE アカウント= '" + accountname + "'";

                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysCmpyDS != null)
                {
                    WgMlSysCmpyDS.Clear();
                    WgMlSysCmpyDS.Tables.Clear();
                    WgMlSysCmpyDS.Dispose();
                    WgMlSysCmpyDS = null;
                }
                WgMlSysCmpyDS = new DataSet("WgMlSysCmpy");
                WgMlSysCmpyDS.Tables.Add(stWgMlSysCmpy);
                WgMlSysCmpyDT = WgMlSysCmpyDS.Tables[stWgMlSysCmpy];

                dAdp.Fill(WgMlSysCmpyDS, stWgMlSysCmpy);
                cn.Close();
            }
            catch (Exception ex)
            {
                bret = false;
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }

        //警報設定のアップデート
        public bool UpdateATGWrgCfg()
        {
            bool bret = true;
            try
            {
                string wrgstr = System.Text.Encoding.ASCII.GetString(atgwrgcfg);
                string odstr = System.Text.Encoding.ASCII.GetString(odctsyscfg);
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                sqlstr = "UPDATE WgMlSysCmpy SET 液面計警報有無= '" + wrgstr + "', 油漏えい検知警報有無= '" + odstr + "' WHERE 会社名= '" + cmpnyname + "'";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                bret = false;
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //警報設定のアップデート
        public bool UpdateMailAdr()
        {
            bool bret = true;
            try
            {
                string mladr1 = WgMlSysCmpyDT.Rows[0][COLMLADR1].ToString();
                string mladr2 = WgMlSysCmpyDT.Rows[0][COLMLADR2].ToString();
                string mladr3 = WgMlSysCmpyDT.Rows[0][COLMLADR3].ToString();
                string mlname1 = WgMlSysCmpyDT.Rows[0][COLMLNAME1].ToString();
                string mlname2 = WgMlSysCmpyDT.Rows[0][COLMLNAME2].ToString();
                string mlname3 = WgMlSysCmpyDT.Rows[0][COLMLNAME3].ToString();
                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                sqlstr = "UPDATE WgMlSysCmpy SET メールアドレス1= '" + mladr1 + "', アドレス表示名1= '" + mlname1 + "', メールアドレス2= '" + mladr2 + "', アドレス表示名2= '" + mlname2 + "', メールアドレス3= '" + mladr3 + "', アドレス表示名3= '" + mlname3 +
                "' WHERE 会社名= '" + cmpnyname + "'";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                bret = false;
                Console.WriteLine(e.ToString());
            }
            return bret;
        }


        //会社名一覧取得
        public List<string> GetCompanyNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < WgMlSysCmpyDT.Rows.Count; i++)
            {
                list.Add(WgMlSysCmpyDT.Rows[i][COLCMPNAME].ToString().TrimEnd());
            }
            return list;
        }

        //会社名取得
        public string GetCompanyName()
        {
            return WgMlSysCmpyDT.Rows[0][COLCMPNAME].ToString().TrimEnd();
        }

        //表示名取得
        public string GetDispName()
        {
            return WgMlSysCmpyDT.Rows[0][COLDPNAME].ToString().TrimEnd();
        }


        //レコード数取得
        public int GetNumOfRecord()
        {
            return WgMlSysCmpyDT.Rows.Count;
        }

        //メールアドレス1取得
        public string GetMlAddr1()
        {
            string mladr = "";
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    mladr = WgMlSysCmpyDT.Rows[0][COLMLADR1].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス1表示名取得
        public string GetMlAddrName1()
        {
            string mladr = "";
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    mladr = WgMlSysCmpyDT.Rows[0][COLMLNAME1].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス2取得
        public string GetMlAddr2()
        {
            string mladr = "";
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    mladr = WgMlSysCmpyDT.Rows[0][COLMLADR2].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス2表示名取得
        public string GetMlAddrName2()
        {
            string mladr = "";
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    mladr = WgMlSysCmpyDT.Rows[0][COLMLNAME2].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス3取得
        public string GetMlAddr3()
        {
            string mladr = "";
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    mladr = WgMlSysCmpyDT.Rows[0][COLMLADR3].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス3表示名取得
        public string GetMlAddrName3()
        {
            string mladr = "";
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    mladr = WgMlSysCmpyDT.Rows[0][COLMLNAME3].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス1設定
        public void SetMlAddr1(string mladr)
        {
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    WgMlSysCmpyDT.Rows[0][COLMLADR1] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス1表示名設定
        public void SetMlAddrName1(string mladr)
        {
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    WgMlSysCmpyDT.Rows[0][COLMLNAME1] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2設定
        public void SetMlAddr2(string mladr)
        {
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    WgMlSysCmpyDT.Rows[0][COLMLADR2] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2表示名設定
        public void SetMlAddrName2(string mladr)
        {
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    WgMlSysCmpyDT.Rows[0][COLMLNAME2] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス3設定
        public void SetMlAddr3(string mladr)
        {
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    WgMlSysCmpyDT.Rows[0][COLMLADR3] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス3表示名設定
        public void SetMlAddrName3(string mladr)
        {
            try
            {
                if (WgMlSysCmpyDT != null)
                {
                    WgMlSysCmpyDT.Rows[0][COLMLNAME3] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //指定行の 液面計警報有無　液面計警報状態　油漏えい検知警報有無　油漏えい検知警報状態を取り出す
        public void OpenLine(int lineno)
        {
            try
            {
                if ((WgMlSysCmpyDT != null) && (lineno < WgMlSysCmpyDT.Rows.Count))
                {
                    atgwrgcfg = Encoding.ASCII.GetBytes(WgMlSysCmpyDT.Rows[lineno][COLATGWRGCFG].ToString().TrimEnd());
                    odctsyscfg = Encoding.ASCII.GetBytes(WgMlSysCmpyDT.Rows[lineno][COLODCTSYSCFG].ToString().TrimEnd());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の減警報有り無しを取り出す
        public bool ChkLowWrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[0] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の満警報有り無しを取り出す
        public bool ChkHighWrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[1] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の水検知警報有り無しを取り出す
        public bool ChkWtrWrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[2] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のセンサー異常警報有り無しを取り出す
        public bool ChkSnsWrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[3] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のオイルリーク警報有り無しを取り出す
        public bool ChkLkWrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[4] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の測定下限警報有り無しを取り出す
        public bool ChkLowLimWrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[5] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-1限警報有り無しを取り出す
        public bool ChkLC1WrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[6] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-4限警報有り無しを取り出す
        public bool ChkLC4WrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[7] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-5限警報有り無しを取り出す
        public bool ChkLC5WrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[8] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-7限警報有り無しを取り出す
        public bool ChkLC7WrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[9] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行のLC-8限警報有り無しを取り出す
        public bool ChkLC8WrgCfg()
        {
            bool bret = false;
            try
            {
                if (atgwrgcfg[10] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムの油漏えいセンサー通信エラー警報有り無しを取り出す
        public bool ChkLkSysComErrCfg()
        {
            bool bret = false;
            try
            {
                if (odctsyscfg[0] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムの油漏えい水検知エラー警報有り無しを取り出す
        public bool ChkLkSysWtrDtctCfg()
        {
            bool bret = false;
            try
            {
                if (odctsyscfg[1] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の油漏えい検知システムの油漏えい油検知エラー警報有り無しを取り出す
        public bool ChkLkSysOilDtctCfg()
        {
            bool bret = false;
            try
            {
                if (odctsyscfg[2] == 0x31)
                    bret = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

        //現在行の減警報有り無し設定を変更する
        public void ChgLowWrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[0] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の満警報有り無し設定を変更する
        public void ChgHighWrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[1] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の水検知警報有り無し設定を変更する
        public void ChgWtrWrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[2] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のセンサー異常警報有り無し設定を変更する
        public void ChgSnsWrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[3] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のオイルリーク警報有り無し設定を変更する
        public void ChgLkWrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[4] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の測定下限警報有り無し設定を変更する
        public void ChgLowLimWrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[5] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-1限警報有り無し設定を変更する
        public void ChgLC1WrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[6] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-4限警報有り無し設定を変更する
        public void ChgLC4WrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[7] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-5限警報有り無し設定を変更する
        public void ChgLC5WrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[8] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-7限警報有り無し設定を変更する
        public void ChgLC7WrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[9] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行のLC-8限警報有り無し設定を変更する
        public void ChgLC8WrgCfg(bool bval)
        {
            try
            {
                atgwrgcfg[10] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムの油漏えいセンサー通信エラー警報有り無し設定を変更する
        public void ChgLkSysComErrCfg(bool bval)
        {
            try
            {
                odctsyscfg[0] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムの油漏えい水検知エラー警報有り無し設定を変更する
        public void ChgLkSysWtrDtctCfg(bool bval)
        {
            try
            {
                odctsyscfg[1] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        //現在行の油漏えい検知システムの油漏えい油検知エラー警報有り無しを変更する
        public void ChgLkSysOilDtctCfg(bool bval)
        {
            try
            {
                odctsyscfg[2] = (bval == true) ? (byte)0x31 : (byte)0x30;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

    }
}