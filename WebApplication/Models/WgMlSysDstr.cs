﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace WebApplication.Models
{
    public class WgMlSysDstr
    {
        private DataSet WgMlSysDstrDS;
        private DataTable WgMlSysDstrDT;
        private const string stWgMlSysDstr = "メール警報システム特約店情報";
        private const int COLDSTRNAME = 1;   //特約店名
        private const int COLCMPNAME = 2;   //会社名
        private const int COLMLADR1 = 3;    //メールアドレス1
        private const int COLMLNAME1 = 4;    //メールアドレス1表示名
        private const int COLMLADR2 = 5;    //メールアドレス2
        private const int COLMLNAME2 = 6;    //メールアドレス2表示名
        private const int COLMLADR3 = 7;    //メールアドレス3
        private const int COLMLNAME3 = 8;    //メールアドレス3表示名

        private string cmpnyname;
        private string dstrname; //特約店名
        //コンストラクター
        public WgMlSysDstr()
        {
            if (WgMlSysDstrDS != null)
            {
                WgMlSysDstrDS.Clear();
                WgMlSysDstrDS.Tables.Clear();
                WgMlSysDstrDS.Dispose();
                WgMlSysDstrDS = null;
            }
        }

        //テーブル読み込み
        public bool OpenTable(string cmpny, string dstr)
        {
            bool bret = true;
            try
            {
                //string dbpath;
                cmpnyname = cmpny;
                dstrname = dstr;

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                string sqlstr;

                if (((cmpny == "*") || (cmpny == "")) && ((dstr == "*") || (dstr == "")))
                    sqlstr = "SELECT * FROM WgMlSysDstr";
                else if ((dstr == "*") || (dstr == ""))
                    sqlstr = "SELECT * FROM WgMlSysDstr WHERE 会社名= '" + cmpny + "'";
                else
                    sqlstr = "SELECT * FROM WgMlSysDstr WHERE 会社名= '" + cmpny + "' AND 特約店名= '" + dstr + "'";

                System.Data.SqlClient.SqlDataAdapter dAdp = new System.Data.SqlClient.SqlDataAdapter(sqlstr, cn);
                if (WgMlSysDstrDS != null)
                {
                    WgMlSysDstrDS.Clear();
                    WgMlSysDstrDS.Tables.Clear();
                    WgMlSysDstrDS.Dispose();
                    WgMlSysDstrDS = null;
                }
                WgMlSysDstrDS = new DataSet("WgMlSysDstr");
                WgMlSysDstrDS.Tables.Add(stWgMlSysDstr);
                WgMlSysDstrDT = WgMlSysDstrDS.Tables[stWgMlSysDstr];

                dAdp.Fill(WgMlSysDstrDS, stWgMlSysDstr);
                cn.Close();
            }
            catch (Exception ex)
            {
                bret = false;
                Console.WriteLine(ex.ToString());
            }
            return bret;
        }

        //レコード数取得
        public int GetNumOfRecord()
        {
            return WgMlSysDstrDT.Rows.Count;
        }

        //メールアドレス1取得
        public string GetMlAddr1(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysDstrDT != null) && (lineno < WgMlSysDstrDT.Rows.Count))
                {
                    mladr = WgMlSysDstrDT.Rows[lineno][COLMLADR1].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス1表示名取得
        public string GetMlAddrName1(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysDstrDT != null) && (lineno < WgMlSysDstrDT.Rows.Count))
                {
                    mladr = WgMlSysDstrDT.Rows[lineno][COLMLNAME1].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス2取得
        public string GetMlAddr2(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysDstrDT != null) && (lineno < WgMlSysDstrDT.Rows.Count))
                {
                    mladr = WgMlSysDstrDT.Rows[lineno][COLMLADR2].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス2表示名取得
        public string GetMlAddrName2(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysDstrDT != null) && (lineno < WgMlSysDstrDT.Rows.Count))
                {
                    mladr = WgMlSysDstrDT.Rows[lineno][COLMLNAME2].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス3取得
        public string GetMlAddr3(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysDstrDT != null) && (lineno < WgMlSysDstrDT.Rows.Count))
                {
                    mladr = WgMlSysDstrDT.Rows[lineno][COLMLADR3].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //メールアドレス3表示名取得
        public string GetMlAddrName3(int lineno)
        {
            string mladr = "";
            try
            {
                if ((WgMlSysDstrDT != null) && (lineno < WgMlSysDstrDT.Rows.Count))
                {
                    mladr = WgMlSysDstrDT.Rows[lineno][COLMLNAME3].ToString().TrimEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return mladr;
        }

        //特約店リスト取得
        //会社名一覧取得
        public List<string> GetDistrNames()
        {
            List<string> list = new List<string>();
            for (int i = 0; i < WgMlSysDstrDT.Rows.Count; i++)
            {
                list.Add(WgMlSysDstrDT.Rows[i][COLDSTRNAME].ToString().TrimEnd());
            }
            return list;
        }

        //メールアドレス1設定
        public void SetMlAddr1(string mladr)
        {
            try
            {
                if (WgMlSysDstrDT != null)
                {
                    WgMlSysDstrDT.Rows[0][COLMLADR1] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス1表示名設定
        public void SetMlAddrName1(string mladr)
        {
            try
            {
                if (WgMlSysDstrDT != null)
                {
                    WgMlSysDstrDT.Rows[0][COLMLNAME1] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2設定
        public void SetMlAddr2(string mladr)
        {
            try
            {
                if (WgMlSysDstrDT != null)
                {
                    WgMlSysDstrDT.Rows[0][COLMLADR2] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス2表示名設定
        public void SetMlAddrName2(string mladr)
        {
            try
            {
                if (WgMlSysDstrDT != null)
                {
                    WgMlSysDstrDT.Rows[0][COLMLNAME2] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス3設定
        public void SetMlAddr3(string mladr)
        {
            try
            {
                if (WgMlSysDstrDT != null)
                {
                    WgMlSysDstrDT.Rows[0][COLMLADR3] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス3表示名設定
        public void SetMlAddrName3(string mladr)
        {
            try
            {
                if (WgMlSysDstrDT != null)
                {
                    WgMlSysDstrDT.Rows[0][COLMLNAME3] = mladr;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            return;
        }

        //メールアドレス、警報設定のアップデート
        public bool UpdateMailAdr()
        {
            bool bret = true;
            try
            {
                string mladr1 = WgMlSysDstrDT.Rows[0][COLMLADR1].ToString();
                string mladr2 = WgMlSysDstrDT.Rows[0][COLMLADR2].ToString();
                string mladr3 = WgMlSysDstrDT.Rows[0][COLMLADR3].ToString();
                string mlname1 = WgMlSysDstrDT.Rows[0][COLMLNAME1].ToString();
                string mlname2 = WgMlSysDstrDT.Rows[0][COLMLNAME2].ToString();
                string mlname3 = WgMlSysDstrDT.Rows[0][COLMLNAME3].ToString();

                string cnstr = GlobalVar.DBCONNECTION;
                System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(cnstr);
                cn.Open();
                System.Data.SqlClient.SqlCommand Com;
                string sqlstr;
                sqlstr = "UPDATE WgMlSysDstr SET メールアドレス1= '" + mladr1 + "', アドレス表示名1= '" + mlname1 + "', メールアドレス2= '" + mladr2 + "', アドレス表示名2= '" + mlname2 + "', メールアドレス3= '" + mladr3 + "', アドレス表示名3= '" + mlname3 +
                    "' WHERE 特約店名 = '" + dstrname + "'";
                Com = new System.Data.SqlClient.SqlCommand(sqlstr, cn);
                Com.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception e)
            {
                bret = false;
                Console.WriteLine(e.ToString());
            }
            return bret;
        }

    }
}