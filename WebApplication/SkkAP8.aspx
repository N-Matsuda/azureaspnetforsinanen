﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SkkMaster.Master" AutoEventWireup="true" CodeBehind="SkkAP8.aspx.cs" Inherits="WebApplication1.SkkAP8" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Tabs" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="Panel4" runat="server" BackColor="#102C44" ForeColor="#99CCFF" Height="30px">
          &nbsp;&nbsp;&nbsp;<asp:Label ID="TitleLB" runat="server" Text="Label" ForeColor="White" Width="620px"></asp:Label>
          <asp:Button ID="AP8LeveBtn" runat="server" OnClick="AP8LeveBtn_Click" Text="レベビジョン" Height="28px" Width="120px" />
          &nbsp;
          <asp:Button ID="AP8SettingBtn" runat="server" OnClick="AP8SettingBtn_Click" Text="  設定  " Height="28px" Width="84px" />
          &nbsp; <asp:Button ID="AP8LogoutBtn" runat="server" OnClick="AP8LogoutBtn_Click" Text="ログアウト" Height="28px" Width="84px" />
        <br />
        <br />
    </asp:Panel>
    <br />
    <wijmo:C1Tabs ID="C1Tabs3" runat="server" AutoPostBack="True" OnSelectedChanged="C1Tab3_SelectedChanged">
        <Pages>
            <wijmo:C1TabPage runat="server" Text="集信状況" StaticKey="" ID="C1Tabs3_Tab1">
                <br />
                <asp:CheckBox ID="CheckBox1" runat="server" Text="全ての施設にチェックを入れる/チェックを解除する。" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged" />
                <br />
                <br />
                <wijmo:C1GridView ID="C1GridViewAP8Stat" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                    <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                    <Columns>
                        <wijmo:C1CheckBoxField DataField="集信" HeaderText="集信" Width="100px">
                        </wijmo:C1CheckBoxField>
                        <wijmo:C1BoundField DataField="施設名" HeaderText="施設名" Width="200px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="状況" HeaderText="AP-8集信状況" Width="300px">
                        </wijmo:C1BoundField>
                    </Columns>
                </wijmo:C1GridView>
                <br />
                <br />
                <br />
                <asp:Button ID="StartButton" runat="server" OnClick="StartButton_Click" Text="選択した施設のAP-8から清算後受信" />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage runat="server" Text="個別集信状況" StaticKey="" ID="C1Tabs3_Tab2" TabIndex="1">
                <br />
                <asp:Label ID="Label1" runat="server" Text=" 施設名選択  "></asp:Label>
                &nbsp;&nbsp;
                <asp:DropDownList ID="SelectSSDDList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SelectSSDDList_SelectedIndexChanged" Width="200px">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="PrevSSButton" runat="server" OnClick="PrevSSButton_Click" Text="前の施設" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="NextSSButton" runat="server" OnClick="NextSSButton_Click" Text="次の施設" />
                <br />
                <br />
                <wijmo:C1GridView ID="C1GridViewAP8IndvStatus" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                    <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                    <Columns>
                        <wijmo:C1BoundField DataField="状況" HeaderText="最近のAP-8集信状況" ReadOnly="True" Width="500px">
                        </wijmo:C1BoundField>
                    </Columns>
                </wijmo:C1GridView>
                <br />
                <asp:Label ID="Label2" runat="server" Text="右のボタンをクリックすると当施設のAP-8に対して生産要求後、集信データを受信します。"></asp:Label>
                　　　　　<asp:Button ID="CollectButton" runat="server" OnClick="CollectButton_Click" Text="集信" />
                <br />
                <br />
                <asp:Label ID="Label3" runat="server" Text="右のボタンをクリックすると当施設のAP-8の直近の清算済の集信データを受信します。"></asp:Label>
                　　　　　　&nbsp;
                <asp:Button ID="ReCollectButton" runat="server" OnClick="ReCollectButton_Click" Text="再集信" />
                <br />
                <br />
                <asp:Label ID="Label4" runat="server" Text="右のボタンをクリックすると当施設のAP-8の直近の集信データをPCにダウンロードします。"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="DownloadButton" runat="server" OnClick="DownloadButton_Click" Text="ダウンロード" />
                <br />
                <br />
                <asp:Label ID="Label17" runat="server" Text="右のリストボックスで日付を指定し、右のボタンをクリックすると当施設の指定日付の集信データをPCにダウンロードします。"></asp:Label>
                <br />
                <br />
                <asp:ListBox ID="AP8DatDatesLB" runat="server" Rows="1" Width="200px"></asp:ListBox>
                &nbsp;&nbsp;
                <asp:Button ID="SelectedDateDLBtn" runat="server" OnClick="SelectedDateDLBtn_Click" Text="指定日データダウンロード" />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage runat="server" Text="集信設定" StaticKey="" ID="C1Tabs3_Tab3" TabIndex="2">
                <asp:Label ID="Label5" runat="server" Font-Size="X-Large" Text="AP-8集信設定"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label6" runat="server" Text="集信条件を自動集信にする場合は、施設を選択後、時間を指定してください。"></asp:Label>
                　<br /> 
                <br />
                施設名　<asp:ListBox ID="CfgTabSSNameLB" runat="server" Rows="1" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="CfgTabSSNameLB_SelectedIndexChanged"></asp:ListBox>
                &nbsp;&nbsp;
                <br />
                <br />
                時間　　<asp:ListBox ID="TimeSlctLB" runat="server" Rows="1" Width="100px"></asp:ListBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="AutoColSetBtn" runat="server" OnClick="AutoColSetBtn_Click" Text="設定" />
                &nbsp;&nbsp;
                <asp:Button ID="AutoColRelBtn" runat="server" OnClick="AutoColRelBtn_Click" Text="解除" />
                <br />
                <br />
                <wijmo:C1GridView ID="APCfgGridView" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" GroupAreaCaption="列でグループ化するには、列をここにドラッグします。" LoadingText="読み込み中…" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1">
                    <PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ" />
                    <Columns>
                        <wijmo:C1BoundField DataField="施設名" HeaderText="施設名" ReadOnly="True" Width="200px">
                        </wijmo:C1BoundField>
                        <wijmo:C1BoundField DataField="集信条件" HeaderText="集信条件" ReadOnly="True" Width="300px">
                        </wijmo:C1BoundField>
                    </Columns>
                </wijmo:C1GridView>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage runat="server" Text="カードロック配信" StaticKey="" ID="C1Tabs3_Tab4" TabIndex="3">
                <asp:Panel ID="Panel2" runat="server" BorderStyle="Double">
                    <br />
                    &nbsp;&nbsp;
                    <asp:Label ID="Label15" runat="server" Font-Bold="True" Font-Size="Large" Text="カードロック配信実行"></asp:Label>
                    　　&nbsp;
                    <asp:Button ID="StartDeliveryBtn" runat="server" OnClick="StartDeliveryBtn_Click" Text="直ちに配信" />
                    <br />
                    &nbsp;&nbsp;
                    <wijmo:C1GridView ID="DeliveryStatusGV1" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: 0px; left: 0px">
<PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ"></PagerSettings>
                        <Columns>
                            <wijmo:C1BoundField DataField="施設名" HeaderText="施設名" Width="200px">
                            </wijmo:C1BoundField>
                            <wijmo:C1BoundField DataField="配信状況" HeaderText="配信状況" Width="300px">
                            </wijmo:C1BoundField>
                        </Columns>
                    </wijmo:C1GridView>
                    <br />
                </asp:Panel>
                <br />
                <asp:Panel ID="Panel3" runat="server" BorderStyle="Double">
                    &nbsp;<br /> &nbsp;&nbsp;
                    <asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Size="Large" Text="カードロック配信状況"></asp:Label>
                    <br />
                    <br />
                    　<asp:Label ID="DeliveryStatusLabel" runat="server" Text="更新日時　2018年9月21日"></asp:Label>
                    <br />
                    <br />
                    　<asp:Button ID="StartReDeliveryBtn" runat="server" OnClick="StartReDeliveryBtn_Click" Text="選択した施設に再配信" />
                    <br />
                    <br />
                    <wijmo:C1GridView ID="DeliveryStatusGV2" runat="server" AllowVirtualScrolling="False" AutogenerateColumns="False" Culture="ja-JP" FreezingMode="None" RowHeight="19" ScrollMode="None" StaticColumnIndex="-1" StaticRowIndex="-1" style="top: 0px; left: 0px">
<PagerSettings FirstPageText="最初へ" LastPageText="最後へ" NextPageText="次へ" PreviousPageText="前へ"></PagerSettings>
                        <Columns>
                            <wijmo:C1CheckBoxField DataField="配信開始" HeaderText="配信開始" Width="100px">
                            </wijmo:C1CheckBoxField>
                            <wijmo:C1BoundField DataField="施設名" HeaderText="施設名" Width="200px">
                            </wijmo:C1BoundField>
                            <wijmo:C1BoundField DataField="配信状況" HeaderText="配信状況" Width="300px">
                            </wijmo:C1BoundField>
                        </Columns>
                    </wijmo:C1GridView>
                    <br />
                    <br />
                    <br />
                </asp:Panel>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="C1Tabs3_Tab5" runat="server" StaticKey="" Text="カードロックマスター管理" TabIndex="4">
                <asp:Panel ID="Panel5" runat="server" BorderStyle="Double">
                    <br />
                    <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Size="Large" Text=" 　カードロックマスター管理"></asp:Label>
                    <br />
                    　<br /> 
                    <asp:Label ID="Label19" runat="server" Text="　カード区分　"></asp:Label>
                    <asp:TextBox ID="CardTypeTB" runat="server" MaxLength="2" Width="40px"></asp:TextBox>
                    &nbsp;
                    <asp:Label ID="Label20" runat="server" Text="企業社コード  "></asp:Label>
                    <asp:ListBox ID="CompanyCodeLB" runat="server" Height="26px" Rows="1" Width="133px"></asp:ListBox>
                    　&nbsp;&nbsp;<asp:Label ID="Label21" runat="server" Text="S/Sコード"></asp:Label>
                    　&nbsp;&nbsp;<asp:ListBox ID="SSCodeLB" runat="server" Rows="1"></asp:ListBox>
                    <br />
                    <br />
                    　&nbsp;<asp:Label ID="Label22" runat="server" Text="得意先コード"></asp:Label>
                    　<asp:TextBox ID="ClientCodeTB0" runat="server" MaxLength="6" Width="100px"></asp:TextBox>
                    &nbsp;
                    <asp:Label ID="Label23" runat="server" Text="車両番号"></asp:Label>
                    　<asp:TextBox ID="CarNumberTB0" runat="server" MaxLength="4" Width="80px"></asp:TextBox>
                    <br />
                    <br />
                    &nbsp;&nbsp;
                    <asp:Button ID="SearchBtn0" runat="server" Text="検索" OnClick="SearchBtn0_Click" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label24" runat="server" Text="現在の状態"></asp:Label>
                    　<asp:TextBox ID="CurrentStatusTB0" runat="server"></asp:TextBox>
                    <br />
                    <br />
                    &nbsp;&nbsp;
                    <asp:Label ID="Label25" runat="server" Text="処理区分"></asp:Label>
                    　<asp:RadioButton ID="LockRB1" runat="server" GroupName="LockGroup" Text="ロック有効　　" />
                    　<asp:RadioButton ID="LockRB2" runat="server" GroupName="LockGroup" Text="ロック解除　" />
                    　&nbsp;&nbsp;&nbsp; <asp:Button ID="LockValidChgBtn0" runat="server" Text="登録" OnClick="LockValidChgBtn0_Click" />
                    <br />
                    <br />
                    &nbsp;&nbsp;
                    <asp:Button ID="MasterDLBtn0" runat="server" OnClick="MasterDLBtn_Click" Text="マスターダウンロード" />
                    <br />
                    <br />
                </asp:Panel>
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
<asp:Timer ID="AP8Timer1" runat="server" OnTick="AP8Timer_Tick" Interval="30000" Enabled="False" />
<asp:Timer ID="AP8Timer2" runat="server" OnTick="AP8Timer2_Tick" Interval="30000" Enabled="False" />
<asp:Timer ID="AP8Timer3" runat="server" OnTick="AP8Timer3_Tick" Interval="30000" Enabled="False" />
    <p>
    </p>
    <p>
    </p>
</asp:Content>
